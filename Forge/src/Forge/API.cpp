#include <Forge/API.h>
#include <Forge/ForgeEditor.h>
#include <Scythe/Core/Memory.h>

static const uint32 kForgeAPIVersion = 1;

#if !defined(PRODUCTION_BUILD)
FORGE_API uint32 FORGE_CALL QueryEditorVersion()
{
    return kForgeAPIVersion;
}
#endif

FORGE_API IEditor* FORGE_CALL CreateEditor()
{
    return new ForgeEditor();
}

FORGE_API void FORGE_CALL DestroyEditor(IEditor*& inEditor)
{
    ForgeEditor* editor = static_cast<ForgeEditor*>(inEditor);
    SafeDelete(editor);
}