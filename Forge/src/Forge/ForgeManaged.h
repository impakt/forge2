#pragma once

#include <Scythe/Core/Types.h>
#include <Scythe/Core/Utility.h>

using namespace System;

namespace ForgeManaged
{

    public ref class ForgeEditor
    {
    public:
        ForgeEditor();
        ~ForgeEditor();

        void SendPacket(array<Byte>^ packetData);
        void Resize(UInt32 width, UInt32 height);

    private:

    };

}