#include <Forge/ForgeEditor.h>
#include <forge-rs/forge-rs.h>

ForgeEditor::ForgeEditor()
    : mServerContext(nullptr)
{
}

ForgeEditor::~ForgeEditor()
{
}

bool ForgeEditor::Initialize(EditorCallbackInfo inCallbackInfo)
{
    bool result = true;

    ServerConfig config = {};
    config.host = inCallbackInfo.pHostname;
    config.port = inCallbackInfo.Port;
    config.callback.func = inCallbackInfo.pPacketCallback;
    config.callback.userdata = inCallbackInfo.pUserdata;
    mServerContext = forge_start_server(&config);

    if (mServerContext == nullptr)
    {
        result = false;
    }

    return result;
}

void ForgeEditor::Destroy()
{
    if (mServerContext != nullptr)
    {
        forge_stop_server(mServerContext);
        mServerContext = nullptr;
    }
}

void ForgeEditor::SendPacket(const void* inPacketData, uint32 inPacketSize)
{
    SCY_ASSERT(mServerContext != nullptr);

    forge_send_packet(mServerContext, inPacketData, inPacketSize);
}
