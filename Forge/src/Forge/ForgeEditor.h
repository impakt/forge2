#pragma once

#include <Forge/Editor.h>

struct ServerContext;

class ForgeEditor : public IEditor
{
public:
    ForgeEditor();
    ~ForgeEditor();

    bool Initialize(EditorCallbackInfo inCallbackInfo) override;
    void Destroy() override;

    void SendPacket(const void* inPacketData, uint32 inPacketSize) override;

private:
    ServerContext* mServerContext;

    SCY_DISABLE_COPY_AND_ASSIGN(ForgeEditor);
};