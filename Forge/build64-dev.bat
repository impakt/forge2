mkdir imported
pushd imported
mklink /J Scythe ..\..\..\scythe
popd

rmdir build64 /S /Q
mkdir build64
pushd build64
cmake .. -G "Visual Studio 16 2019" -A x64
mklink /J res ..\res
popd

mkdir bin
pushd bin
mkdir Debug
pushd Debug
mklink /J res ..\..\res
popd
mkdir Release
pushd Release
mklink /J res ..\..\res
popd
popd