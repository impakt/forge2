#include <cstdint>
#include <cstdlib>

struct ServerContext;

struct PacketCallback {
  void *userdata;
  void (*func)(void*, const void*, uint32_t);
};

struct ServerConfig {
  const char *host;
  PacketCallback callback;
  uint32_t port;
  uint32_t _padding;
};

extern "C" {

void forge_send_packet(ServerContext *context, const void *packet, uint32_t packet_size);

ServerContext *forge_start_server(const ServerConfig *config);

void forge_stop_server(ServerContext *context);

} // extern "C"
