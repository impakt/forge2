#pragma once

#include <Scythe/Core/Utility.h>
#include <Scythe/Core/Types.h>

typedef void(*ReceivePacketCallback)(void* inUserdata, const void* inPacketData, uint32 inPacketSize);
struct EditorCallbackInfo
{
    void* pUserdata;
    ReceivePacketCallback pPacketCallback;
    const char* pHostname;
    uint32 Port;
};

class IEditor
{
public:
    virtual bool Initialize(EditorCallbackInfo inCallbackInfo) = 0;
    virtual void Destroy() = 0;

    virtual void SendPacket(const void* inPacketData, uint32 inPacketSize) = 0;

protected:
    IEditor() {}
    virtual ~IEditor() {}

private:
    SCY_DISABLE_COPY_AND_ASSIGN(IEditor);
};