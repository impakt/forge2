#pragma once

#include <Forge/Editor.h>
#include <Forge/APICommon.h>

typedef uint32 (FORGE_CALL *PFN_QueryEditorVersion)();
typedef IEditor* (FORGE_CALL *PFN_CreateEditor)();
typedef void (FORGE_CALL *PFN_DestroyEditor)(IEditor*& inEditor);

extern "C"
{
    // Version queries only work on non-production builds.
    // This prevents the editor from loading production build DLLs
#if !defined(PRODUCTION_BUILD)
    FORGE_API uint32 FORGE_CALL QueryEditorVersion();
#endif

    FORGE_API IEditor* FORGE_CALL CreateEditor();
    FORGE_API void FORGE_CALL DestroyEditor(IEditor*& inEditor);
}
