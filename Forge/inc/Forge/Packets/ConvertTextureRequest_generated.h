// automatically generated by the FlatBuffers compiler, do not modify


#ifndef FLATBUFFERS_GENERATED_CONVERTTEXTUREREQUEST_FORGE_H_
#define FLATBUFFERS_GENERATED_CONVERTTEXTUREREQUEST_FORGE_H_

#include "flatbuffers/flatbuffers.h"

namespace Forge {

struct ConvertTextureRequest;

enum TextureFormat {
  TextureFormat_Unknown = 0,
  TextureFormat_RGB_UNORM = 1,
  TextureFormat_RGB_SRGB = 2,
  TextureFormat_RGBA_UNORM = 3,
  TextureFormat_RGBA_SRGB = 4,
  TextureFormat_BC1_RGB_UNORM = 5,
  TextureFormat_BC1_RGB_SRGB = 6,
  TextureFormat_BC1_RGBA_UNORM = 7,
  TextureFormat_BC1_RGBA_SRGB = 8,
  TextureFormat_BC2_RGBA_UNORM = 9,
  TextureFormat_BC2_RGBA_SRGB = 10,
  TextureFormat_BC3_RGBA_UNORM = 11,
  TextureFormat_BC3_RGBA_SRGB = 12,
  TextureFormat_BC4_R_UNORM = 13,
  TextureFormat_BC4_R_SNORM = 14,
  TextureFormat_BC5_RG_UNORM = 15,
  TextureFormat_BC5_RG_SNORM = 16,
  TextureFormat_BC6_RGB_UFLOAT = 17,
  TextureFormat_BC6_RGB_SFLOAT = 18,
  TextureFormat_BC7_RGB_UNORM = 19,
  TextureFormat_BC7_RGB_SRGB = 20,
  TextureFormat_BC7_RGBA_UNORM = 21,
  TextureFormat_BC7_RGBA_SRGB = 22,
  TextureFormat_MIN = TextureFormat_Unknown,
  TextureFormat_MAX = TextureFormat_BC7_RGBA_SRGB
};

inline const TextureFormat (&EnumValuesTextureFormat())[23] {
  static const TextureFormat values[] = {
    TextureFormat_Unknown,
    TextureFormat_RGB_UNORM,
    TextureFormat_RGB_SRGB,
    TextureFormat_RGBA_UNORM,
    TextureFormat_RGBA_SRGB,
    TextureFormat_BC1_RGB_UNORM,
    TextureFormat_BC1_RGB_SRGB,
    TextureFormat_BC1_RGBA_UNORM,
    TextureFormat_BC1_RGBA_SRGB,
    TextureFormat_BC2_RGBA_UNORM,
    TextureFormat_BC2_RGBA_SRGB,
    TextureFormat_BC3_RGBA_UNORM,
    TextureFormat_BC3_RGBA_SRGB,
    TextureFormat_BC4_R_UNORM,
    TextureFormat_BC4_R_SNORM,
    TextureFormat_BC5_RG_UNORM,
    TextureFormat_BC5_RG_SNORM,
    TextureFormat_BC6_RGB_UFLOAT,
    TextureFormat_BC6_RGB_SFLOAT,
    TextureFormat_BC7_RGB_UNORM,
    TextureFormat_BC7_RGB_SRGB,
    TextureFormat_BC7_RGBA_UNORM,
    TextureFormat_BC7_RGBA_SRGB
  };
  return values;
}

inline const char * const *EnumNamesTextureFormat() {
  static const char * const names[] = {
    "Unknown",
    "RGB_UNORM",
    "RGB_SRGB",
    "RGBA_UNORM",
    "RGBA_SRGB",
    "BC1_RGB_UNORM",
    "BC1_RGB_SRGB",
    "BC1_RGBA_UNORM",
    "BC1_RGBA_SRGB",
    "BC2_RGBA_UNORM",
    "BC2_RGBA_SRGB",
    "BC3_RGBA_UNORM",
    "BC3_RGBA_SRGB",
    "BC4_R_UNORM",
    "BC4_R_SNORM",
    "BC5_RG_UNORM",
    "BC5_RG_SNORM",
    "BC6_RGB_UFLOAT",
    "BC6_RGB_SFLOAT",
    "BC7_RGB_UNORM",
    "BC7_RGB_SRGB",
    "BC7_RGBA_UNORM",
    "BC7_RGBA_SRGB",
    nullptr
  };
  return names;
}

inline const char *EnumNameTextureFormat(TextureFormat e) {
  if (e < TextureFormat_Unknown || e > TextureFormat_BC7_RGBA_SRGB) return "";
  const size_t index = static_cast<int>(e);
  return EnumNamesTextureFormat()[index];
}

enum TextureConversionQuality {
  TextureConversionQuality_Unknown = 0,
  TextureConversionQuality_Low = 1,
  TextureConversionQuality_Medium = 2,
  TextureConversionQuality_High = 3,
  TextureConversionQuality_MIN = TextureConversionQuality_Unknown,
  TextureConversionQuality_MAX = TextureConversionQuality_High
};

inline const TextureConversionQuality (&EnumValuesTextureConversionQuality())[4] {
  static const TextureConversionQuality values[] = {
    TextureConversionQuality_Unknown,
    TextureConversionQuality_Low,
    TextureConversionQuality_Medium,
    TextureConversionQuality_High
  };
  return values;
}

inline const char * const *EnumNamesTextureConversionQuality() {
  static const char * const names[] = {
    "Unknown",
    "Low",
    "Medium",
    "High",
    nullptr
  };
  return names;
}

inline const char *EnumNameTextureConversionQuality(TextureConversionQuality e) {
  if (e < TextureConversionQuality_Unknown || e > TextureConversionQuality_High) return "";
  const size_t index = static_cast<int>(e);
  return EnumNamesTextureConversionQuality()[index];
}

struct ConvertTextureRequest FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_FILENAME = 4,
    VT_OUTPUTPATH = 6,
    VT_OUTPUTFORMAT = 8,
    VT_QUALITY = 10,
    VT_FILEDATA = 12
  };
  const flatbuffers::String *filename() const {
    return GetPointer<const flatbuffers::String *>(VT_FILENAME);
  }
  const flatbuffers::String *outputPath() const {
    return GetPointer<const flatbuffers::String *>(VT_OUTPUTPATH);
  }
  TextureFormat outputFormat() const {
    return static_cast<TextureFormat>(GetField<uint32_t>(VT_OUTPUTFORMAT, 0));
  }
  TextureConversionQuality quality() const {
    return static_cast<TextureConversionQuality>(GetField<uint32_t>(VT_QUALITY, 0));
  }
  const flatbuffers::Vector<uint8_t> *fileData() const {
    return GetPointer<const flatbuffers::Vector<uint8_t> *>(VT_FILEDATA);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyOffset(verifier, VT_FILENAME) &&
           verifier.VerifyString(filename()) &&
           VerifyOffset(verifier, VT_OUTPUTPATH) &&
           verifier.VerifyString(outputPath()) &&
           VerifyField<uint32_t>(verifier, VT_OUTPUTFORMAT) &&
           VerifyField<uint32_t>(verifier, VT_QUALITY) &&
           VerifyOffset(verifier, VT_FILEDATA) &&
           verifier.VerifyVector(fileData()) &&
           verifier.EndTable();
  }
};

struct ConvertTextureRequestBuilder {
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_filename(flatbuffers::Offset<flatbuffers::String> filename) {
    fbb_.AddOffset(ConvertTextureRequest::VT_FILENAME, filename);
  }
  void add_outputPath(flatbuffers::Offset<flatbuffers::String> outputPath) {
    fbb_.AddOffset(ConvertTextureRequest::VT_OUTPUTPATH, outputPath);
  }
  void add_outputFormat(TextureFormat outputFormat) {
    fbb_.AddElement<uint32_t>(ConvertTextureRequest::VT_OUTPUTFORMAT, static_cast<uint32_t>(outputFormat), 0);
  }
  void add_quality(TextureConversionQuality quality) {
    fbb_.AddElement<uint32_t>(ConvertTextureRequest::VT_QUALITY, static_cast<uint32_t>(quality), 0);
  }
  void add_fileData(flatbuffers::Offset<flatbuffers::Vector<uint8_t>> fileData) {
    fbb_.AddOffset(ConvertTextureRequest::VT_FILEDATA, fileData);
  }
  explicit ConvertTextureRequestBuilder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  ConvertTextureRequestBuilder &operator=(const ConvertTextureRequestBuilder &);
  flatbuffers::Offset<ConvertTextureRequest> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<ConvertTextureRequest>(end);
    return o;
  }
};

inline flatbuffers::Offset<ConvertTextureRequest> CreateConvertTextureRequest(
    flatbuffers::FlatBufferBuilder &_fbb,
    flatbuffers::Offset<flatbuffers::String> filename = 0,
    flatbuffers::Offset<flatbuffers::String> outputPath = 0,
    TextureFormat outputFormat = TextureFormat_Unknown,
    TextureConversionQuality quality = TextureConversionQuality_Unknown,
    flatbuffers::Offset<flatbuffers::Vector<uint8_t>> fileData = 0) {
  ConvertTextureRequestBuilder builder_(_fbb);
  builder_.add_fileData(fileData);
  builder_.add_quality(quality);
  builder_.add_outputFormat(outputFormat);
  builder_.add_outputPath(outputPath);
  builder_.add_filename(filename);
  return builder_.Finish();
}

inline flatbuffers::Offset<ConvertTextureRequest> CreateConvertTextureRequestDirect(
    flatbuffers::FlatBufferBuilder &_fbb,
    const char *filename = nullptr,
    const char *outputPath = nullptr,
    TextureFormat outputFormat = TextureFormat_Unknown,
    TextureConversionQuality quality = TextureConversionQuality_Unknown,
    const std::vector<uint8_t> *fileData = nullptr) {
  auto filename__ = filename ? _fbb.CreateString(filename) : 0;
  auto outputPath__ = outputPath ? _fbb.CreateString(outputPath) : 0;
  auto fileData__ = fileData ? _fbb.CreateVector<uint8_t>(*fileData) : 0;
  return Forge::CreateConvertTextureRequest(
      _fbb,
      filename__,
      outputPath__,
      outputFormat,
      quality,
      fileData__);
}

inline const Forge::ConvertTextureRequest *GetConvertTextureRequest(const void *buf) {
  return flatbuffers::GetRoot<Forge::ConvertTextureRequest>(buf);
}

inline const Forge::ConvertTextureRequest *GetSizePrefixedConvertTextureRequest(const void *buf) {
  return flatbuffers::GetSizePrefixedRoot<Forge::ConvertTextureRequest>(buf);
}

inline bool VerifyConvertTextureRequestBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifyBuffer<Forge::ConvertTextureRequest>(nullptr);
}

inline bool VerifySizePrefixedConvertTextureRequestBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifySizePrefixedBuffer<Forge::ConvertTextureRequest>(nullptr);
}

inline void FinishConvertTextureRequestBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<Forge::ConvertTextureRequest> root) {
  fbb.Finish(root);
}

inline void FinishSizePrefixedConvertTextureRequestBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<Forge::ConvertTextureRequest> root) {
  fbb.FinishSizePrefixed(root);
}

}  // namespace Forge

#endif  // FLATBUFFERS_GENERATED_CONVERTTEXTUREREQUEST_FORGE_H_
