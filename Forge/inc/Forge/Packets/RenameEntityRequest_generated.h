// automatically generated by the FlatBuffers compiler, do not modify


#ifndef FLATBUFFERS_GENERATED_RENAMEENTITYREQUEST_FORGE_H_
#define FLATBUFFERS_GENERATED_RENAMEENTITYREQUEST_FORGE_H_

#include "flatbuffers/flatbuffers.h"

#include "Forge/Packets/Common_generated.h"

namespace Forge {

struct RenameEntityRequest;

struct RenameEntityRequest FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_ENTITYID = 4,
    VT_NAME = 6
  };
  uint32_t entityId() const {
    return GetField<uint32_t>(VT_ENTITYID, 0);
  }
  const flatbuffers::String *name() const {
    return GetPointer<const flatbuffers::String *>(VT_NAME);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyField<uint32_t>(verifier, VT_ENTITYID) &&
           VerifyOffset(verifier, VT_NAME) &&
           verifier.VerifyString(name()) &&
           verifier.EndTable();
  }
};

struct RenameEntityRequestBuilder {
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_entityId(uint32_t entityId) {
    fbb_.AddElement<uint32_t>(RenameEntityRequest::VT_ENTITYID, entityId, 0);
  }
  void add_name(flatbuffers::Offset<flatbuffers::String> name) {
    fbb_.AddOffset(RenameEntityRequest::VT_NAME, name);
  }
  explicit RenameEntityRequestBuilder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  RenameEntityRequestBuilder &operator=(const RenameEntityRequestBuilder &);
  flatbuffers::Offset<RenameEntityRequest> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<RenameEntityRequest>(end);
    return o;
  }
};

inline flatbuffers::Offset<RenameEntityRequest> CreateRenameEntityRequest(
    flatbuffers::FlatBufferBuilder &_fbb,
    uint32_t entityId = 0,
    flatbuffers::Offset<flatbuffers::String> name = 0) {
  RenameEntityRequestBuilder builder_(_fbb);
  builder_.add_name(name);
  builder_.add_entityId(entityId);
  return builder_.Finish();
}

inline flatbuffers::Offset<RenameEntityRequest> CreateRenameEntityRequestDirect(
    flatbuffers::FlatBufferBuilder &_fbb,
    uint32_t entityId = 0,
    const char *name = nullptr) {
  auto name__ = name ? _fbb.CreateString(name) : 0;
  return Forge::CreateRenameEntityRequest(
      _fbb,
      entityId,
      name__);
}

inline const Forge::RenameEntityRequest *GetRenameEntityRequest(const void *buf) {
  return flatbuffers::GetRoot<Forge::RenameEntityRequest>(buf);
}

inline const Forge::RenameEntityRequest *GetSizePrefixedRenameEntityRequest(const void *buf) {
  return flatbuffers::GetSizePrefixedRoot<Forge::RenameEntityRequest>(buf);
}

inline bool VerifyRenameEntityRequestBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifyBuffer<Forge::RenameEntityRequest>(nullptr);
}

inline bool VerifySizePrefixedRenameEntityRequestBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifySizePrefixedBuffer<Forge::RenameEntityRequest>(nullptr);
}

inline void FinishRenameEntityRequestBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<Forge::RenameEntityRequest> root) {
  fbb.Finish(root);
}

inline void FinishSizePrefixedRenameEntityRequestBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<Forge::RenameEntityRequest> root) {
  fbb.FinishSizePrefixed(root);
}

}  // namespace Forge

#endif  // FLATBUFFERS_GENERATED_RENAMEENTITYREQUEST_FORGE_H_
