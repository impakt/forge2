// automatically generated by the FlatBuffers compiler, do not modify


#ifndef FLATBUFFERS_GENERATED_QUERYINTERFACERESPONSE_FORGE_H_
#define FLATBUFFERS_GENERATED_QUERYINTERFACERESPONSE_FORGE_H_

#include "flatbuffers/flatbuffers.h"

#include "Forge/Packets/Common_generated.h"

namespace Forge {

struct QueryInterfaceResponse;

struct QueryInterfaceResponse FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_VERSION = 4
  };
  uint32_t version() const {
    return GetField<uint32_t>(VT_VERSION, 0);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyField<uint32_t>(verifier, VT_VERSION) &&
           verifier.EndTable();
  }
};

struct QueryInterfaceResponseBuilder {
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_version(uint32_t version) {
    fbb_.AddElement<uint32_t>(QueryInterfaceResponse::VT_VERSION, version, 0);
  }
  explicit QueryInterfaceResponseBuilder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  QueryInterfaceResponseBuilder &operator=(const QueryInterfaceResponseBuilder &);
  flatbuffers::Offset<QueryInterfaceResponse> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<QueryInterfaceResponse>(end);
    return o;
  }
};

inline flatbuffers::Offset<QueryInterfaceResponse> CreateQueryInterfaceResponse(
    flatbuffers::FlatBufferBuilder &_fbb,
    uint32_t version = 0) {
  QueryInterfaceResponseBuilder builder_(_fbb);
  builder_.add_version(version);
  return builder_.Finish();
}

inline const Forge::QueryInterfaceResponse *GetQueryInterfaceResponse(const void *buf) {
  return flatbuffers::GetRoot<Forge::QueryInterfaceResponse>(buf);
}

inline const Forge::QueryInterfaceResponse *GetSizePrefixedQueryInterfaceResponse(const void *buf) {
  return flatbuffers::GetSizePrefixedRoot<Forge::QueryInterfaceResponse>(buf);
}

inline bool VerifyQueryInterfaceResponseBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifyBuffer<Forge::QueryInterfaceResponse>(nullptr);
}

inline bool VerifySizePrefixedQueryInterfaceResponseBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifySizePrefixedBuffer<Forge::QueryInterfaceResponse>(nullptr);
}

inline void FinishQueryInterfaceResponseBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<Forge::QueryInterfaceResponse> root) {
  fbb.Finish(root);
}

inline void FinishSizePrefixedQueryInterfaceResponseBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<Forge::QueryInterfaceResponse> root) {
  fbb.FinishSizePrefixed(root);
}

}  // namespace Forge

#endif  // FLATBUFFERS_GENERATED_QUERYINTERFACERESPONSE_FORGE_H_
