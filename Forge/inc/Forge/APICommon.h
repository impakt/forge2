#pragma once

#include <Scythe/Interface/Interface.h>

#if defined(FORGE_EXPORTS)
#define FORGE_API INTERFACE_EXPORT
#else
#define FORGE_API INTERFACE_IMPORT
#endif

#define FORGE_CALL INTERFACE_CALL