import sys
import itertools
import os
import re
import subprocess
import shlex
import tempfile

FLATC_COMMAND = "%s --%s --es6-js-export --include-prefix Forge/Packets -o %s %s"

GENERATED_DIR_NAME = "Generated"

if __name__ == "__main__":
    if len(sys.argv) == 5:
        compilerPath = sys.argv[1]
        specDir = sys.argv[2]
        outputDir = sys.argv[3]
        language = sys.argv[4]

        compilerDir = os.path.dirname(compilerPath)
        compilerExecutable = os.path.basename(compilerPath)

        specPaths = []
        suffixTable = [".fbs"]
        for root, dirs, filenames in os.walk(specDir):
            for filename in filenames:
                if os.path.splitext(os.path.basename(filename))[1].lower() in suffixTable:
                    specPaths.append(os.path.abspath(os.path.join(root, filename)).replace("\\", "/"))

        specPathsString = ' '.join(specPaths)

        commandString = FLATC_COMMAND % (compilerExecutable, language, os.path.join(os.path.abspath(outputDir), '').replace("\\", "/"), specPathsString)
        commandArgs = shlex.split(commandString)
        result = subprocess.run(commandArgs, stdout=subprocess.PIPE, universal_newlines=True, cwd=compilerDir, shell=True)
        if result.returncode is not 0:
            print("Failed")
            print(result.stdout)
            sys.exit(1)
        else:
            print("Succeeded")
    else:
        print("Invalid arguments!")
    sys.exit(0)