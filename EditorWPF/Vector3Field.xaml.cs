﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Core.Converters;

namespace EditorWPF
{
    /// <summary>
    /// Interaction logic for Vector3Field.xaml
    /// </summary>
    public partial class Vector3Field : UserControl
    {
        public Vector3Field()
        {
            DataContext = this;

            InitializeComponent();
        }

        public float X
        {
            get => (float) GetValue(XProperty);
            set => SetValue(XProperty, value);
        }

        public float Y
        {
            get => (float) GetValue(YProperty);
            set => SetValue(YProperty, value);
        }

        public float Z
        {
            get => (float) GetValue(ZProperty);
            set => SetValue(ZProperty, value);
        }

        public static DependencyProperty XProperty =
            DependencyProperty.Register(
                "X", typeof(float), typeof(SingleUpDown),
                new FrameworkPropertyMetadata(
                    0f,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    XPropertyChanged));

        public static DependencyProperty YProperty =
            DependencyProperty.Register(
                "Y", typeof(float), typeof(SingleUpDown),
                new FrameworkPropertyMetadata(
                    0f,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    YPropertyChanged));

        public static DependencyProperty ZProperty =
            DependencyProperty.Register(
                "Z", typeof(float), typeof(SingleUpDown),
                new FrameworkPropertyMetadata(
                    0f,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    ZPropertyChanged));

        private static void XPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SingleUpDown control)
            {
                control.Value = (float) e.NewValue;
            }
        }

        private static void YPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SingleUpDown control)
            {
                control.Value = (float) e.NewValue;
            }
        }

        private static void ZPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SingleUpDown control)
            {
                control.Value = (float) e.NewValue;
            }
        }
    }
}
