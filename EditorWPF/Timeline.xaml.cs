﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Editor;
using Forge;

namespace EditorWPF
{
    /// <summary>
    /// Interaction logic for Timeline.xaml
    /// </summary>
    public partial class Timeline : UserControl
    {
        private EntityDescription selectedEntity;

        public Timeline()
        {
            InitializeComponent();
        }

        public ForgeInterface PrivateForgeInterface { get; set; }
        public Forge.AnimatedPropertyDescription[] AnimatedProperties { get; set; }

        public Forge.EntityDescription SelectedEntity
        {
            get => selectedEntity;
            set
            {
                stackPanel.Children.Clear();
                selectedEntity = value;

                AnimatedProperties = PrivateForgeInterface.GetAnimatedProperties(value.Id);

                foreach (var animatedProperty in AnimatedProperties)
                {
                    var label = new Label
                    {
                        Content = animatedProperty.Name
                    };
                    stackPanel.Children.Add(label);
                }
            }
        }
    }
}
