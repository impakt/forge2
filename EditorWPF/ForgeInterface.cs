﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Threading;
using FlatBuffers;

namespace Editor
{
    public class ForgeInterface
    {
        private Assembly mEditorAssembly;
        private object mEditor;
        private MethodInfo mSendPacketFunc;
        private EventInfo mOnPacketReceivedEvent;

        private FlatBufferBuilder mBuilder = new FlatBufferBuilder(1024);
        private SemaphoreSlim mSemaphore = new SemaphoreSlim(0, 1);
        private Forge.Packet mPacket;

        private void OnPacketReceived(byte[] packetData)
        {
            ByteBuffer byteBuffer = new ByteBuffer(packetData);
            mPacket = Forge.Packet.GetRootAsPacket(byteBuffer);
            mSemaphore.Release();
        }

        private void SendPacket(byte[] packetData)
        {
            mSendPacketFunc.Invoke(mEditor, new object[] {packetData});
        }

        public ForgeInterface(string modulePath, IntPtr windowHandle, UInt32 width, UInt32 height)
        {
            mEditorAssembly = Assembly.LoadFrom(modulePath);

            var editorType = mEditorAssembly.GetType("Stiletto.Editor");
            mSendPacketFunc = editorType.GetMethod("SendPacket");
            mOnPacketReceivedEvent = editorType.GetEvent("OnPacketReceived");

            mEditor = System.Activator.CreateInstance(editorType, windowHandle, width, height);

            MethodInfo methodInfo =
                typeof(ForgeInterface).GetMethod("OnPacketReceived", BindingFlags.NonPublic | BindingFlags.Instance);
            Delegate packetReceivedDelegate =
                Delegate.CreateDelegate(mOnPacketReceivedEvent.EventHandlerType, this, methodInfo);
            mOnPacketReceivedEvent.AddEventHandler(mEditor, packetReceivedDelegate);
        }

        ~ForgeInterface()
        {
            ((IDisposable) mEditor).Dispose();
        }

        private FlatBufferBuilder BeginRequest()
        {
            mBuilder.Clear();

            return mBuilder;
        }

        private Forge.Packet EndRequest(FlatBufferBuilder builder, int rootTable)
        {
            builder.Finish(rootTable);

            SendPacket(builder.SizedByteArray());
            mSemaphore.Wait();

            return mPacket;
        }

        private void EndRequestWithoutResponse(FlatBufferBuilder builder, int rootTable)
        {
            builder.Finish(rootTable);
            SendPacket(builder.SizedByteArray());
        }

        public bool LoadDemo(string path)
        {
            var builder = BeginRequest();

            var payloadOffset = Forge.LoadDemoRequest.CreateLoadDemoRequest(builder, builder.CreateString(path));
            var packetOffset =
                Forge.Packet.CreatePacket(builder, Forge.PacketPayload.LoadDemoRequest, payloadOffset.Value);

            Forge.Packet packet = EndRequest(builder, packetOffset.Value);

            Forge.LoadDemoResponse response = packet.Payload<Forge.LoadDemoResponse>().GetValueOrDefault();
            return (response.Result == Forge.Result.Success);
        }

        public Forge.EntityDescription[] QueryEntities()
        {
            var builder = BeginRequest();

            var packetOffset = Forge.Packet.CreatePacket(builder, Forge.PacketPayload.QueryEntitiesRequest);

            Forge.Packet packet = EndRequest(builder, packetOffset.Value);

            Forge.QueryEntitiesResponse response = packet.Payload<Forge.QueryEntitiesResponse>().GetValueOrDefault();

            Forge.EntityDescription[] entities = new Forge.EntityDescription[response.EntitiesLength];
            for (int i = 0; i < response.EntitiesLength; ++i)
            {
                entities[i] = response.Entities(i).GetValueOrDefault();
            }

            return entities;
        }

        public Forge.PropertyDescription[] QueryProperties(uint entityId)
        {
            var builder = BeginRequest();

            var payloadOffset = Forge.QueryPropertiesRequest.CreateQueryPropertiesRequest(builder, entityId);
            var packetOffset = Forge.Packet.CreatePacket(builder, Forge.PacketPayload.QueryPropertiesRequest,
                payloadOffset.Value);

            Forge.Packet packet = EndRequest(builder, packetOffset.Value);

            Forge.QueryPropertiesResponse
                response = packet.Payload<Forge.QueryPropertiesResponse>().GetValueOrDefault();

            Forge.PropertyDescription[] properties = null;

            if (response.Result == Forge.Result.Success)
            {
                properties = new Forge.PropertyDescription[response.PropertiesLength];
                for (int i = 0; i < response.PropertiesLength; ++i)
                {
                    properties[i] = response.Properties(i).GetValueOrDefault();
                }
            }

            return properties;
        }

        public bool SelectEntity(uint entityId)
        {
            var builder = BeginRequest();

            var payloadOffset = Forge.SelectEntityRequest.CreateSelectEntityRequest(builder, entityId);
            var packetOffset =
                Forge.Packet.CreatePacket(builder, Forge.PacketPayload.SelectEntityRequest, payloadOffset.Value);

            Forge.Packet packet = EndRequest(builder, packetOffset.Value);

            Forge.SelectEntityResponse response = packet.Payload<Forge.SelectEntityResponse>().GetValueOrDefault();
            return (response.Result == Forge.Result.Success);
        }

        public bool ModifyStringProperty(uint entityId, uint propertyIndex, string data)
        {
            var builder = BeginRequest();

            var stringOffset = builder.CreateString(data);
            var valueOffset = Forge.StringProperty.CreateStringProperty(builder, stringOffset);

            var payloadOffset = Forge.ModifyPropertyRequest.CreateModifyPropertyRequest(builder, entityId,
                propertyIndex, Forge.PropertyData.StringProperty, valueOffset.Value);
            var packetOffset =
                Forge.Packet.CreatePacket(builder, Forge.PacketPayload.ModifyPropertyRequest, payloadOffset.Value);

            var packet = EndRequest(builder, packetOffset.Value);

            var response = packet.Payload<Forge.ModifyPropertyResponse>().GetValueOrDefault();
            return (response.Result == Forge.Result.Success);
        }

        public bool ModifyBooleanProperty(uint entityId, uint propertyIndex, bool data)
        {
            var builder = BeginRequest();

            var valueOffset = Forge.BoolProperty.CreateBoolProperty(builder, data);

            var payloadOffset = Forge.ModifyPropertyRequest.CreateModifyPropertyRequest(builder, entityId,
                propertyIndex, Forge.PropertyData.BoolProperty, valueOffset.Value);
            var packetOffset =
                Forge.Packet.CreatePacket(builder, Forge.PacketPayload.ModifyPropertyRequest, payloadOffset.Value);

            var packet = EndRequest(builder, packetOffset.Value);

            var response = packet.Payload<Forge.ModifyPropertyResponse>().GetValueOrDefault();
            return (response.Result == Forge.Result.Success);
        }

        public bool ModifyIntegerProperty(uint entityId, uint propertyIndex, int data)
        {
            var builder = BeginRequest();

            var valueOffset = Forge.IntegerProperty.CreateIntegerProperty(builder, data);

            var payloadOffset = Forge.ModifyPropertyRequest.CreateModifyPropertyRequest(builder, entityId,
                propertyIndex, Forge.PropertyData.IntegerProperty, valueOffset.Value);
            var packetOffset =
                Forge.Packet.CreatePacket(builder, Forge.PacketPayload.ModifyPropertyRequest, payloadOffset.Value);

            var packet = EndRequest(builder, packetOffset.Value);

            var response = packet.Payload<Forge.ModifyPropertyResponse>().GetValueOrDefault();
            return (response.Result == Forge.Result.Success);
        }

        public bool ModifyUnsignedProperty(uint entityId, uint propertyIndex, uint data)
        {
            var builder = BeginRequest();

            var valueOffset = Forge.UnsignedIntegerProperty.CreateUnsignedIntegerProperty(builder, data);

            var payloadOffset = Forge.ModifyPropertyRequest.CreateModifyPropertyRequest(builder, entityId,
                propertyIndex, Forge.PropertyData.UnsignedIntegerProperty, valueOffset.Value);
            var packetOffset =
                Forge.Packet.CreatePacket(builder, Forge.PacketPayload.ModifyPropertyRequest, payloadOffset.Value);

            var packet = EndRequest(builder, packetOffset.Value);

            var response = packet.Payload<Forge.ModifyPropertyResponse>().GetValueOrDefault();
            return (response.Result == Forge.Result.Success);
        }

        public bool ModifyFloatProperty(uint entityId, uint propertyIndex, float data)
        {
            var builder = BeginRequest();

            var valueOffset = Forge.FloatProperty.CreateFloatProperty(builder, data);

            var payloadOffset = Forge.ModifyPropertyRequest.CreateModifyPropertyRequest(builder, entityId,
                propertyIndex, Forge.PropertyData.FloatProperty, valueOffset.Value);
            var packetOffset =
                Forge.Packet.CreatePacket(builder, Forge.PacketPayload.ModifyPropertyRequest, payloadOffset.Value);

            var packet = EndRequest(builder, packetOffset.Value);

            var response = packet.Payload<Forge.ModifyPropertyResponse>().GetValueOrDefault();
            return (response.Result == Forge.Result.Success);
        }

        public Forge.AnimatedPropertyDescription[] GetAnimatedProperties(uint entityId)
        {
            var builder = BeginRequest();

            var payloadOffset =
                Forge.QueryAnimatedPropertiesRequest.CreateQueryAnimatedPropertiesRequest(builder, entityId);
            var packetOffset = Forge.Packet.CreatePacket(builder, Forge.PacketPayload.QueryAnimatedPropertiesRequest,
                payloadOffset.Value);

            var packet = EndRequest(builder, packetOffset.Value);

            var response = packet.Payload<Forge.QueryAnimatedPropertiesResponse>().GetValueOrDefault();

            Forge.AnimatedPropertyDescription[] properties = null;

            if (response.Result == Forge.Result.Success)
            {
                properties = new Forge.AnimatedPropertyDescription[response.PropertiesLength];
                for (int i = 0; i < response.PropertiesLength; ++i)
                {
                    properties[i] = response.Properties(i).GetValueOrDefault();
                }
            }

            return properties;
        }
    }
}
