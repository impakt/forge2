﻿using Editor;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace EditorWPF
{
    /// <summary>
    /// Interaction logic for PropertyField.xaml
    /// </summary>
    public partial class PropertyField : UserControl
    {
        public PropertyField()
        {
            InitializeComponent();
        }

        private ForgeInterface forgeInterface;
        private uint propertyIndex;
        public Forge.EntityDescription Entity { get; set; }
        public Forge.PropertyDescription EntityProperty { get; set; }

        private Label CreateLabel(Forge.PropertyDescription property)
        {
            var label = new Label
            {
                Name = "label",
                Content = property.Name,
                Template = (ControlTemplate) FindResource("baseLabel"),
                ToolTip = property.Description,
            };

            return label;
        }

        private Label CreateLabel(Forge.PropertyDescription property, string alternativeLabel)
        {
            var label = new Label
            {
                Name = "label",
                Content = alternativeLabel,
                Template = (ControlTemplate) FindResource("baseLabel"),
                ToolTip = property.Description,
            };

            return label;
        }


        public PropertyField(ForgeInterface forgeInterface, Forge.EntityDescription entity,
            Forge.PropertyDescription property, uint propertyIndex)
        {
            this.propertyIndex = propertyIndex;
            this.forgeInterface = forgeInterface;
            Entity = entity;
            EntityProperty = property;

            InitializeComponent();

            switch (property.ValueType)
            {
                case Forge.PropertyData.StringProperty:
                    var textBox = new TextBox
                    {
                        Text = property.Value<Forge.StringProperty>()?.Value ?? "",
                        Template = (ControlTemplate) FindResource("stringTextBox"),
                    };

                    textBox.TextChanged += TextBox_TextChanged;

                    grid.Children.Add(CreateLabel(property));
                    grid.Children.Add(textBox);
                    break;

                case Forge.PropertyData.BoolProperty:
                    var checkBox = new CheckBox
                    {
                        IsChecked = property.Value<Forge.BoolProperty>()?.Value,
                        Content = property.Name,
                        Template = (ControlTemplate) FindResource("checkBox"),
                        ToolTip = property.Description,
                    };

                    checkBox.Checked += CheckBox_Checked;
                    checkBox.Unchecked += CheckBox_Unchecked;

                    Height = 20;
                    grid.Children.Add(checkBox);
                    break;

                case Forge.PropertyData.IntegerProperty:
                    var integerBox = new IntegerUpDown
                    {
                        Value = property.Value<Forge.IntegerProperty>()?.Value,
                        Template = (ControlTemplate) FindResource("integerTextBox"),
                        Minimum = 0,
                    };

                    integerBox.ValueChanged += IntegerBox_ValueChanged;

                    grid.Children.Add(CreateLabel(property));
                    grid.Children.Add(integerBox);
                    break;

                case Forge.PropertyData.UnsignedIntegerProperty:
                    var unsignedIntBox = new UIntegerUpDown
                    {
                        Value = property.Value<Forge.UnsignedIntegerProperty>()?.Value,
                        Template = (ControlTemplate) FindResource("unsignedIntTextBox"),
                    };

                    unsignedIntBox.ValueChanged += UnsignedIntBox_ValueChanged;

                    grid.Children.Add(CreateLabel(property));
                    grid.Children.Add(unsignedIntBox);
                    break;

                case Forge.PropertyData.FloatProperty:
                    var floatBox = new SingleUpDown
                    {
                        Value = property.Value<Forge.FloatProperty>()?.Value,
                        Template = (ControlTemplate) FindResource("floatTextBox"),
                    };

                    floatBox.ValueChanged += FloatBox_ValueChanged;

                    grid.Children.Add(CreateLabel(property));
                    grid.Children.Add(floatBox);
                    break;

                case Forge.PropertyData.Vector3Property:
                    var vector3 = property.Value<Forge.Vector3Property>()?.Value;
                    var vector3Box = new Vector3Field
                    {
                        X = vector3?.X ?? 0f,
                        Y = vector3?.Y ?? 0f,
                        Z = vector3?.Z ?? 0f,
                    };

                    vector3Box.x.ValueChanged += XOnValueChanged;
                    vector3Box.y.ValueChanged += YOnValueChanged;
                    vector3Box.z.ValueChanged += ZOnValueChanged;

                    grid.Children.Add(CreateLabel(property, $"{property.Name} (X, Y, Z)"));
                    grid.Children.Add(vector3Box);
                    break;

                default:
                    Debug.WriteLine($"Unhandled property type: {property.ValueType}");
                    break;
            }
        }

        private void ZOnValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            throw new NotImplementedException();
        }

        private void YOnValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            throw new NotImplementedException();
        }

        private void XOnValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            throw new NotImplementedException();
        }

        private void FloatBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            forgeInterface.ModifyFloatProperty(Entity.Id, propertyIndex, (float?) e.NewValue ?? 0f);
        }

        private void UnsignedIntBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            forgeInterface.ModifyUnsignedProperty(Entity.Id, propertyIndex, (uint?) e.NewValue ?? 0);
        }

        private void IntegerBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            forgeInterface.ModifyIntegerProperty(Entity.Id, propertyIndex, (int?) e.NewValue ?? 0);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            forgeInterface.ModifyBooleanProperty(Entity.Id, propertyIndex, true);
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            forgeInterface.ModifyBooleanProperty(Entity.Id, propertyIndex, false);
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var text = ((TextBox) e.OriginalSource).Text;

            // Don't update text if it's the same
            if (!text.Equals(EntityProperty.Value<Forge.StringProperty>()?.Value ?? ""))
            {
                forgeInterface.ModifyStringProperty(Entity.Id, propertyIndex, text);
            }
        }
    }
}
