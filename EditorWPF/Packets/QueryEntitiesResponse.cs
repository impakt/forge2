// <auto-generated>
//  automatically generated by the FlatBuffers compiler, do not modify
// </auto-generated>

namespace Forge
{

using global::System;
using global::FlatBuffers;

public struct QueryEntitiesResponse : IFlatbufferObject
{
  private Table __p;
  public ByteBuffer ByteBuffer { get { return __p.bb; } }
  public static QueryEntitiesResponse GetRootAsQueryEntitiesResponse(ByteBuffer _bb) { return GetRootAsQueryEntitiesResponse(_bb, new QueryEntitiesResponse()); }
  public static QueryEntitiesResponse GetRootAsQueryEntitiesResponse(ByteBuffer _bb, QueryEntitiesResponse obj) { return (obj.__assign(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __p.bb_pos = _i; __p.bb = _bb; }
  public QueryEntitiesResponse __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public EntityDescription? Entities(int j) { int o = __p.__offset(4); return o != 0 ? (EntityDescription?)(new EntityDescription()).__assign(__p.__indirect(__p.__vector(o) + j * 4), __p.bb) : null; }
  public int EntitiesLength { get { int o = __p.__offset(4); return o != 0 ? __p.__vector_len(o) : 0; } }

  public static Offset<QueryEntitiesResponse> CreateQueryEntitiesResponse(FlatBufferBuilder builder,
      VectorOffset entitiesOffset = default(VectorOffset)) {
    builder.StartObject(1);
    QueryEntitiesResponse.AddEntities(builder, entitiesOffset);
    return QueryEntitiesResponse.EndQueryEntitiesResponse(builder);
  }

  public static void StartQueryEntitiesResponse(FlatBufferBuilder builder) { builder.StartObject(1); }
  public static void AddEntities(FlatBufferBuilder builder, VectorOffset entitiesOffset) { builder.AddOffset(0, entitiesOffset.Value, 0); }
  public static VectorOffset CreateEntitiesVector(FlatBufferBuilder builder, Offset<EntityDescription>[] data) { builder.StartVector(4, data.Length, 4); for (int i = data.Length - 1; i >= 0; i--) builder.AddOffset(data[i].Value); return builder.EndVector(); }
  public static VectorOffset CreateEntitiesVectorBlock(FlatBufferBuilder builder, Offset<EntityDescription>[] data) { builder.StartVector(4, data.Length, 4); builder.Add(data); return builder.EndVector(); }
  public static void StartEntitiesVector(FlatBufferBuilder builder, int numElems) { builder.StartVector(4, numElems, 4); }
  public static Offset<QueryEntitiesResponse> EndQueryEntitiesResponse(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<QueryEntitiesResponse>(o);
  }
  public static void FinishQueryEntitiesResponseBuffer(FlatBufferBuilder builder, Offset<QueryEntitiesResponse> offset) { builder.Finish(offset.Value); }
  public static void FinishSizePrefixedQueryEntitiesResponseBuffer(FlatBufferBuilder builder, Offset<QueryEntitiesResponse> offset) { builder.FinishSizePrefixed(offset.Value); }
};


}
