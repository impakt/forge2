// <auto-generated>
//  automatically generated by the FlatBuffers compiler, do not modify
// </auto-generated>

namespace Forge
{

using global::System;
using global::FlatBuffers;

public struct RemoveAnimatedPropertyKeyRequest : IFlatbufferObject
{
  private Table __p;
  public ByteBuffer ByteBuffer { get { return __p.bb; } }
  public static RemoveAnimatedPropertyKeyRequest GetRootAsRemoveAnimatedPropertyKeyRequest(ByteBuffer _bb) { return GetRootAsRemoveAnimatedPropertyKeyRequest(_bb, new RemoveAnimatedPropertyKeyRequest()); }
  public static RemoveAnimatedPropertyKeyRequest GetRootAsRemoveAnimatedPropertyKeyRequest(ByteBuffer _bb, RemoveAnimatedPropertyKeyRequest obj) { return (obj.__assign(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __p.bb_pos = _i; __p.bb = _bb; }
  public RemoveAnimatedPropertyKeyRequest __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public uint EntityId { get { int o = __p.__offset(4); return o != 0 ? __p.bb.GetUint(o + __p.bb_pos) : (uint)0; } }
  public uint PropertyIndex { get { int o = __p.__offset(6); return o != 0 ? __p.bb.GetUint(o + __p.bb_pos) : (uint)0; } }
  public uint KeyIndex { get { int o = __p.__offset(8); return o != 0 ? __p.bb.GetUint(o + __p.bb_pos) : (uint)0; } }

  public static Offset<RemoveAnimatedPropertyKeyRequest> CreateRemoveAnimatedPropertyKeyRequest(FlatBufferBuilder builder,
      uint entityId = 0,
      uint propertyIndex = 0,
      uint keyIndex = 0) {
    builder.StartObject(3);
    RemoveAnimatedPropertyKeyRequest.AddKeyIndex(builder, keyIndex);
    RemoveAnimatedPropertyKeyRequest.AddPropertyIndex(builder, propertyIndex);
    RemoveAnimatedPropertyKeyRequest.AddEntityId(builder, entityId);
    return RemoveAnimatedPropertyKeyRequest.EndRemoveAnimatedPropertyKeyRequest(builder);
  }

  public static void StartRemoveAnimatedPropertyKeyRequest(FlatBufferBuilder builder) { builder.StartObject(3); }
  public static void AddEntityId(FlatBufferBuilder builder, uint entityId) { builder.AddUint(0, entityId, 0); }
  public static void AddPropertyIndex(FlatBufferBuilder builder, uint propertyIndex) { builder.AddUint(1, propertyIndex, 0); }
  public static void AddKeyIndex(FlatBufferBuilder builder, uint keyIndex) { builder.AddUint(2, keyIndex, 0); }
  public static Offset<RemoveAnimatedPropertyKeyRequest> EndRemoveAnimatedPropertyKeyRequest(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<RemoveAnimatedPropertyKeyRequest>(o);
  }
  public static void FinishRemoveAnimatedPropertyKeyRequestBuffer(FlatBufferBuilder builder, Offset<RemoveAnimatedPropertyKeyRequest> offset) { builder.Finish(offset.Value); }
  public static void FinishSizePrefixedRemoveAnimatedPropertyKeyRequestBuffer(FlatBufferBuilder builder, Offset<RemoveAnimatedPropertyKeyRequest> offset) { builder.FinishSizePrefixed(offset.Value); }
};


}
