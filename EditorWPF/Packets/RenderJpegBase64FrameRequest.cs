// <auto-generated>
//  automatically generated by the FlatBuffers compiler, do not modify
// </auto-generated>

namespace Forge
{

using global::System;
using global::FlatBuffers;

public struct RenderJpegBase64FrameRequest : IFlatbufferObject
{
  private Table __p;
  public ByteBuffer ByteBuffer { get { return __p.bb; } }
  public static RenderJpegBase64FrameRequest GetRootAsRenderJpegBase64FrameRequest(ByteBuffer _bb) { return GetRootAsRenderJpegBase64FrameRequest(_bb, new RenderJpegBase64FrameRequest()); }
  public static RenderJpegBase64FrameRequest GetRootAsRenderJpegBase64FrameRequest(ByteBuffer _bb, RenderJpegBase64FrameRequest obj) { return (obj.__assign(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __p.bb_pos = _i; __p.bb = _bb; }
  public RenderJpegBase64FrameRequest __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public float TimeInS { get { int o = __p.__offset(4); return o != 0 ? __p.bb.GetFloat(o + __p.bb_pos) : (float)0.0f; } }
  public uint Quality { get { int o = __p.__offset(6); return o != 0 ? __p.bb.GetUint(o + __p.bb_pos) : (uint)0; } }

  public static Offset<RenderJpegBase64FrameRequest> CreateRenderJpegBase64FrameRequest(FlatBufferBuilder builder,
      float timeInS = 0.0f,
      uint quality = 0) {
    builder.StartObject(2);
    RenderJpegBase64FrameRequest.AddQuality(builder, quality);
    RenderJpegBase64FrameRequest.AddTimeInS(builder, timeInS);
    return RenderJpegBase64FrameRequest.EndRenderJpegBase64FrameRequest(builder);
  }

  public static void StartRenderJpegBase64FrameRequest(FlatBufferBuilder builder) { builder.StartObject(2); }
  public static void AddTimeInS(FlatBufferBuilder builder, float timeInS) { builder.AddFloat(0, timeInS, 0.0f); }
  public static void AddQuality(FlatBufferBuilder builder, uint quality) { builder.AddUint(1, quality, 0); }
  public static Offset<RenderJpegBase64FrameRequest> EndRenderJpegBase64FrameRequest(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<RenderJpegBase64FrameRequest>(o);
  }
  public static void FinishRenderJpegBase64FrameRequestBuffer(FlatBufferBuilder builder, Offset<RenderJpegBase64FrameRequest> offset) { builder.Finish(offset.Value); }
  public static void FinishSizePrefixedRenderJpegBase64FrameRequestBuffer(FlatBufferBuilder builder, Offset<RenderJpegBase64FrameRequest> offset) { builder.FinishSizePrefixed(offset.Value); }
};


}
