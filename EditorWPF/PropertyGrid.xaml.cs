﻿using Editor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EditorWPF
{
    /// <summary>
    /// Interaction logic for PropertyGrid.xaml
    /// </summary>
    public partial class PropertyGrid : UserControl
    {
        public PropertyGrid()
        {
            DataContext = this;
            InitializeComponent();
        }

        private Forge.EntityDescription entity;

        public Forge.PropertyDescription[] EntityProperties { get; set; }
        public ForgeInterface PrivateForgeInterface { get; set; }

        public Forge.EntityDescription Entity
        {
            get => entity;
            set
            {
                entity = value;
                var properties = PrivateForgeInterface.QueryProperties(value.Id);
                EntityProperties = properties;
                stackPanel.Children.Clear();

                // TODO: Binding
                label.Content = entity.Name;

                for (uint i = 0; i < properties.Length; i++)
                {
                    Forge.PropertyDescription property = properties[i];
                    var field = new PropertyField(PrivateForgeInterface, entity, property, i);
                    stackPanel.Children.Add(field);
                }
            }
        }
    }
}
