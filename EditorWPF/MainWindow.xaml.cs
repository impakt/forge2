﻿using Editor;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Forge;

namespace EditorWPF
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private ForgeInterface mForgeInterface;
        private string filteringTerm = "";
        private EntityDescription selectedEntity;
        private Forge.EntityDescription[] entityDescriptions;
        private EntityDescription[] filteredEntityDescriptions;

        public Forge.EntityDescription[] FilteredEntityDescriptions
        {
            get => filteredEntityDescriptions;
            set
            {
                filteredEntityDescriptions = value;
                RaisePropertyChanged();
            }
        }

        public Forge.EntityDescription SelectedEntity
        {
            get => selectedEntity;
            set
            {
                mForgeInterface.SelectEntity(value.Id);
                propertyGrid.Entity = value;
                timeline.SelectedEntity = value;
                RaisePropertyChanged();
            }
        }

        public string FilteringTerm
        {
            get => filteringTerm;
            set
            {
                filteringTerm = value;

                var empty = string.IsNullOrEmpty(value);
                filterGrid.Visibility = empty ? Visibility.Collapsed : Visibility.Visible;

                if (empty)
                {
                    FilteredEntityDescriptions = entityDescriptions;
                }
                else
                {
                    FilteredEntityDescriptions = entityDescriptions.Where(
                            d => d.Id.ToString().Equals(FilteringTerm) || d.Name.ToLower().Contains(FilteringTerm))
                        .ToArray();
                }

                RaisePropertyChanged();
            }
        }

        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string modulePath = null;

#if DEBUG
            const string kDefaultModuleFileName = "Editor_Debug.dll";
#else
            const string kDefaultModuleFileName = "Editor_Release.dll";
#endif

            if (File.Exists(kDefaultModuleFileName))
            {
                modulePath = kDefaultModuleFileName;
            }

            bool exitRequested = false;

            if (modulePath == null)
            {
                OpenFileDialog openFileDialog = new OpenFileDialog
                {
                    Filter = "DLL Files (*.dll)|*.dll|All files (*.*)|*.*",
                    RestoreDirectory = true
                };

                if (openFileDialog.ShowDialog() == true)
                {
                    modulePath = openFileDialog.FileName;
                }
                else
                {
                    exitRequested = true;
                }
            }

            if (modulePath != null)
            {
                mForgeInterface = new ForgeInterface(modulePath, forgeViewport.Handle,
                    (UInt32) forgeViewportHost.ActualWidth, (UInt32) forgeViewportHost.ActualHeight);

                // TODO: Find a common MVVM solution instead of copying around this class
                propertyGrid.PrivateForgeInterface = mForgeInterface;
                timeline.PrivateForgeInterface = mForgeInterface;
            }
            else
            {
                if (exitRequested == false)
                {
                    MessageBox.Show("Failed to load editor module!");
                }

                Application.Current.Shutdown();
            }
        }

        private void PopulateEntities()
        {
            var entities = mForgeInterface.QueryEntities();
            entityDescriptions = entities;
            FilteredEntityDescriptions = entities;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Demo Files (*.demo)|*.demo|All files (*.*)|*.*",
                RestoreDirectory = true
            };

            if (openFileDialog.ShowDialog() == true)
            {
                mForgeInterface.LoadDemo(openFileDialog.FileName);
                PopulateEntities();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // TODO: Cleanly close the engine
        }

        private void listView_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key >= Key.A && e.Key <= Key.Z) || (e.Key >= Key.D0 && e.Key <= Key.D9) ||
                (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
            {
                if (Keyboard.IsKeyDown(Key.LeftCtrl) && e.Key == Key.A)
                {
                    filterTextBox.SelectAll();
                }
                else
                {
                    FilteringTerm += new KeyConverter().ConvertToString(e.Key)?.ToLower();
                }
            }
            else if (e.Key == Key.Escape)
            {
                FilteringTerm = "";
            }
            else if (FilteringTerm.Length > 0 && (e.Key == Key.Delete || e.Key == Key.Back))
            {
                FilteringTerm = FilteringTerm.Substring(0, FilteringTerm.Length - 1);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private void RaisePropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
