﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Editor
{
    public partial class ForgeViewport : UserControl
    {
        public ForgeViewport()
        {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.UserPaint |
                          ControlStyles.AllPaintingInWmPaint |
                          ControlStyles.ResizeRedraw |
                          ControlStyles.ContainerControl |
                          ControlStyles.OptimizedDoubleBuffer |
                          ControlStyles.SupportsTransparentBackColor
                          , true);

            InitializeComponent();
        }

        // Send all keys (including modifiers and TAB) to the control to allow the engine code
        // to handle keyboard input.
        protected override bool IsInputKey(Keys keyData)
        {
            return true;
        }

        // Handle hiding the mouse when the editor is in free-look mode
        // The engine normally handles this, but in the editor setup, it doesn't have control of the mouse cursor.
        private void ForgeViewport_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Cursor.Hide();
            }
        }

        private void ForgeViewport_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Cursor.Show();
            }
        }
    }
}
