// <auto-generated>
//  automatically generated by the FlatBuffers compiler, do not modify
// </auto-generated>

namespace Forge
{

using global::System;
using global::FlatBuffers;

public struct Variant : IFlatbufferObject
{
  private Table __p;
  public ByteBuffer ByteBuffer { get { return __p.bb; } }
  public static Variant GetRootAsVariant(ByteBuffer _bb) { return GetRootAsVariant(_bb, new Variant()); }
  public static Variant GetRootAsVariant(ByteBuffer _bb, Variant obj) { return (obj.__assign(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __p.bb_pos = _i; __p.bb = _bb; }
  public Variant __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public bool Bool { get { int o = __p.__offset(4); return o != 0 ? 0!=__p.bb.Get(o + __p.bb_pos) : (bool)false; } }
  public int Int { get { int o = __p.__offset(6); return o != 0 ? __p.bb.GetInt(o + __p.bb_pos) : (int)0; } }
  public uint Uint { get { int o = __p.__offset(8); return o != 0 ? __p.bb.GetUint(o + __p.bb_pos) : (uint)0; } }
  public float Float { get { int o = __p.__offset(10); return o != 0 ? __p.bb.GetFloat(o + __p.bb_pos) : (float)0.0f; } }
  public Vector3? Vec3 { get { int o = __p.__offset(12); return o != 0 ? (Vector3?)(new Vector3()).__assign(o + __p.bb_pos, __p.bb) : null; } }
  public Vector4? Vec4 { get { int o = __p.__offset(14); return o != 0 ? (Vector4?)(new Vector4()).__assign(o + __p.bb_pos, __p.bb) : null; } }
  public string String { get { int o = __p.__offset(16); return o != 0 ? __p.__string(o + __p.bb_pos) : null; } }
#if ENABLE_SPAN_T
  public Span<byte> GetStringBytes() { return __p.__vector_as_span(16); }
#else
  public ArraySegment<byte>? GetStringBytes() { return __p.__vector_as_arraysegment(16); }
#endif
  public byte[] GetStringArray() { return __p.__vector_as_array<byte>(16); }

  public static void StartVariant(FlatBufferBuilder builder) { builder.StartObject(7); }
  public static void AddBool(FlatBufferBuilder builder, bool boolVal) { builder.AddBool(0, boolVal, false); }
  public static void AddInt(FlatBufferBuilder builder, int intVal) { builder.AddInt(1, intVal, 0); }
  public static void AddUint(FlatBufferBuilder builder, uint uintVal) { builder.AddUint(2, uintVal, 0); }
  public static void AddFloat(FlatBufferBuilder builder, float floatVal) { builder.AddFloat(3, floatVal, 0.0f); }
  public static void AddVec3(FlatBufferBuilder builder, Offset<Vector3> vec3Offset) { builder.AddStruct(4, vec3Offset.Value, 0); }
  public static void AddVec4(FlatBufferBuilder builder, Offset<Vector4> vec4Offset) { builder.AddStruct(5, vec4Offset.Value, 0); }
  public static void AddString(FlatBufferBuilder builder, StringOffset stringOffset) { builder.AddOffset(6, stringOffset.Value, 0); }
  public static Offset<Variant> EndVariant(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<Variant>(o);
  }
};


}
