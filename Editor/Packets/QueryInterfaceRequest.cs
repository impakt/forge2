// <auto-generated>
//  automatically generated by the FlatBuffers compiler, do not modify
// </auto-generated>

namespace Forge
{

using global::System;
using global::FlatBuffers;

public struct QueryInterfaceRequest : IFlatbufferObject
{
  private Table __p;
  public ByteBuffer ByteBuffer { get { return __p.bb; } }
  public static QueryInterfaceRequest GetRootAsQueryInterfaceRequest(ByteBuffer _bb) { return GetRootAsQueryInterfaceRequest(_bb, new QueryInterfaceRequest()); }
  public static QueryInterfaceRequest GetRootAsQueryInterfaceRequest(ByteBuffer _bb, QueryInterfaceRequest obj) { return (obj.__assign(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __p.bb_pos = _i; __p.bb = _bb; }
  public QueryInterfaceRequest __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }


  public static void StartQueryInterfaceRequest(FlatBufferBuilder builder) { builder.StartObject(0); }
  public static Offset<QueryInterfaceRequest> EndQueryInterfaceRequest(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<QueryInterfaceRequest>(o);
  }
  public static void FinishQueryInterfaceRequestBuffer(FlatBufferBuilder builder, Offset<QueryInterfaceRequest> offset) { builder.Finish(offset.Value); }
  public static void FinishSizePrefixedQueryInterfaceRequestBuffer(FlatBufferBuilder builder, Offset<QueryInterfaceRequest> offset) { builder.FinishSizePrefixed(offset.Value); }
};


}
