// <auto-generated>
//  automatically generated by the FlatBuffers compiler, do not modify
// </auto-generated>

namespace Forge
{

using global::System;
using global::FlatBuffers;

public struct EntityDescription : IFlatbufferObject
{
  private Table __p;
  public ByteBuffer ByteBuffer { get { return __p.bb; } }
  public static EntityDescription GetRootAsEntityDescription(ByteBuffer _bb) { return GetRootAsEntityDescription(_bb, new EntityDescription()); }
  public static EntityDescription GetRootAsEntityDescription(ByteBuffer _bb, EntityDescription obj) { return (obj.__assign(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __p.bb_pos = _i; __p.bb = _bb; }
  public EntityDescription __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public uint Id { get { int o = __p.__offset(4); return o != 0 ? __p.bb.GetUint(o + __p.bb_pos) : (uint)0; } }
  public uint TypeIndex { get { int o = __p.__offset(6); return o != 0 ? __p.bb.GetUint(o + __p.bb_pos) : (uint)0; } }
  public string Name { get { int o = __p.__offset(8); return o != 0 ? __p.__string(o + __p.bb_pos) : null; } }
#if ENABLE_SPAN_T
  public Span<byte> GetNameBytes() { return __p.__vector_as_span(8); }
#else
  public ArraySegment<byte>? GetNameBytes() { return __p.__vector_as_arraysegment(8); }
#endif
  public byte[] GetNameArray() { return __p.__vector_as_array<byte>(8); }

  public static Offset<EntityDescription> CreateEntityDescription(FlatBufferBuilder builder,
      uint id = 0,
      uint typeIndex = 0,
      StringOffset nameOffset = default(StringOffset)) {
    builder.StartObject(3);
    EntityDescription.AddName(builder, nameOffset);
    EntityDescription.AddTypeIndex(builder, typeIndex);
    EntityDescription.AddId(builder, id);
    return EntityDescription.EndEntityDescription(builder);
  }

  public static void StartEntityDescription(FlatBufferBuilder builder) { builder.StartObject(3); }
  public static void AddId(FlatBufferBuilder builder, uint id) { builder.AddUint(0, id, 0); }
  public static void AddTypeIndex(FlatBufferBuilder builder, uint typeIndex) { builder.AddUint(1, typeIndex, 0); }
  public static void AddName(FlatBufferBuilder builder, StringOffset nameOffset) { builder.AddOffset(2, nameOffset.Value, 0); }
  public static Offset<EntityDescription> EndEntityDescription(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<EntityDescription>(o);
  }
  public static void FinishEntityDescriptionBuffer(FlatBufferBuilder builder, Offset<EntityDescription> offset) { builder.Finish(offset.Value); }
  public static void FinishSizePrefixedEntityDescriptionBuffer(FlatBufferBuilder builder, Offset<EntityDescription> offset) { builder.FinishSizePrefixed(offset.Value); }
};


}
