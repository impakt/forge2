﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Editor
{
    public partial class MainWindow : Form
    {
        private ForgeInterface mForgeInterface;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            string modulePath = null;

#if DEBUG
            const string kDefaultModuleFileName = "Editor_Debug.dll";
#else
            const string kDefaultModuleFileName = "Editor_Release.dll";
#endif

            if (File.Exists(kDefaultModuleFileName))
            {
                modulePath = kDefaultModuleFileName;
            }

            bool exitRequested = false;

            if (modulePath == null)
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "DLL Files (*.dll)|*.dll|All files (*.*)|*.*";
                openFileDialog.RestoreDirectory = true;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    modulePath = openFileDialog.FileName;
                }
                else
                {
                    exitRequested = true;
                }
            }

            if (modulePath != null)
            {
                mForgeInterface = new ForgeInterface(modulePath, forgeViewport1.Handle, (UInt32)forgeViewport1.Width, (UInt32)forgeViewport1.Height);
            }
            else
            {
                if (exitRequested == false)
                {
                    MessageBox.Show("Failed to load editor module!");
                }

                Application.Exit();
            }
        }

        private void PopulateEntities()
        {
            var entities = mForgeInterface.QueryEntities();
            foreach (var entity in entities)
            {
                listBox1.Items.Add(entity);
            }
        }

        private void openDemoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Demo Files (*.demo)|*.demo|All files (*.*)|*.*";
            openFileDialog.RestoreDirectory = true;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                mForgeInterface.LoadDemo(openFileDialog.FileName);

                PopulateEntities();
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                Forge.EntityDescription entityDescription = (Forge.EntityDescription)listBox1.Items[listBox1.SelectedIndex];

                var properties = mForgeInterface.QueryProperties(entityDescription.Id);
                if (properties != null)
                {
                    propertyGrid1.SelectedObject = properties;
                }
            }
        }
    }
}
