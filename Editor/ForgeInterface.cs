﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Threading;

namespace Editor
{
    class ForgeInterface
    {
        private Assembly mEditorAssembly;
        private object mEditor;
        private MethodInfo mSendPacketFunc;
        private EventInfo mOnPacketReceivedEvent;

        private FlatBuffers.FlatBufferBuilder mBuilder = new FlatBuffers.FlatBufferBuilder(1024);
        private SemaphoreSlim mSemaphore = new SemaphoreSlim(0, 1);
        private Forge.Packet mPacket;

        private void OnPacketReceived(byte[] packetData)
        {
            FlatBuffers.ByteBuffer byteBuffer = new FlatBuffers.ByteBuffer(packetData);
            mPacket = Forge.Packet.GetRootAsPacket(byteBuffer);
            mSemaphore.Release();
        }

        public void SendPacket(byte[] packetData)
        {
            mSendPacketFunc.Invoke(mEditor, new object[] { packetData } );
        }

        public ForgeInterface(string modulePath, IntPtr windowHandle, UInt32 width, UInt32 height)
        {
            mEditorAssembly = Assembly.LoadFrom(modulePath);

            var editorType = mEditorAssembly.GetType("Stiletto.Editor");
            mSendPacketFunc = editorType.GetMethod("SendPacket");
            mOnPacketReceivedEvent = editorType.GetEvent("OnPacketReceived");

            mEditor = System.Activator.CreateInstance(editorType, windowHandle, width, height);

            MethodInfo methodInfo = typeof(ForgeInterface).GetMethod("OnPacketReceived", BindingFlags.NonPublic | BindingFlags.Instance);
            Delegate packetReceivedDelegate = Delegate.CreateDelegate(mOnPacketReceivedEvent.EventHandlerType, this, methodInfo);
            mOnPacketReceivedEvent.AddEventHandler(mEditor, packetReceivedDelegate);
        }

        ~ForgeInterface()
        {
            ((IDisposable)mEditor).Dispose();
        }

        private FlatBuffers.FlatBufferBuilder BeginRequest()
        {
            mBuilder.Clear();

            return mBuilder;
        }

        private Forge.Packet EndRequest(FlatBuffers.FlatBufferBuilder builder, int rootTable)
        {
            builder.Finish(rootTable);

            SendPacket(builder.SizedByteArray());
            mSemaphore.Wait();

            return mPacket;
        }

        public bool LoadDemo(string path)
        {
            var builder = BeginRequest();

            var payloadOffset = Forge.LoadDemoRequest.CreateLoadDemoRequest(builder, builder.CreateString(path));
            var packetOffset = Forge.Packet.CreatePacket(builder, Forge.PacketPayload.LoadDemoRequest, payloadOffset.Value);

            Forge.Packet packet = EndRequest(builder, packetOffset.Value);

            Forge.LoadDemoResponse response = packet.Payload<Forge.LoadDemoResponse>().GetValueOrDefault();
            return (response.Result == Forge.Result.Success);
        }

        public Forge.EntityDescription[] QueryEntities()
        {
            var builder = BeginRequest();

            var packetOffset = Forge.Packet.CreatePacket(builder, Forge.PacketPayload.QueryEntitiesRequest);

            Forge.Packet packet = EndRequest(builder, packetOffset.Value);

            Forge.QueryEntitiesResponse response = packet.Payload<Forge.QueryEntitiesResponse>().GetValueOrDefault();

            Forge.EntityDescription[] entities = new Forge.EntityDescription[response.EntitiesLength];
            for (int i = 0; i < response.EntitiesLength; ++i)
            {
                entities[i] = response.Entities(i).GetValueOrDefault();
            }

            return entities;
        }
        public Forge.PropertyDescription[] QueryProperties(uint entityId)
        {
            var builder = BeginRequest();

            var payloadOffset = Forge.QueryPropertiesRequest.CreateQueryPropertiesRequest(builder, entityId);
            var packetOffset = Forge.Packet.CreatePacket(builder, Forge.PacketPayload.QueryPropertiesRequest, payloadOffset.Value);

            Forge.Packet packet = EndRequest(builder, packetOffset.Value);

            Forge.QueryPropertiesResponse response = packet.Payload<Forge.QueryPropertiesResponse>().GetValueOrDefault();

            Forge.PropertyDescription[] properties = null;

            if (response.Result == Forge.Result.Success)
            {
                properties = new Forge.PropertyDescription[response.PropertiesLength];
                for (int i = 0; i < response.PropertiesLength; ++i)
                {
                    properties[i] = response.Properties(i).GetValueOrDefault();
                }
            }

            return properties;
        }
}
}
