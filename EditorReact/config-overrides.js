const { addBabelPlugins, override } = require('customize-cra');

// https://github.com/arackaf/customize-cra#available-plugins

module.exports = override(
  ...addBabelPlugins(
    '@babel/proposal-optional-chaining',
    '@babel/proposal-nullish-coalescing-operator',
  ),
);
