import { flatbuffers } from "flatbuffers";
/**
 * @enum
 */
export declare namespace Forge {
    enum Result {
        Success = 0,
        Error = 1
    }
}
/**
 * @enum
 */
export declare namespace Forge {
    enum PropertyData {
        NONE = 0,
        BoolProperty = 1,
        IntegerProperty = 2,
        UnsignedIntegerProperty = 3,
        FloatProperty = 4,
        Vector3Property = 5,
        Vector4Property = 6,
        StringProperty = 7
    }
}
/**
 * @enum
 */
export declare namespace Forge {
    enum TimelineKeyData {
        NONE = 0,
        BooleanTimelineKey = 1,
        IntegerTimelineKey = 2,
        UnsignedIntegerTimelineKey = 3,
        FloatTimelineKey = 4,
        Vector3TimelineKey = 5,
        QuaternionTimelineKey = 6
    }
}
/**
 * @enum
 */
export declare namespace Forge {
    enum TimelineData {
        NONE = 0,
        BooleanTimeline = 1,
        IntegerTimeline = 2,
        UnsignedIntegerTimeline = 3,
        FloatTimeline = 4,
        Vector3Timeline = 5,
        QuaternionTimeline = 6
    }
}
/**
 * @enum
 */
export declare namespace Forge {
    enum TextureFormat {
        Unknown = 0,
        RGB_UNORM = 1,
        RGB_SRGB = 2,
        RGBA_UNORM = 3,
        RGBA_SRGB = 4,
        BC1_RGB_UNORM = 5,
        BC1_RGB_SRGB = 6,
        BC1_RGBA_UNORM = 7,
        BC1_RGBA_SRGB = 8,
        BC2_RGBA_UNORM = 9,
        BC2_RGBA_SRGB = 10,
        BC3_RGBA_UNORM = 11,
        BC3_RGBA_SRGB = 12,
        BC4_R_UNORM = 13,
        BC4_R_SNORM = 14,
        BC5_RG_UNORM = 15,
        BC5_RG_SNORM = 16,
        BC6_RGB_UFLOAT = 17,
        BC6_RGB_SFLOAT = 18,
        BC7_RGB_UNORM = 19,
        BC7_RGB_SRGB = 20,
        BC7_RGBA_UNORM = 21,
        BC7_RGBA_SRGB = 22
    }
}
/**
 * @enum
 */
export declare namespace Forge {
    enum TextureConversionQuality {
        Unknown = 0,
        Low = 1,
        Medium = 2,
        High = 3
    }
}
/**
 * @enum
 */
export declare namespace Forge {
    enum PacketPayload {
        NONE = 0,
        QueryInterfaceRequest = 1,
        QueryInterfaceResponse = 2,
        ModifyViewportNotification = 3,
        LoadDemoRequest = 4,
        LoadDemoResponse = 5,
        CreateEntityRequest = 6,
        CreateEntityResponse = 7,
        DestroyEntityRequest = 8,
        DestroyEntityResponse = 9,
        QueryEntitiesRequest = 10,
        QueryEntitiesResponse = 11,
        QueryPropertiesRequest = 12,
        QueryPropertiesResponse = 13,
        QueryAnimatedPropertiesRequest = 14,
        QueryAnimatedPropertiesResponse = 15,
        TogglePlaybackNotification = 16,
        RenderFrameRequest = 17,
        RenderFrameResponse = 18,
        ModifyPropertyRequest = 19,
        ModifyPropertyResponse = 20,
        ModifyAnimatedPropertyKeyRequest = 21,
        ModifyAnimatedPropertyKeyResponse = 22,
        AddAnimatedPropertyKeyRequest = 23,
        AddAnimatedPropertyKeyResponse = 24,
        RemoveAnimatedPropertyKeyRequest = 25,
        RemoveAnimatedPropertyKeyResponse = 26,
        SaveDemoRequest = 27,
        SaveDemoResponse = 28,
        QueryDemoStateRequest = 29,
        QueryDemoStateResponse = 30,
        UpdateFreeLookCameraNotification = 31,
        ConvertMeshRequest = 32,
        ConvertMeshResponse = 33,
        ConvertTextureRequest = 34,
        ConvertTextureResponse = 35,
        QueryAudioTrackRequest = 36,
        QueryAudioTrackResponse = 37,
        LoadAudioTrackRequest = 38,
        LoadAudioTrackResponse = 39,
        ModifyDemoPropertiesNotification = 40,
        RenameEntityRequest = 41,
        RenameEntityResponse = 42,
        ToggleFreeLookNotification = 43,
        QuerySelectedEntityRequest = 44,
        QuerySelectedEntityResponse = 45,
        SelectEntityRequest = 46,
        SelectEntityResponse = 47,
        QuerySelectionRayRequest = 48,
        QuerySelectionRayResponse = 49,
        RenderJpegFrameRequest = 50,
        RenderJpegFrameResponse = 51,
        ReloadResourcesRequest = 52,
        ReloadResourcesResponse = 53,
        QueryEntityTypesRequest = 54,
        QueryEntityTypesResponse = 55,
        RenderJpegBase64FrameRequest = 56,
        RenderJpegBase64FrameResponse = 57
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class Vector2 {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns Vector2
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): Vector2;
        /**
         * @returns number
         */
        x(): number;
        /**
         * @returns number
         */
        y(): number;
        /**
         * @param flatbuffers.Builder builder
         * @param number x
         * @param number y
         * @returns flatbuffers.Offset
         */
        static createVector2(builder: flatbuffers.Builder, x: number, y: number): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class Vector3 {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns Vector3
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): Vector3;
        /**
         * @returns number
         */
        x(): number;
        /**
         * @returns number
         */
        y(): number;
        /**
         * @returns number
         */
        z(): number;
        /**
         * @param flatbuffers.Builder builder
         * @param number x
         * @param number y
         * @param number z
         * @returns flatbuffers.Offset
         */
        static createVector3(builder: flatbuffers.Builder, x: number, y: number, z: number): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class Vector4 {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns Vector4
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): Vector4;
        /**
         * @returns number
         */
        x(): number;
        /**
         * @returns number
         */
        y(): number;
        /**
         * @returns number
         */
        z(): number;
        /**
         * @returns number
         */
        w(): number;
        /**
         * @param flatbuffers.Builder builder
         * @param number x
         * @param number y
         * @param number z
         * @param number w
         * @returns flatbuffers.Offset
         */
        static createVector4(builder: flatbuffers.Builder, x: number, y: number, z: number, w: number): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class Quaternion {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns Quaternion
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): Quaternion;
        /**
         * @returns number
         */
        x(): number;
        /**
         * @returns number
         */
        y(): number;
        /**
         * @returns number
         */
        z(): number;
        /**
         * @returns number
         */
        w(): number;
        /**
         * @param flatbuffers.Builder builder
         * @param number x
         * @param number y
         * @param number z
         * @param number w
         * @returns flatbuffers.Offset
         */
        static createQuaternion(builder: flatbuffers.Builder, x: number, y: number, z: number, w: number): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class BoolProperty {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns BoolProperty
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): BoolProperty;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param BoolProperty= obj
         * @returns BoolProperty
         */
        static getRootAsBoolProperty(bb: flatbuffers.ByteBuffer, obj?: BoolProperty): BoolProperty;
        /**
         * @returns boolean
         */
        value(): boolean;
        /**
         * @param flatbuffers.Builder builder
         */
        static startBoolProperty(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param boolean value
         */
        static addValue(builder: flatbuffers.Builder, value: boolean): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endBoolProperty(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class IntegerProperty {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns IntegerProperty
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): IntegerProperty;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param IntegerProperty= obj
         * @returns IntegerProperty
         */
        static getRootAsIntegerProperty(bb: flatbuffers.ByteBuffer, obj?: IntegerProperty): IntegerProperty;
        /**
         * @returns number
         */
        value(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startIntegerProperty(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number value
         */
        static addValue(builder: flatbuffers.Builder, value: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endIntegerProperty(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class UnsignedIntegerProperty {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns UnsignedIntegerProperty
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): UnsignedIntegerProperty;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param UnsignedIntegerProperty= obj
         * @returns UnsignedIntegerProperty
         */
        static getRootAsUnsignedIntegerProperty(bb: flatbuffers.ByteBuffer, obj?: UnsignedIntegerProperty): UnsignedIntegerProperty;
        /**
         * @returns number
         */
        value(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startUnsignedIntegerProperty(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number value
         */
        static addValue(builder: flatbuffers.Builder, value: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endUnsignedIntegerProperty(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class FloatProperty {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns FloatProperty
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): FloatProperty;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param FloatProperty= obj
         * @returns FloatProperty
         */
        static getRootAsFloatProperty(bb: flatbuffers.ByteBuffer, obj?: FloatProperty): FloatProperty;
        /**
         * @returns number
         */
        value(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startFloatProperty(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number value
         */
        static addValue(builder: flatbuffers.Builder, value: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endFloatProperty(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class Vector3Property {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns Vector3Property
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): Vector3Property;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param Vector3Property= obj
         * @returns Vector3Property
         */
        static getRootAsVector3Property(bb: flatbuffers.ByteBuffer, obj?: Vector3Property): Vector3Property;
        /**
         * @param Forge.Vector3= obj
         * @returns Forge.Vector3|null
         */
        value(obj?: Forge.Vector3): Forge.Vector3 | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startVector3Property(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset valueOffset
         */
        static addValue(builder: flatbuffers.Builder, valueOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endVector3Property(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class Vector4Property {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns Vector4Property
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): Vector4Property;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param Vector4Property= obj
         * @returns Vector4Property
         */
        static getRootAsVector4Property(bb: flatbuffers.ByteBuffer, obj?: Vector4Property): Vector4Property;
        /**
         * @param Forge.Vector4= obj
         * @returns Forge.Vector4|null
         */
        value(obj?: Forge.Vector4): Forge.Vector4 | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startVector4Property(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset valueOffset
         */
        static addValue(builder: flatbuffers.Builder, valueOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endVector4Property(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class StringProperty {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns StringProperty
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): StringProperty;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param StringProperty= obj
         * @returns StringProperty
         */
        static getRootAsStringProperty(bb: flatbuffers.ByteBuffer, obj?: StringProperty): StringProperty;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        value(): string | null;
        value(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startStringProperty(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset valueOffset
         */
        static addValue(builder: flatbuffers.Builder, valueOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endStringProperty(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class BooleanTimelineKey {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns BooleanTimelineKey
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): BooleanTimelineKey;
        /**
         * @returns number
         */
        time(): number;
        /**
         * @returns boolean
         */
        value(): boolean;
        /**
         * @param flatbuffers.Builder builder
         * @param number time
         * @param boolean value
         * @returns flatbuffers.Offset
         */
        static createBooleanTimelineKey(builder: flatbuffers.Builder, time: number, value: boolean): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class IntegerTimelineKey {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns IntegerTimelineKey
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): IntegerTimelineKey;
        /**
         * @returns number
         */
        time(): number;
        /**
         * @returns number
         */
        value(): number;
        /**
         * @param flatbuffers.Builder builder
         * @param number time
         * @param number value
         * @returns flatbuffers.Offset
         */
        static createIntegerTimelineKey(builder: flatbuffers.Builder, time: number, value: number): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class UnsignedIntegerTimelineKey {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns UnsignedIntegerTimelineKey
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): UnsignedIntegerTimelineKey;
        /**
         * @returns number
         */
        time(): number;
        /**
         * @returns number
         */
        value(): number;
        /**
         * @param flatbuffers.Builder builder
         * @param number time
         * @param number value
         * @returns flatbuffers.Offset
         */
        static createUnsignedIntegerTimelineKey(builder: flatbuffers.Builder, time: number, value: number): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class FloatTimelineKey {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns FloatTimelineKey
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): FloatTimelineKey;
        /**
         * @returns number
         */
        time(): number;
        /**
         * @returns number
         */
        value(): number;
        /**
         * @param flatbuffers.Builder builder
         * @param number time
         * @param number value
         * @returns flatbuffers.Offset
         */
        static createFloatTimelineKey(builder: flatbuffers.Builder, time: number, value: number): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class Vector3TimelineKey {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns Vector3TimelineKey
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): Vector3TimelineKey;
        /**
         * @returns number
         */
        time(): number;
        /**
         * @param Forge.Vector3= obj
         * @returns Forge.Vector3|null
         */
        value(obj?: Forge.Vector3): Forge.Vector3 | null;
        /**
         * @param flatbuffers.Builder builder
         * @param number time
         * @param number value_x
         * @param number value_y
         * @param number value_z
         * @returns flatbuffers.Offset
         */
        static createVector3TimelineKey(builder: flatbuffers.Builder, time: number, value_x: number, value_y: number, value_z: number): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QuaternionTimelineKey {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QuaternionTimelineKey
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QuaternionTimelineKey;
        /**
         * @returns number
         */
        time(): number;
        /**
         * @param Forge.Quaternion= obj
         * @returns Forge.Quaternion|null
         */
        value(obj?: Forge.Quaternion): Forge.Quaternion | null;
        /**
         * @param flatbuffers.Builder builder
         * @param number time
         * @param number value_x
         * @param number value_y
         * @param number value_z
         * @param number value_w
         * @returns flatbuffers.Offset
         */
        static createQuaternionTimelineKey(builder: flatbuffers.Builder, time: number, value_x: number, value_y: number, value_z: number, value_w: number): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QueryInterfaceRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QueryInterfaceRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QueryInterfaceRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QueryInterfaceRequest= obj
         * @returns QueryInterfaceRequest
         */
        static getRootAsQueryInterfaceRequest(bb: flatbuffers.ByteBuffer, obj?: QueryInterfaceRequest): QueryInterfaceRequest;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQueryInterfaceRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQueryInterfaceRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QueryInterfaceResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QueryInterfaceResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QueryInterfaceResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QueryInterfaceResponse= obj
         * @returns QueryInterfaceResponse
         */
        static getRootAsQueryInterfaceResponse(bb: flatbuffers.ByteBuffer, obj?: QueryInterfaceResponse): QueryInterfaceResponse;
        /**
         * @returns number
         */
        version(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQueryInterfaceResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number version
         */
        static addVersion(builder: flatbuffers.Builder, version: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQueryInterfaceResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class ModifyViewportNotification {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns ModifyViewportNotification
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): ModifyViewportNotification;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param ModifyViewportNotification= obj
         * @returns ModifyViewportNotification
         */
        static getRootAsModifyViewportNotification(bb: flatbuffers.ByteBuffer, obj?: ModifyViewportNotification): ModifyViewportNotification;
        /**
         * @returns number
         */
        width(): number;
        /**
         * @returns number
         */
        height(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startModifyViewportNotification(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number width
         */
        static addWidth(builder: flatbuffers.Builder, width: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number height
         */
        static addHeight(builder: flatbuffers.Builder, height: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endModifyViewportNotification(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class LoadDemoRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns LoadDemoRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): LoadDemoRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param LoadDemoRequest= obj
         * @returns LoadDemoRequest
         */
        static getRootAsLoadDemoRequest(bb: flatbuffers.ByteBuffer, obj?: LoadDemoRequest): LoadDemoRequest;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        path(): string | null;
        path(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startLoadDemoRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset pathOffset
         */
        static addPath(builder: flatbuffers.Builder, pathOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endLoadDemoRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class LoadDemoResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns LoadDemoResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): LoadDemoResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param LoadDemoResponse= obj
         * @returns LoadDemoResponse
         */
        static getRootAsLoadDemoResponse(bb: flatbuffers.ByteBuffer, obj?: LoadDemoResponse): LoadDemoResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param flatbuffers.Builder builder
         */
        static startLoadDemoResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endLoadDemoResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class CreateEntityRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns CreateEntityRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): CreateEntityRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param CreateEntityRequest= obj
         * @returns CreateEntityRequest
         */
        static getRootAsCreateEntityRequest(bb: flatbuffers.ByteBuffer, obj?: CreateEntityRequest): CreateEntityRequest;
        /**
         * @returns number
         */
        typeIndex(): number;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        name(): string | null;
        name(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startCreateEntityRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number typeIndex
         */
        static addTypeIndex(builder: flatbuffers.Builder, typeIndex: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset nameOffset
         */
        static addName(builder: flatbuffers.Builder, nameOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endCreateEntityRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class CreateEntityResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns CreateEntityResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): CreateEntityResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param CreateEntityResponse= obj
         * @returns CreateEntityResponse
         */
        static getRootAsCreateEntityResponse(bb: flatbuffers.ByteBuffer, obj?: CreateEntityResponse): CreateEntityResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @returns number
         */
        id(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startCreateEntityResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number id
         */
        static addId(builder: flatbuffers.Builder, id: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endCreateEntityResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class DestroyEntityRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns DestroyEntityRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): DestroyEntityRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param DestroyEntityRequest= obj
         * @returns DestroyEntityRequest
         */
        static getRootAsDestroyEntityRequest(bb: flatbuffers.ByteBuffer, obj?: DestroyEntityRequest): DestroyEntityRequest;
        /**
         * @returns number
         */
        entityId(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startDestroyEntityRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number entityId
         */
        static addEntityId(builder: flatbuffers.Builder, entityId: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endDestroyEntityRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class DestroyEntityResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns DestroyEntityResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): DestroyEntityResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param DestroyEntityResponse= obj
         * @returns DestroyEntityResponse
         */
        static getRootAsDestroyEntityResponse(bb: flatbuffers.ByteBuffer, obj?: DestroyEntityResponse): DestroyEntityResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param flatbuffers.Builder builder
         */
        static startDestroyEntityResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endDestroyEntityResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QueryEntitiesRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QueryEntitiesRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QueryEntitiesRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QueryEntitiesRequest= obj
         * @returns QueryEntitiesRequest
         */
        static getRootAsQueryEntitiesRequest(bb: flatbuffers.ByteBuffer, obj?: QueryEntitiesRequest): QueryEntitiesRequest;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQueryEntitiesRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQueryEntitiesRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class EntityDescription {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns EntityDescription
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): EntityDescription;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param EntityDescription= obj
         * @returns EntityDescription
         */
        static getRootAsEntityDescription(bb: flatbuffers.ByteBuffer, obj?: EntityDescription): EntityDescription;
        /**
         * @returns number
         */
        id(): number;
        /**
         * @returns number
         */
        typeIndex(): number;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        name(): string | null;
        name(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startEntityDescription(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number id
         */
        static addId(builder: flatbuffers.Builder, id: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number typeIndex
         */
        static addTypeIndex(builder: flatbuffers.Builder, typeIndex: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset nameOffset
         */
        static addName(builder: flatbuffers.Builder, nameOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endEntityDescription(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QueryEntitiesResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QueryEntitiesResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QueryEntitiesResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QueryEntitiesResponse= obj
         * @returns QueryEntitiesResponse
         */
        static getRootAsQueryEntitiesResponse(bb: flatbuffers.ByteBuffer, obj?: QueryEntitiesResponse): QueryEntitiesResponse;
        /**
         * @param number index
         * @param Forge.EntityDescription= obj
         * @returns Forge.EntityDescription
         */
        entities(index: number, obj?: Forge.EntityDescription): Forge.EntityDescription | null;
        /**
         * @returns number
         */
        entitiesLength(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQueryEntitiesResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset entitiesOffset
         */
        static addEntities(builder: flatbuffers.Builder, entitiesOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Array.<flatbuffers.Offset> data
         * @returns flatbuffers.Offset
         */
        static createEntitiesVector(builder: flatbuffers.Builder, data: flatbuffers.Offset[]): flatbuffers.Offset;
        /**
         * @param flatbuffers.Builder builder
         * @param number numElems
         */
        static startEntitiesVector(builder: flatbuffers.Builder, numElems: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQueryEntitiesResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QueryPropertiesRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QueryPropertiesRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QueryPropertiesRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QueryPropertiesRequest= obj
         * @returns QueryPropertiesRequest
         */
        static getRootAsQueryPropertiesRequest(bb: flatbuffers.ByteBuffer, obj?: QueryPropertiesRequest): QueryPropertiesRequest;
        /**
         * @returns number
         */
        entityId(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQueryPropertiesRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number entityId
         */
        static addEntityId(builder: flatbuffers.Builder, entityId: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQueryPropertiesRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class PropertyDescription {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns PropertyDescription
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): PropertyDescription;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param PropertyDescription= obj
         * @returns PropertyDescription
         */
        static getRootAsPropertyDescription(bb: flatbuffers.ByteBuffer, obj?: PropertyDescription): PropertyDescription;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        name(): string | null;
        name(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        description(): string | null;
        description(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @returns Forge.PropertyData
         */
        valueType(): Forge.PropertyData;
        /**
         * @param flatbuffers.Table obj
         * @returns ?flatbuffers.Table
         */
        value<T extends flatbuffers.Table>(obj: T): T | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startPropertyDescription(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset nameOffset
         */
        static addName(builder: flatbuffers.Builder, nameOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset descriptionOffset
         */
        static addDescription(builder: flatbuffers.Builder, descriptionOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.PropertyData valueType
         */
        static addValueType(builder: flatbuffers.Builder, valueType: Forge.PropertyData): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset valueOffset
         */
        static addValue(builder: flatbuffers.Builder, valueOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endPropertyDescription(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QueryPropertiesResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QueryPropertiesResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QueryPropertiesResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QueryPropertiesResponse= obj
         * @returns QueryPropertiesResponse
         */
        static getRootAsQueryPropertiesResponse(bb: flatbuffers.ByteBuffer, obj?: QueryPropertiesResponse): QueryPropertiesResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param number index
         * @param Forge.PropertyDescription= obj
         * @returns Forge.PropertyDescription
         */
        properties(index: number, obj?: Forge.PropertyDescription): Forge.PropertyDescription | null;
        /**
         * @returns number
         */
        propertiesLength(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQueryPropertiesResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset propertiesOffset
         */
        static addProperties(builder: flatbuffers.Builder, propertiesOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Array.<flatbuffers.Offset> data
         * @returns flatbuffers.Offset
         */
        static createPropertiesVector(builder: flatbuffers.Builder, data: flatbuffers.Offset[]): flatbuffers.Offset;
        /**
         * @param flatbuffers.Builder builder
         * @param number numElems
         */
        static startPropertiesVector(builder: flatbuffers.Builder, numElems: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQueryPropertiesResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QueryAnimatedPropertiesRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QueryAnimatedPropertiesRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QueryAnimatedPropertiesRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QueryAnimatedPropertiesRequest= obj
         * @returns QueryAnimatedPropertiesRequest
         */
        static getRootAsQueryAnimatedPropertiesRequest(bb: flatbuffers.ByteBuffer, obj?: QueryAnimatedPropertiesRequest): QueryAnimatedPropertiesRequest;
        /**
         * @returns number
         */
        entityId(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQueryAnimatedPropertiesRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number entityId
         */
        static addEntityId(builder: flatbuffers.Builder, entityId: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQueryAnimatedPropertiesRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class BooleanTimeline {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns BooleanTimeline
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): BooleanTimeline;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param BooleanTimeline= obj
         * @returns BooleanTimeline
         */
        static getRootAsBooleanTimeline(bb: flatbuffers.ByteBuffer, obj?: BooleanTimeline): BooleanTimeline;
        /**
         * @param number index
         * @param Forge.BooleanTimelineKey= obj
         * @returns Forge.BooleanTimelineKey
         */
        keys(index: number, obj?: Forge.BooleanTimelineKey): Forge.BooleanTimelineKey | null;
        /**
         * @returns number
         */
        keysLength(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startBooleanTimeline(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset keysOffset
         */
        static addKeys(builder: flatbuffers.Builder, keysOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number numElems
         */
        static startKeysVector(builder: flatbuffers.Builder, numElems: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endBooleanTimeline(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class IntegerTimeline {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns IntegerTimeline
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): IntegerTimeline;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param IntegerTimeline= obj
         * @returns IntegerTimeline
         */
        static getRootAsIntegerTimeline(bb: flatbuffers.ByteBuffer, obj?: IntegerTimeline): IntegerTimeline;
        /**
         * @param number index
         * @param Forge.IntegerTimelineKey= obj
         * @returns Forge.IntegerTimelineKey
         */
        keys(index: number, obj?: Forge.IntegerTimelineKey): Forge.IntegerTimelineKey | null;
        /**
         * @returns number
         */
        keysLength(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startIntegerTimeline(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset keysOffset
         */
        static addKeys(builder: flatbuffers.Builder, keysOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number numElems
         */
        static startKeysVector(builder: flatbuffers.Builder, numElems: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endIntegerTimeline(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class UnsignedIntegerTimeline {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns UnsignedIntegerTimeline
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): UnsignedIntegerTimeline;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param UnsignedIntegerTimeline= obj
         * @returns UnsignedIntegerTimeline
         */
        static getRootAsUnsignedIntegerTimeline(bb: flatbuffers.ByteBuffer, obj?: UnsignedIntegerTimeline): UnsignedIntegerTimeline;
        /**
         * @param number index
         * @param Forge.UnsignedIntegerTimelineKey= obj
         * @returns Forge.UnsignedIntegerTimelineKey
         */
        keys(index: number, obj?: Forge.UnsignedIntegerTimelineKey): Forge.UnsignedIntegerTimelineKey | null;
        /**
         * @returns number
         */
        keysLength(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startUnsignedIntegerTimeline(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset keysOffset
         */
        static addKeys(builder: flatbuffers.Builder, keysOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number numElems
         */
        static startKeysVector(builder: flatbuffers.Builder, numElems: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endUnsignedIntegerTimeline(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class FloatTimeline {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns FloatTimeline
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): FloatTimeline;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param FloatTimeline= obj
         * @returns FloatTimeline
         */
        static getRootAsFloatTimeline(bb: flatbuffers.ByteBuffer, obj?: FloatTimeline): FloatTimeline;
        /**
         * @param number index
         * @param Forge.FloatTimelineKey= obj
         * @returns Forge.FloatTimelineKey
         */
        keys(index: number, obj?: Forge.FloatTimelineKey): Forge.FloatTimelineKey | null;
        /**
         * @returns number
         */
        keysLength(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startFloatTimeline(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset keysOffset
         */
        static addKeys(builder: flatbuffers.Builder, keysOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number numElems
         */
        static startKeysVector(builder: flatbuffers.Builder, numElems: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endFloatTimeline(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class Vector3Timeline {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns Vector3Timeline
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): Vector3Timeline;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param Vector3Timeline= obj
         * @returns Vector3Timeline
         */
        static getRootAsVector3Timeline(bb: flatbuffers.ByteBuffer, obj?: Vector3Timeline): Vector3Timeline;
        /**
         * @param number index
         * @param Forge.Vector3TimelineKey= obj
         * @returns Forge.Vector3TimelineKey
         */
        keys(index: number, obj?: Forge.Vector3TimelineKey): Forge.Vector3TimelineKey | null;
        /**
         * @returns number
         */
        keysLength(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startVector3Timeline(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset keysOffset
         */
        static addKeys(builder: flatbuffers.Builder, keysOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number numElems
         */
        static startKeysVector(builder: flatbuffers.Builder, numElems: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endVector3Timeline(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QuaternionTimeline {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QuaternionTimeline
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QuaternionTimeline;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QuaternionTimeline= obj
         * @returns QuaternionTimeline
         */
        static getRootAsQuaternionTimeline(bb: flatbuffers.ByteBuffer, obj?: QuaternionTimeline): QuaternionTimeline;
        /**
         * @param number index
         * @param Forge.QuaternionTimelineKey= obj
         * @returns Forge.QuaternionTimelineKey
         */
        keys(index: number, obj?: Forge.QuaternionTimelineKey): Forge.QuaternionTimelineKey | null;
        /**
         * @returns number
         */
        keysLength(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQuaternionTimeline(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset keysOffset
         */
        static addKeys(builder: flatbuffers.Builder, keysOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number numElems
         */
        static startKeysVector(builder: flatbuffers.Builder, numElems: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQuaternionTimeline(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class AnimatedPropertyDescription {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns AnimatedPropertyDescription
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): AnimatedPropertyDescription;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param AnimatedPropertyDescription= obj
         * @returns AnimatedPropertyDescription
         */
        static getRootAsAnimatedPropertyDescription(bb: flatbuffers.ByteBuffer, obj?: AnimatedPropertyDescription): AnimatedPropertyDescription;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        name(): string | null;
        name(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        description(): string | null;
        description(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @returns Forge.TimelineData
         */
        timelineType(): Forge.TimelineData;
        /**
         * @param flatbuffers.Table obj
         * @returns ?flatbuffers.Table
         */
        timeline<T extends flatbuffers.Table>(obj: T): T | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startAnimatedPropertyDescription(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset nameOffset
         */
        static addName(builder: flatbuffers.Builder, nameOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset descriptionOffset
         */
        static addDescription(builder: flatbuffers.Builder, descriptionOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.TimelineData timelineType
         */
        static addTimelineType(builder: flatbuffers.Builder, timelineType: Forge.TimelineData): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset timelineOffset
         */
        static addTimeline(builder: flatbuffers.Builder, timelineOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endAnimatedPropertyDescription(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QueryAnimatedPropertiesResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QueryAnimatedPropertiesResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QueryAnimatedPropertiesResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QueryAnimatedPropertiesResponse= obj
         * @returns QueryAnimatedPropertiesResponse
         */
        static getRootAsQueryAnimatedPropertiesResponse(bb: flatbuffers.ByteBuffer, obj?: QueryAnimatedPropertiesResponse): QueryAnimatedPropertiesResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param number index
         * @param Forge.AnimatedPropertyDescription= obj
         * @returns Forge.AnimatedPropertyDescription
         */
        properties(index: number, obj?: Forge.AnimatedPropertyDescription): Forge.AnimatedPropertyDescription | null;
        /**
         * @returns number
         */
        propertiesLength(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQueryAnimatedPropertiesResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset propertiesOffset
         */
        static addProperties(builder: flatbuffers.Builder, propertiesOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Array.<flatbuffers.Offset> data
         * @returns flatbuffers.Offset
         */
        static createPropertiesVector(builder: flatbuffers.Builder, data: flatbuffers.Offset[]): flatbuffers.Offset;
        /**
         * @param flatbuffers.Builder builder
         * @param number numElems
         */
        static startPropertiesVector(builder: flatbuffers.Builder, numElems: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQueryAnimatedPropertiesResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class TogglePlaybackNotification {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns TogglePlaybackNotification
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): TogglePlaybackNotification;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param TogglePlaybackNotification= obj
         * @returns TogglePlaybackNotification
         */
        static getRootAsTogglePlaybackNotification(bb: flatbuffers.ByteBuffer, obj?: TogglePlaybackNotification): TogglePlaybackNotification;
        /**
         * @param flatbuffers.Builder builder
         */
        static startTogglePlaybackNotification(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endTogglePlaybackNotification(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class RenderFrameRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns RenderFrameRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): RenderFrameRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param RenderFrameRequest= obj
         * @returns RenderFrameRequest
         */
        static getRootAsRenderFrameRequest(bb: flatbuffers.ByteBuffer, obj?: RenderFrameRequest): RenderFrameRequest;
        /**
         * @returns number
         */
        timeInS(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startRenderFrameRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number timeInS
         */
        static addTimeInS(builder: flatbuffers.Builder, timeInS: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endRenderFrameRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class RenderFrameResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns RenderFrameResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): RenderFrameResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param RenderFrameResponse= obj
         * @returns RenderFrameResponse
         */
        static getRootAsRenderFrameResponse(bb: flatbuffers.ByteBuffer, obj?: RenderFrameResponse): RenderFrameResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param number index
         * @returns number
         */
        pixelBytes(index: number): number | null;
        /**
         * @returns number
         */
        pixelBytesLength(): number;
        /**
         * @returns Uint8ClampedArray
         */
        pixelBytesArray(): Uint8ClampedArray | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startRenderFrameResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset pixelBytesOffset
         */
        static addPixelBytes(builder: flatbuffers.Builder, pixelBytesOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Array.<number> data
         * @returns flatbuffers.Offset
         */
        static createPixelBytesVector(builder: flatbuffers.Builder, data: number[] | Uint8Array): flatbuffers.Offset;
        /**
         * @param flatbuffers.Builder builder
         * @param number numElems
         */
        static startPixelBytesVector(builder: flatbuffers.Builder, numElems: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endRenderFrameResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class ModifyPropertyRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns ModifyPropertyRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): ModifyPropertyRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param ModifyPropertyRequest= obj
         * @returns ModifyPropertyRequest
         */
        static getRootAsModifyPropertyRequest(bb: flatbuffers.ByteBuffer, obj?: ModifyPropertyRequest): ModifyPropertyRequest;
        /**
         * @returns number
         */
        entityId(): number;
        /**
         * @returns number
         */
        propertyIndex(): number;
        /**
         * @returns Forge.PropertyData
         */
        valueType(): Forge.PropertyData;
        /**
         * @param flatbuffers.Table obj
         * @returns ?flatbuffers.Table
         */
        value<T extends flatbuffers.Table>(obj: T): T | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startModifyPropertyRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number entityId
         */
        static addEntityId(builder: flatbuffers.Builder, entityId: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number propertyIndex
         */
        static addPropertyIndex(builder: flatbuffers.Builder, propertyIndex: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.PropertyData valueType
         */
        static addValueType(builder: flatbuffers.Builder, valueType: Forge.PropertyData): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset valueOffset
         */
        static addValue(builder: flatbuffers.Builder, valueOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endModifyPropertyRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class ModifyPropertyResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns ModifyPropertyResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): ModifyPropertyResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param ModifyPropertyResponse= obj
         * @returns ModifyPropertyResponse
         */
        static getRootAsModifyPropertyResponse(bb: flatbuffers.ByteBuffer, obj?: ModifyPropertyResponse): ModifyPropertyResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param flatbuffers.Builder builder
         */
        static startModifyPropertyResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endModifyPropertyResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class ModifyAnimatedPropertyKeyRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns ModifyAnimatedPropertyKeyRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): ModifyAnimatedPropertyKeyRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param ModifyAnimatedPropertyKeyRequest= obj
         * @returns ModifyAnimatedPropertyKeyRequest
         */
        static getRootAsModifyAnimatedPropertyKeyRequest(bb: flatbuffers.ByteBuffer, obj?: ModifyAnimatedPropertyKeyRequest): ModifyAnimatedPropertyKeyRequest;
        /**
         * @returns number
         */
        entityId(): number;
        /**
         * @returns number
         */
        propertyIndex(): number;
        /**
         * @returns number
         */
        keyIndex(): number;
        /**
         * @returns number
         */
        time(): number;
        /**
         * @returns Forge.TimelineKeyData
         */
        valueType(): Forge.TimelineKeyData;
        /**
         * @param flatbuffers.Table obj
         * @returns ?flatbuffers.Table
         */
        value<T extends flatbuffers.Table>(obj: T): T | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startModifyAnimatedPropertyKeyRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number entityId
         */
        static addEntityId(builder: flatbuffers.Builder, entityId: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number propertyIndex
         */
        static addPropertyIndex(builder: flatbuffers.Builder, propertyIndex: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number keyIndex
         */
        static addKeyIndex(builder: flatbuffers.Builder, keyIndex: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number time
         */
        static addTime(builder: flatbuffers.Builder, time: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.TimelineKeyData valueType
         */
        static addValueType(builder: flatbuffers.Builder, valueType: Forge.TimelineKeyData): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset valueOffset
         */
        static addValue(builder: flatbuffers.Builder, valueOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endModifyAnimatedPropertyKeyRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class ModifyAnimatedPropertyKeyResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns ModifyAnimatedPropertyKeyResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): ModifyAnimatedPropertyKeyResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param ModifyAnimatedPropertyKeyResponse= obj
         * @returns ModifyAnimatedPropertyKeyResponse
         */
        static getRootAsModifyAnimatedPropertyKeyResponse(bb: flatbuffers.ByteBuffer, obj?: ModifyAnimatedPropertyKeyResponse): ModifyAnimatedPropertyKeyResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param flatbuffers.Builder builder
         */
        static startModifyAnimatedPropertyKeyResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endModifyAnimatedPropertyKeyResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class AddAnimatedPropertyKeyRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns AddAnimatedPropertyKeyRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): AddAnimatedPropertyKeyRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param AddAnimatedPropertyKeyRequest= obj
         * @returns AddAnimatedPropertyKeyRequest
         */
        static getRootAsAddAnimatedPropertyKeyRequest(bb: flatbuffers.ByteBuffer, obj?: AddAnimatedPropertyKeyRequest): AddAnimatedPropertyKeyRequest;
        /**
         * @returns number
         */
        entityId(): number;
        /**
         * @returns number
         */
        propertyIndex(): number;
        /**
         * @returns number
         */
        timeInS(): number;
        /**
         * @returns Forge.TimelineKeyData
         */
        valueType(): Forge.TimelineKeyData;
        /**
         * @param flatbuffers.Table obj
         * @returns ?flatbuffers.Table
         */
        value<T extends flatbuffers.Table>(obj: T): T | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startAddAnimatedPropertyKeyRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number entityId
         */
        static addEntityId(builder: flatbuffers.Builder, entityId: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number propertyIndex
         */
        static addPropertyIndex(builder: flatbuffers.Builder, propertyIndex: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number timeInS
         */
        static addTimeInS(builder: flatbuffers.Builder, timeInS: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.TimelineKeyData valueType
         */
        static addValueType(builder: flatbuffers.Builder, valueType: Forge.TimelineKeyData): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset valueOffset
         */
        static addValue(builder: flatbuffers.Builder, valueOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endAddAnimatedPropertyKeyRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class AddAnimatedPropertyKeyResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns AddAnimatedPropertyKeyResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): AddAnimatedPropertyKeyResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param AddAnimatedPropertyKeyResponse= obj
         * @returns AddAnimatedPropertyKeyResponse
         */
        static getRootAsAddAnimatedPropertyKeyResponse(bb: flatbuffers.ByteBuffer, obj?: AddAnimatedPropertyKeyResponse): AddAnimatedPropertyKeyResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param flatbuffers.Builder builder
         */
        static startAddAnimatedPropertyKeyResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endAddAnimatedPropertyKeyResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class RemoveAnimatedPropertyKeyRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns RemoveAnimatedPropertyKeyRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): RemoveAnimatedPropertyKeyRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param RemoveAnimatedPropertyKeyRequest= obj
         * @returns RemoveAnimatedPropertyKeyRequest
         */
        static getRootAsRemoveAnimatedPropertyKeyRequest(bb: flatbuffers.ByteBuffer, obj?: RemoveAnimatedPropertyKeyRequest): RemoveAnimatedPropertyKeyRequest;
        /**
         * @returns number
         */
        entityId(): number;
        /**
         * @returns number
         */
        propertyIndex(): number;
        /**
         * @returns number
         */
        keyIndex(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startRemoveAnimatedPropertyKeyRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number entityId
         */
        static addEntityId(builder: flatbuffers.Builder, entityId: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number propertyIndex
         */
        static addPropertyIndex(builder: flatbuffers.Builder, propertyIndex: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number keyIndex
         */
        static addKeyIndex(builder: flatbuffers.Builder, keyIndex: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endRemoveAnimatedPropertyKeyRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class RemoveAnimatedPropertyKeyResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns RemoveAnimatedPropertyKeyResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): RemoveAnimatedPropertyKeyResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param RemoveAnimatedPropertyKeyResponse= obj
         * @returns RemoveAnimatedPropertyKeyResponse
         */
        static getRootAsRemoveAnimatedPropertyKeyResponse(bb: flatbuffers.ByteBuffer, obj?: RemoveAnimatedPropertyKeyResponse): RemoveAnimatedPropertyKeyResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param flatbuffers.Builder builder
         */
        static startRemoveAnimatedPropertyKeyResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endRemoveAnimatedPropertyKeyResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class SaveDemoRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns SaveDemoRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): SaveDemoRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param SaveDemoRequest= obj
         * @returns SaveDemoRequest
         */
        static getRootAsSaveDemoRequest(bb: flatbuffers.ByteBuffer, obj?: SaveDemoRequest): SaveDemoRequest;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        path(): string | null;
        path(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startSaveDemoRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset pathOffset
         */
        static addPath(builder: flatbuffers.Builder, pathOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endSaveDemoRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class SaveDemoResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns SaveDemoResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): SaveDemoResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param SaveDemoResponse= obj
         * @returns SaveDemoResponse
         */
        static getRootAsSaveDemoResponse(bb: flatbuffers.ByteBuffer, obj?: SaveDemoResponse): SaveDemoResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param flatbuffers.Builder builder
         */
        static startSaveDemoResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endSaveDemoResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QueryDemoStateRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QueryDemoStateRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QueryDemoStateRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QueryDemoStateRequest= obj
         * @returns QueryDemoStateRequest
         */
        static getRootAsQueryDemoStateRequest(bb: flatbuffers.ByteBuffer, obj?: QueryDemoStateRequest): QueryDemoStateRequest;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQueryDemoStateRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQueryDemoStateRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QueryDemoStateResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QueryDemoStateResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QueryDemoStateResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QueryDemoStateResponse= obj
         * @returns QueryDemoStateResponse
         */
        static getRootAsQueryDemoStateResponse(bb: flatbuffers.ByteBuffer, obj?: QueryDemoStateResponse): QueryDemoStateResponse;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        project(): string | null;
        project(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        group(): string | null;
        group(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        name(): string | null;
        name(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQueryDemoStateResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset projectOffset
         */
        static addProject(builder: flatbuffers.Builder, projectOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset groupOffset
         */
        static addGroup(builder: flatbuffers.Builder, groupOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset nameOffset
         */
        static addName(builder: flatbuffers.Builder, nameOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQueryDemoStateResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class UpdateFreeLookCameraNotification {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns UpdateFreeLookCameraNotification
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): UpdateFreeLookCameraNotification;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param UpdateFreeLookCameraNotification= obj
         * @returns UpdateFreeLookCameraNotification
         */
        static getRootAsUpdateFreeLookCameraNotification(bb: flatbuffers.ByteBuffer, obj?: UpdateFreeLookCameraNotification): UpdateFreeLookCameraNotification;
        /**
         * @returns number
         */
        deltaPitch(): number;
        /**
         * @returns number
         */
        deltaYaw(): number;
        /**
         * @returns number
         */
        deltaX(): number;
        /**
         * @returns number
         */
        deltaY(): number;
        /**
         * @returns number
         */
        deltaZ(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startUpdateFreeLookCameraNotification(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number deltaPitch
         */
        static addDeltaPitch(builder: flatbuffers.Builder, deltaPitch: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number deltaYaw
         */
        static addDeltaYaw(builder: flatbuffers.Builder, deltaYaw: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number deltaX
         */
        static addDeltaX(builder: flatbuffers.Builder, deltaX: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number deltaY
         */
        static addDeltaY(builder: flatbuffers.Builder, deltaY: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number deltaZ
         */
        static addDeltaZ(builder: flatbuffers.Builder, deltaZ: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endUpdateFreeLookCameraNotification(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class ConvertMeshRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns ConvertMeshRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): ConvertMeshRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param ConvertMeshRequest= obj
         * @returns ConvertMeshRequest
         */
        static getRootAsConvertMeshRequest(bb: flatbuffers.ByteBuffer, obj?: ConvertMeshRequest): ConvertMeshRequest;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        filename(): string | null;
        filename(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        outputPath(): string | null;
        outputPath(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @returns number
         */
        attributeMask(): number;
        /**
         * @param number index
         * @returns number
         */
        fileData(index: number): number | null;
        /**
         * @returns number
         */
        fileDataLength(): number;
        /**
         * @returns Uint8Array
         */
        fileDataArray(): Uint8Array | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startConvertMeshRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset filenameOffset
         */
        static addFilename(builder: flatbuffers.Builder, filenameOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset outputPathOffset
         */
        static addOutputPath(builder: flatbuffers.Builder, outputPathOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number attributeMask
         */
        static addAttributeMask(builder: flatbuffers.Builder, attributeMask: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset fileDataOffset
         */
        static addFileData(builder: flatbuffers.Builder, fileDataOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Array.<number> data
         * @returns flatbuffers.Offset
         */
        static createFileDataVector(builder: flatbuffers.Builder, data: number[] | Uint8Array): flatbuffers.Offset;
        /**
         * @param flatbuffers.Builder builder
         * @param number numElems
         */
        static startFileDataVector(builder: flatbuffers.Builder, numElems: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endConvertMeshRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class ConvertMeshResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns ConvertMeshResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): ConvertMeshResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param ConvertMeshResponse= obj
         * @returns ConvertMeshResponse
         */
        static getRootAsConvertMeshResponse(bb: flatbuffers.ByteBuffer, obj?: ConvertMeshResponse): ConvertMeshResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param flatbuffers.Builder builder
         */
        static startConvertMeshResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endConvertMeshResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class ConvertTextureRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns ConvertTextureRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): ConvertTextureRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param ConvertTextureRequest= obj
         * @returns ConvertTextureRequest
         */
        static getRootAsConvertTextureRequest(bb: flatbuffers.ByteBuffer, obj?: ConvertTextureRequest): ConvertTextureRequest;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        filename(): string | null;
        filename(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        outputPath(): string | null;
        outputPath(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @returns Forge.TextureFormat
         */
        outputFormat(): Forge.TextureFormat;
        /**
         * @returns Forge.TextureConversionQuality
         */
        quality(): Forge.TextureConversionQuality;
        /**
         * @param number index
         * @returns number
         */
        fileData(index: number): number | null;
        /**
         * @returns number
         */
        fileDataLength(): number;
        /**
         * @returns Uint8Array
         */
        fileDataArray(): Uint8Array | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startConvertTextureRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset filenameOffset
         */
        static addFilename(builder: flatbuffers.Builder, filenameOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset outputPathOffset
         */
        static addOutputPath(builder: flatbuffers.Builder, outputPathOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.TextureFormat outputFormat
         */
        static addOutputFormat(builder: flatbuffers.Builder, outputFormat: Forge.TextureFormat): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.TextureConversionQuality quality
         */
        static addQuality(builder: flatbuffers.Builder, quality: Forge.TextureConversionQuality): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset fileDataOffset
         */
        static addFileData(builder: flatbuffers.Builder, fileDataOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Array.<number> data
         * @returns flatbuffers.Offset
         */
        static createFileDataVector(builder: flatbuffers.Builder, data: number[] | Uint8Array): flatbuffers.Offset;
        /**
         * @param flatbuffers.Builder builder
         * @param number numElems
         */
        static startFileDataVector(builder: flatbuffers.Builder, numElems: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endConvertTextureRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class ConvertTextureResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns ConvertTextureResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): ConvertTextureResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param ConvertTextureResponse= obj
         * @returns ConvertTextureResponse
         */
        static getRootAsConvertTextureResponse(bb: flatbuffers.ByteBuffer, obj?: ConvertTextureResponse): ConvertTextureResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param flatbuffers.Builder builder
         */
        static startConvertTextureResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endConvertTextureResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QueryAudioTrackRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QueryAudioTrackRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QueryAudioTrackRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QueryAudioTrackRequest= obj
         * @returns QueryAudioTrackRequest
         */
        static getRootAsQueryAudioTrackRequest(bb: flatbuffers.ByteBuffer, obj?: QueryAudioTrackRequest): QueryAudioTrackRequest;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQueryAudioTrackRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQueryAudioTrackRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QueryAudioTrackResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QueryAudioTrackResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QueryAudioTrackResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QueryAudioTrackResponse= obj
         * @returns QueryAudioTrackResponse
         */
        static getRootAsQueryAudioTrackResponse(bb: flatbuffers.ByteBuffer, obj?: QueryAudioTrackResponse): QueryAudioTrackResponse;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        path(): string | null;
        path(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @returns number
         */
        lengthInS(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQueryAudioTrackResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset pathOffset
         */
        static addPath(builder: flatbuffers.Builder, pathOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number lengthInS
         */
        static addLengthInS(builder: flatbuffers.Builder, lengthInS: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQueryAudioTrackResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class LoadAudioTrackRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns LoadAudioTrackRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): LoadAudioTrackRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param LoadAudioTrackRequest= obj
         * @returns LoadAudioTrackRequest
         */
        static getRootAsLoadAudioTrackRequest(bb: flatbuffers.ByteBuffer, obj?: LoadAudioTrackRequest): LoadAudioTrackRequest;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        path(): string | null;
        path(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startLoadAudioTrackRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset pathOffset
         */
        static addPath(builder: flatbuffers.Builder, pathOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endLoadAudioTrackRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class LoadAudioTrackResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns LoadAudioTrackResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): LoadAudioTrackResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param LoadAudioTrackResponse= obj
         * @returns LoadAudioTrackResponse
         */
        static getRootAsLoadAudioTrackResponse(bb: flatbuffers.ByteBuffer, obj?: LoadAudioTrackResponse): LoadAudioTrackResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @returns number
         */
        lengthInS(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startLoadAudioTrackResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number lengthInS
         */
        static addLengthInS(builder: flatbuffers.Builder, lengthInS: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endLoadAudioTrackResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class ModifyDemoPropertiesNotification {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns ModifyDemoPropertiesNotification
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): ModifyDemoPropertiesNotification;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param ModifyDemoPropertiesNotification= obj
         * @returns ModifyDemoPropertiesNotification
         */
        static getRootAsModifyDemoPropertiesNotification(bb: flatbuffers.ByteBuffer, obj?: ModifyDemoPropertiesNotification): ModifyDemoPropertiesNotification;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        group(): string | null;
        group(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        name(): string | null;
        name(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startModifyDemoPropertiesNotification(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset groupOffset
         */
        static addGroup(builder: flatbuffers.Builder, groupOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset nameOffset
         */
        static addName(builder: flatbuffers.Builder, nameOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endModifyDemoPropertiesNotification(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class RenameEntityRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns RenameEntityRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): RenameEntityRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param RenameEntityRequest= obj
         * @returns RenameEntityRequest
         */
        static getRootAsRenameEntityRequest(bb: flatbuffers.ByteBuffer, obj?: RenameEntityRequest): RenameEntityRequest;
        /**
         * @returns number
         */
        entityId(): number;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        name(): string | null;
        name(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startRenameEntityRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number entityId
         */
        static addEntityId(builder: flatbuffers.Builder, entityId: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset nameOffset
         */
        static addName(builder: flatbuffers.Builder, nameOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endRenameEntityRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class RenameEntityResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns RenameEntityResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): RenameEntityResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param RenameEntityResponse= obj
         * @returns RenameEntityResponse
         */
        static getRootAsRenameEntityResponse(bb: flatbuffers.ByteBuffer, obj?: RenameEntityResponse): RenameEntityResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param flatbuffers.Builder builder
         */
        static startRenameEntityResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endRenameEntityResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class ToggleFreeLookNotification {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns ToggleFreeLookNotification
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): ToggleFreeLookNotification;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param ToggleFreeLookNotification= obj
         * @returns ToggleFreeLookNotification
         */
        static getRootAsToggleFreeLookNotification(bb: flatbuffers.ByteBuffer, obj?: ToggleFreeLookNotification): ToggleFreeLookNotification;
        /**
         * @param flatbuffers.Builder builder
         */
        static startToggleFreeLookNotification(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endToggleFreeLookNotification(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QuerySelectedEntityRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QuerySelectedEntityRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QuerySelectedEntityRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QuerySelectedEntityRequest= obj
         * @returns QuerySelectedEntityRequest
         */
        static getRootAsQuerySelectedEntityRequest(bb: flatbuffers.ByteBuffer, obj?: QuerySelectedEntityRequest): QuerySelectedEntityRequest;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQuerySelectedEntityRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQuerySelectedEntityRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QuerySelectedEntityResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QuerySelectedEntityResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QuerySelectedEntityResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QuerySelectedEntityResponse= obj
         * @returns QuerySelectedEntityResponse
         */
        static getRootAsQuerySelectedEntityResponse(bb: flatbuffers.ByteBuffer, obj?: QuerySelectedEntityResponse): QuerySelectedEntityResponse;
        /**
         * @returns number
         */
        entityId(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQuerySelectedEntityResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number entityId
         */
        static addEntityId(builder: flatbuffers.Builder, entityId: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQuerySelectedEntityResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class SelectEntityRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns SelectEntityRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): SelectEntityRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param SelectEntityRequest= obj
         * @returns SelectEntityRequest
         */
        static getRootAsSelectEntityRequest(bb: flatbuffers.ByteBuffer, obj?: SelectEntityRequest): SelectEntityRequest;
        /**
         * @returns number
         */
        entityId(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startSelectEntityRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number entityId
         */
        static addEntityId(builder: flatbuffers.Builder, entityId: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endSelectEntityRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class SelectEntityResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns SelectEntityResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): SelectEntityResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param SelectEntityResponse= obj
         * @returns SelectEntityResponse
         */
        static getRootAsSelectEntityResponse(bb: flatbuffers.ByteBuffer, obj?: SelectEntityResponse): SelectEntityResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param flatbuffers.Builder builder
         */
        static startSelectEntityResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endSelectEntityResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QuerySelectionRayRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QuerySelectionRayRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QuerySelectionRayRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QuerySelectionRayRequest= obj
         * @returns QuerySelectionRayRequest
         */
        static getRootAsQuerySelectionRayRequest(bb: flatbuffers.ByteBuffer, obj?: QuerySelectionRayRequest): QuerySelectionRayRequest;
        /**
         * @param Forge.Vector2= obj
         * @returns Forge.Vector2|null
         */
        normalizedRayOrigin(obj?: Forge.Vector2): Forge.Vector2 | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQuerySelectionRayRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset normalizedRayOriginOffset
         */
        static addNormalizedRayOrigin(builder: flatbuffers.Builder, normalizedRayOriginOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQuerySelectionRayRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QuerySelectionRayResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QuerySelectionRayResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QuerySelectionRayResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QuerySelectionRayResponse= obj
         * @returns QuerySelectionRayResponse
         */
        static getRootAsQuerySelectionRayResponse(bb: flatbuffers.ByteBuffer, obj?: QuerySelectionRayResponse): QuerySelectionRayResponse;
        /**
         * @param number index
         * @returns number
         */
        entityIds(index: number): number | null;
        /**
         * @returns number
         */
        entityIdsLength(): number;
        /**
         * @returns Uint32Array
         */
        entityIdsArray(): Uint32Array | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQuerySelectionRayResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset entityIdsOffset
         */
        static addEntityIds(builder: flatbuffers.Builder, entityIdsOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Array.<number> data
         * @returns flatbuffers.Offset
         */
        static createEntityIdsVector(builder: flatbuffers.Builder, data: number[] | Uint8Array): flatbuffers.Offset;
        /**
         * @param flatbuffers.Builder builder
         * @param number numElems
         */
        static startEntityIdsVector(builder: flatbuffers.Builder, numElems: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQuerySelectionRayResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class RenderJpegFrameRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns RenderJpegFrameRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): RenderJpegFrameRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param RenderJpegFrameRequest= obj
         * @returns RenderJpegFrameRequest
         */
        static getRootAsRenderJpegFrameRequest(bb: flatbuffers.ByteBuffer, obj?: RenderJpegFrameRequest): RenderJpegFrameRequest;
        /**
         * @returns number
         */
        timeInS(): number;
        /**
         * @returns number
         */
        quality(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startRenderJpegFrameRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number timeInS
         */
        static addTimeInS(builder: flatbuffers.Builder, timeInS: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number quality
         */
        static addQuality(builder: flatbuffers.Builder, quality: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endRenderJpegFrameRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class RenderJpegFrameResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns RenderJpegFrameResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): RenderJpegFrameResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param RenderJpegFrameResponse= obj
         * @returns RenderJpegFrameResponse
         */
        static getRootAsRenderJpegFrameResponse(bb: flatbuffers.ByteBuffer, obj?: RenderJpegFrameResponse): RenderJpegFrameResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param number index
         * @returns number
         */
        jpegBytes(index: number): number | null;
        /**
         * @returns number
         */
        jpegBytesLength(): number;
        /**
         * @returns Uint8Array
         */
        jpegBytesArray(): Uint8Array | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startRenderJpegFrameResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset jpegBytesOffset
         */
        static addJpegBytes(builder: flatbuffers.Builder, jpegBytesOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Array.<number> data
         * @returns flatbuffers.Offset
         */
        static createJpegBytesVector(builder: flatbuffers.Builder, data: number[] | Uint8Array): flatbuffers.Offset;
        /**
         * @param flatbuffers.Builder builder
         * @param number numElems
         */
        static startJpegBytesVector(builder: flatbuffers.Builder, numElems: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endRenderJpegFrameResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class ReloadResourcesRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns ReloadResourcesRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): ReloadResourcesRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param ReloadResourcesRequest= obj
         * @returns ReloadResourcesRequest
         */
        static getRootAsReloadResourcesRequest(bb: flatbuffers.ByteBuffer, obj?: ReloadResourcesRequest): ReloadResourcesRequest;
        /**
         * @param flatbuffers.Builder builder
         */
        static startReloadResourcesRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endReloadResourcesRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class ReloadResourcesResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns ReloadResourcesResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): ReloadResourcesResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param ReloadResourcesResponse= obj
         * @returns ReloadResourcesResponse
         */
        static getRootAsReloadResourcesResponse(bb: flatbuffers.ByteBuffer, obj?: ReloadResourcesResponse): ReloadResourcesResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param flatbuffers.Builder builder
         */
        static startReloadResourcesResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endReloadResourcesResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QueryEntityTypesRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QueryEntityTypesRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QueryEntityTypesRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QueryEntityTypesRequest= obj
         * @returns QueryEntityTypesRequest
         */
        static getRootAsQueryEntityTypesRequest(bb: flatbuffers.ByteBuffer, obj?: QueryEntityTypesRequest): QueryEntityTypesRequest;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQueryEntityTypesRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQueryEntityTypesRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class EntityTypeDescription {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns EntityTypeDescription
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): EntityTypeDescription;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param EntityTypeDescription= obj
         * @returns EntityTypeDescription
         */
        static getRootAsEntityTypeDescription(bb: flatbuffers.ByteBuffer, obj?: EntityTypeDescription): EntityTypeDescription;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        name(): string | null;
        name(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        description(): string | null;
        description(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startEntityTypeDescription(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset nameOffset
         */
        static addName(builder: flatbuffers.Builder, nameOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset descriptionOffset
         */
        static addDescription(builder: flatbuffers.Builder, descriptionOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endEntityTypeDescription(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class QueryEntityTypesResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns QueryEntityTypesResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): QueryEntityTypesResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param QueryEntityTypesResponse= obj
         * @returns QueryEntityTypesResponse
         */
        static getRootAsQueryEntityTypesResponse(bb: flatbuffers.ByteBuffer, obj?: QueryEntityTypesResponse): QueryEntityTypesResponse;
        /**
         * @param number index
         * @param Forge.EntityTypeDescription= obj
         * @returns Forge.EntityTypeDescription
         */
        entityTypes(index: number, obj?: Forge.EntityTypeDescription): Forge.EntityTypeDescription | null;
        /**
         * @returns number
         */
        entityTypesLength(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startQueryEntityTypesResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset entityTypesOffset
         */
        static addEntityTypes(builder: flatbuffers.Builder, entityTypesOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Array.<flatbuffers.Offset> data
         * @returns flatbuffers.Offset
         */
        static createEntityTypesVector(builder: flatbuffers.Builder, data: flatbuffers.Offset[]): flatbuffers.Offset;
        /**
         * @param flatbuffers.Builder builder
         * @param number numElems
         */
        static startEntityTypesVector(builder: flatbuffers.Builder, numElems: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endQueryEntityTypesResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class RenderJpegBase64FrameRequest {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns RenderJpegBase64FrameRequest
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): RenderJpegBase64FrameRequest;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param RenderJpegBase64FrameRequest= obj
         * @returns RenderJpegBase64FrameRequest
         */
        static getRootAsRenderJpegBase64FrameRequest(bb: flatbuffers.ByteBuffer, obj?: RenderJpegBase64FrameRequest): RenderJpegBase64FrameRequest;
        /**
         * @returns number
         */
        timeInS(): number;
        /**
         * @returns number
         */
        quality(): number;
        /**
         * @param flatbuffers.Builder builder
         */
        static startRenderJpegBase64FrameRequest(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number timeInS
         */
        static addTimeInS(builder: flatbuffers.Builder, timeInS: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @param number quality
         */
        static addQuality(builder: flatbuffers.Builder, quality: number): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endRenderJpegBase64FrameRequest(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class RenderJpegBase64FrameResponse {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns RenderJpegBase64FrameResponse
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): RenderJpegBase64FrameResponse;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param RenderJpegBase64FrameResponse= obj
         * @returns RenderJpegBase64FrameResponse
         */
        static getRootAsRenderJpegBase64FrameResponse(bb: flatbuffers.ByteBuffer, obj?: RenderJpegBase64FrameResponse): RenderJpegBase64FrameResponse;
        /**
         * @returns Forge.Result
         */
        result(): Forge.Result;
        /**
         * @param flatbuffers.Encoding= optionalEncoding
         * @returns string|Uint8Array|null
         */
        jpegData(): string | null;
        jpegData(optionalEncoding: flatbuffers.Encoding): string | Uint8Array | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startRenderJpegBase64FrameResponse(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.Result result
         */
        static addResult(builder: flatbuffers.Builder, result: Forge.Result): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset jpegDataOffset
         */
        static addJpegData(builder: flatbuffers.Builder, jpegDataOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endRenderJpegBase64FrameResponse(builder: flatbuffers.Builder): flatbuffers.Offset;
    }
}
/**
 * @constructor
 */
export declare namespace Forge {
    class Packet {
        bb: flatbuffers.ByteBuffer | null;
        bb_pos: number;
        /**
         * @param number i
         * @param flatbuffers.ByteBuffer bb
         * @returns Packet
         */
        __init(i: number, bb: flatbuffers.ByteBuffer): Packet;
        /**
         * @param flatbuffers.ByteBuffer bb
         * @param Packet= obj
         * @returns Packet
         */
        static getRootAsPacket(bb: flatbuffers.ByteBuffer, obj?: Packet): Packet;
        /**
         * @returns Forge.PacketPayload
         */
        payloadType(): Forge.PacketPayload;
        /**
         * @param flatbuffers.Table obj
         * @returns ?flatbuffers.Table
         */
        payload<T extends flatbuffers.Table>(obj: T): T | null;
        /**
         * @param flatbuffers.Builder builder
         */
        static startPacket(builder: flatbuffers.Builder): void;
        /**
         * @param flatbuffers.Builder builder
         * @param Forge.PacketPayload payloadType
         */
        static addPayloadType(builder: flatbuffers.Builder, payloadType: Forge.PacketPayload): void;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset payloadOffset
         */
        static addPayload(builder: flatbuffers.Builder, payloadOffset: flatbuffers.Offset): void;
        /**
         * @param flatbuffers.Builder builder
         * @returns flatbuffers.Offset
         */
        static endPacket(builder: flatbuffers.Builder): flatbuffers.Offset;
        /**
         * @param flatbuffers.Builder builder
         * @param flatbuffers.Offset offset
         */
        static finishPacketBuffer(builder: flatbuffers.Builder, offset: flatbuffers.Offset): void;
    }
}
