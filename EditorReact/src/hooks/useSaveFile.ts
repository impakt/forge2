import useReduxState from './useReduxState';
import { getLoadedDemoFileName } from '../store/demoFile/selectors';
import useReduxAction from './useReduxAction';
import { saveDemoRequest } from '../store/engine/actions';

export default function useSaveFile(): null | ((...args: any[]) => any) {
  const demoFile = useReduxState(getLoadedDemoFileName);
  const saveDemo = useReduxAction(saveDemoRequest);

  if (!demoFile) {
    return null;
  }

  return () => saveDemo(demoFile);
}
