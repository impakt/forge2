import React, { useEffect, useRef } from 'react';
import {
  renderBase64JpegFrameRequest,
  toggleFreelookRequest,
  updateCameraLocation,
} from '../store/engine/actions';
import { getCurrentTime } from '../store/engine/selectors';
import { useDispatch, useSelector } from 'react-redux';

type Deltas = {
  mouseX: number;
  mouseY: number;
  keyW: boolean;
  keyA: boolean;
  keyS: boolean;
  keyD: boolean;
  accelerate: boolean;
};

const initialDeltas: Deltas = {
  mouseX: 0,
  mouseY: 0,
  keyW: false,
  keyA: false,
  keyS: false,
  keyD: false,
  accelerate: false,
};

export default function useCameraControls(
  pointerLocked: boolean,
  canvasRef: React.MutableRefObject<HTMLCanvasElement | null>,
) {
  const dispatch = useDispatch();
  const currentTime = useSelector(getCurrentTime);

  const movementDeltas = useRef(initialDeltas);

  useEffect(() => {
    if (pointerLocked) {
      function handleKeyPress(e: KeyboardEvent) {
        if (e.key === 'X') {
          e.preventDefault();
          e.stopPropagation();

          dispatch(toggleFreelookRequest());
        }
      }

      function handleMouseMove(e: MouseEvent) {
        movementDeltas.current.mouseX =
          movementDeltas.current.mouseX + e.movementX;
        movementDeltas.current.mouseY =
          movementDeltas.current.mouseY + e.movementY;
      }

      function handleKeyDown(e: KeyboardEvent) {
        movementDeltas.current.accelerate = e.shiftKey;

        if (e.code === 'KeyW') {
          movementDeltas.current.keyW = true;
        } else if (e.code === 'KeyA') {
          movementDeltas.current.keyA = true;
        } else if (e.code === 'KeyS') {
          movementDeltas.current.keyS = true;
        } else if (e.code === 'KeyD') {
          movementDeltas.current.keyD = true;
        } else if (e.code === 'Shift') {
          movementDeltas.current.accelerate = true;
        }
      }

      function handleKeyUp(e: KeyboardEvent) {
        movementDeltas.current.accelerate = e.shiftKey;

        if (e.code === 'KeyW') {
          movementDeltas.current.keyW = false;
        } else if (e.code === 'KeyA') {
          movementDeltas.current.keyA = false;
        } else if (e.code === 'KeyS') {
          movementDeltas.current.keyS = false;
        } else if (e.code === 'KeyD') {
          movementDeltas.current.keyD = false;
        }
      }

      let frame: number;

      function animate() {
        frame = requestAnimationFrame(animate);

        let X = 0;
        let Y = 0;
        let Z = 0;

        const accelerate = movementDeltas.current.accelerate ? 3 : 1.5;

        if (movementDeltas.current.keyA) {
          X = -accelerate;
        } else if (movementDeltas.current.keyD) {
          X = accelerate;
        }

        if (movementDeltas.current.keyW) {
          Z = -accelerate;
        } else if (movementDeltas.current.keyS) {
          Z = accelerate;
        }

        if (
          movementDeltas.current.mouseX !== 0 ||
          movementDeltas.current.mouseY !== 0 ||
          X !== 0 ||
          Z !== 0
        ) {
          dispatch(
            updateCameraLocation({
              deltaX: X,
              deltaY: 0,
              deltaZ: Z,
              deltaPitch: -movementDeltas.current.mouseY,
              deltaYaw: -movementDeltas.current.mouseX,
            }),
          );

          dispatch(renderBase64JpegFrameRequest(currentTime));
        }

        movementDeltas.current.mouseX = 0;
        movementDeltas.current.mouseY = 0;
      }

      frame = requestAnimationFrame(animate);

      canvasRef.current!.addEventListener('mousemove', handleMouseMove);
      window.addEventListener('keypress', handleKeyPress);
      window.addEventListener('keydown', handleKeyDown);
      window.addEventListener('keyup', handleKeyUp);

      return () => {
        cancelAnimationFrame(frame);
        canvasRef.current!.removeEventListener('mousemove', handleMouseMove);
        window.removeEventListener('keypress', handleKeyPress);
        window.removeEventListener('keydown', handleKeyDown);
        window.removeEventListener('keyup', handleKeyUp);

        movementDeltas.current.mouseX = 0;
        movementDeltas.current.mouseY = 0;
        movementDeltas.current.keyW = false;
        movementDeltas.current.keyA = false;
        movementDeltas.current.keyS = false;
        movementDeltas.current.keyD = false;
        movementDeltas.current.accelerate = false;
      };
    }
  }, [pointerLocked]);
}
