import { useContext, useMemo } from 'react';
import { ReactReduxContext } from 'react-redux';
import { bindActionCreator } from '../util/bindActionCreator';
import { ActionCreator } from 'typescript-fsa';

export default function useReduxActions(
  actions: ActionCreator<any>[],
): ((...args: any[]) => any)[] {
  const reduxContext = useContext(ReactReduxContext);
  if (!reduxContext) {
    throw new Error('[useReduxActions]: Must be in a Redux provider');
  }

  const dispatch = reduxContext.store.dispatch;

  return useMemo(() => {
    const creators = [];
    for (let i = 0; i < actions.length; i++) {
      const actionCreator = actions[i];
      if (typeof actionCreator === 'function') {
        creators.push(bindActionCreator(actionCreator, dispatch));
      }
    }
    return creators;
  }, [actions, dispatch]);
}
