import { useContext, useMemo } from 'react';
import { ReactReduxContext } from 'react-redux';
import { bindActionCreator } from '../util/bindActionCreator';
import { ActionCreator } from 'typescript-fsa';

export default function useReduxAction(
  action: (...args: any[]) => any,
): (...args: any[]) => any {
  const reduxContext = useContext(ReactReduxContext);
  if (!reduxContext) {
    throw new Error('[useReduxAction]: Must be in a Redux provider');
  }

  const dispatch = reduxContext.store.dispatch;

  return useMemo(() => {
    return bindActionCreator(action, dispatch);
  }, [action, dispatch]);
}
