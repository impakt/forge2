import { useEffect, useState } from 'react';
import { RootState } from '../types/store';
import useReduxContext from './useReduxContext';

export default function useReduxState<T>(
  selector: (s: RootState, ...args: any[]) => T,
  ...args: any[]
): T {
  const reduxContext = useReduxContext();
  if (!reduxContext) {
    throw new Error('[useReduxState]: Must be in a Redux provider');
  }

  const [value, setValue] = useState(
    selector(reduxContext.store.getState(), ...args),
  );

  useEffect(() => {
    // TODO: make sure to not call setValue when unmounting
    return reduxContext.store.subscribe(() =>
      setValue(selector(reduxContext.store.getState(), ...args)),
    );
  }, []);

  return value;
}
