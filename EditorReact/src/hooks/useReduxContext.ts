import { useContext } from 'react';
import { ReactReduxContext, ReactReduxContextValue } from 'react-redux';
import { RootState } from '../types/store';

export default function useReduxContext(): ReactReduxContextValue<RootState> {
  return useContext(ReactReduxContext);
}
