import useReduxContext from './useReduxContext';

export default function useReduxDispatch() {
  const context = useReduxContext();
  if (!context) {
    throw new Error('Must be in a Redux context');
  }

  return context.store.dispatch;
}
