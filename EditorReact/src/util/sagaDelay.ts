import { delay as reduxSagaDelay, SagaIterator } from 'redux-saga';
import { call } from 'redux-saga/effects';

export default function* delay(ms: number): SagaIterator {
  yield call(reduxSagaDelay, ms);
}
