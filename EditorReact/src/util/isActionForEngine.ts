import { Action } from 'typescript-fsa';

export default function isActionForEngine(action: Action<any> | null) {
  return action && action.meta && action.meta.engineAction;
}
