import { Dispatch } from 'redux';

export function bindActionCreator<T extends Function>(
  actionCreator: T,
  dispatch: Dispatch,
) {
  return function(...args: any[]) {
    return dispatch(actionCreator(...Array.from(arguments)));
  };
}
