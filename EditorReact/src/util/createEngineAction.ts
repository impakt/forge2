import actionCreatorFactory from 'typescript-fsa';

const baseMeta = {
  engineAction: true,
};

function identity<T>(arg: T): T {
  return arg;
}

const actionCreator = actionCreatorFactory('engine');

export default function createEngineAction<Payload = undefined>(type: string) {
  return actionCreator<Payload>(type, baseMeta);
}
