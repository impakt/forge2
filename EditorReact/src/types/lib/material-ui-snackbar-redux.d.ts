declare module 'material-ui-snackbar-redux' {
  import { SnackbarProps } from '@material-ui/core/Snackbar';

  export function snackbarReducer(): any;

  interface SnackbarProviderProps {
    children: React.ReactNode;
    SnackbarProps: Partial<Omit<SnackbarProps, 'open'>>;
  }

  export const SnackbarProvider: React.ComponentType<SnackbarProviderProps>;

  interface SnackbarActions {
    show: (options: {
      message: string;
      action?: string;
      handler?: React.MouseEventHandler<HTMLButtonElement>;
    }) => any;
  }

  export const snackbarActions: SnackbarActions;
}
