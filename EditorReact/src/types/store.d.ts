import { ActionType } from 'typesafe-actions';
import rootReducer from '../store/rootReducer';

export type RootAction = ActionType<
  typeof import('../store/demoFile/actions.js').default
>;

declare module 'typesafe-actions' {
  interface Types {
    RootAction: RootAction;
  }
}

export type RootState = ReturnType<typeof rootReducer>;
