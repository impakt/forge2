import { Forge } from '../../engine/Packet_generated';
import {
  PropertyDataType,
  Vector3,
  Vector4,
} from '../../store/engineMiddleware/propertyDataToValue';

type EntityType = number;

type Statuses = 'ALIVE' | 'CONNECTING' | 'DEAD';

// export type EntityPropertyType<T> = T extends AnimatedEntityProperty
//   ? AnimatedEntityProperty
//   : T extends StaticEntityProperty
//   ? StaticEntityProperty
//   : BaseEntityProperty;

// prettier-ignore
export type PropertyDataToValue<T extends Forge.PropertyData>
  = T extends Forge.PropertyData.NONE ? null | undefined
  : T extends Forge.PropertyData.BoolProperty ? boolean
  : T extends Forge.PropertyData.FloatProperty ? number
  : T extends Forge.PropertyData.IntegerProperty ? number
  : T extends Forge.PropertyData.UnsignedIntegerProperty ? number
  : T extends Forge.PropertyData.StringProperty ? string
  : T extends Forge.PropertyData.Vector3Property ? Vector3
  : T extends Forge.PropertyData.Vector4Property ? Vector4
    : never;

// prettier-ignore
export type TimelineDataToValue<T>
  = T extends Forge.TimelineData.NONE ? null | undefined
  : T extends Forge.TimelineData.BooleanTimeline ? boolean
  : T extends Forge.TimelineData.FloatTimeline ? number
  : T extends Forge.TimelineData.IntegerTimeline ? number
  : T extends Forge.TimelineData.UnsignedIntegerTimeline ? number
  : T extends Forge.TimelineData.Vector3Timeline ? Vector3
  : T extends Forge.TimelineData.QuaternionTimeline ? Vector4
    : never;

// prettier-ignore
export type TimelineKeyDataToValue<T>
  = T extends Forge.TimelineKeyData.NONE ? null | undefined
  : T extends Forge.TimelineKeyData.BooleanTimelineKey ? boolean
  : T extends Forge.TimelineKeyData.FloatTimelineKey ? number
  : T extends Forge.TimelineKeyData.IntegerTimelineKey ? number
  : T extends Forge.TimelineKeyData.UnsignedIntegerTimelineKey ? number
  : T extends Forge.TimelineKeyData.Vector3TimelineKey ? Vector3
  : T extends Forge.TimelineKeyData.QuaternionTimelineKey ? Vector4
    : never;

export type AnimatedTimelinePropertyValue =
  | boolean
  | number
  | string
  | Vector3
  | Vector4;

export interface KeyframeType {
  time: number;
  value: AnimatedTimelinePropertyValue;
}

export interface StaticEntityProperty<T extends Forge.PropertyData = any> {
  description: string | null;
  name: string;
  type: Forge.PropertyData;
  value: T extends any ? PropertyDataType : PropertyDataToValue<T>;
}

export interface AnimatedEntityProperty {
  description: string | null;
  name: string;
  type: Forge.TimelineKeyData | Forge.TimelineData;

  // Added in reducer:
  animated?: boolean;
  timeline: KeyframeType[];
}

export interface Entity {
  id: number;
  name: string;
  typeIndex: EntityType;
  staticProperties: StaticEntityProperty[];
  animatedProperties: AnimatedEntityProperty[];
}

type EntitiesState = {
  byId: any[];
  normalized: {
    [key: string]: Entity;
  };
};

export type State = {
  group: string | null;
  name: string | null;
  project: string | null;
  entities: EntitiesState;
  reason: string | null;
  status: Statuses;
  time: number;
};
