interface Window {
  requestIdleCallback: (cb: () => any) => any;
}

declare var requestIdleCallback: (cb: () => any) => any;

declare global {
  export interface NodeModule {
    hot?: {
      accept: (path: string, cb: () => any) => void;
    };
  }
}
