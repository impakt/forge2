import React, { useCallback, useState } from 'react';
import { connectModal, InjectedProps } from 'redux-modal';
import produce from 'immer';
import MODALS from '../../store/modal/modals';
import { ModifyStaticPropertiesModalProps } from '../../store/modal/actions';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from '@material-ui/core';
import useReduxState from '../../hooks/useReduxState';
import { getEntityById } from '../../store/engine/selectors';
import PropertyTypeField from './PropertyTypeField';
import useReduxDispatch from '../../hooks/useReduxDispatch';
import {
  modifyEntityPropertiesRequest,
  renameEntityRequest,
} from '../../store/engine/actions';

type Props = InjectedProps & ModifyStaticPropertiesModalProps & {};

const ModifyPropertiesModal: React.FC<Props> = ({
  handleHide,
  show,
  entityId,
}) => {
  const entity = useReduxState(getEntityById, entityId)!;
  const dispatch = useReduxDispatch();

  const [name, setName] = useState(entity.name);
  const [propertyValues, setPropertyValues] = useState<any[]>(
    entity.staticProperties.reduce<any[]>(
      (prev, cur) => {
        return [...prev, cur.value];
      },
      [] as any[],
    ),
  );

  const handlePropertyChange = useCallback(
    (index: number, value: any) => {
      const newValue = produce<any[]>(propertyValues, draft => {
        draft[index] = value;
      });
      setPropertyValues(newValue);

      // setPropertyValues({ ...propertyValues, [index]: value });
    },
    [propertyValues],
  );

  const handleSubmit = useCallback(
    (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();

      // Hide the modal before making updates.
      // This lets us keep the null check when getting the entity from state
      handleHide();

      if (name !== entity.name) {
        dispatch(renameEntityRequest({ entityId: entityId, newName: name }));
      }

      propertyValues.forEach((value, i) => {
        const property = entity.staticProperties[i];
        if (value !== property.value) {
          dispatch(
            modifyEntityPropertiesRequest({
              entityId,
              propertyIndex: i,
              newProperty: {
                ...property,
                value,
              },
            }),
          );
        }
      });
    },
    [name, propertyValues],
  );

  return (
    <Dialog open={show} onClose={handleHide} fullWidth>
      <DialogTitle>Edit Properties for {entity.name}</DialogTitle>

      <DialogContent>
        <form id="modify-properties" onSubmit={handleSubmit}>
          <div>
            <TextField
              fullWidth
              label="Name"
              value={name}
              onChange={e => setName(e.target.value)}
            />

            {entity.staticProperties.map((property, i) => (
              <PropertyTypeField
                label={property.name}
                key={property.name}
                onChange={value => handlePropertyChange(i, value)}
                description={
                  property.description ? property.description : undefined
                }
                type={property.type}
                value={propertyValues[i]}
              />
            ))}
          </div>
        </form>
      </DialogContent>

      <DialogActions>
        <Button variant="text" onClick={handleHide}>
          Cancel
        </Button>

        <Button
          color="primary"
          variant="contained"
          type="submit"
          form="modify-properties"
        >
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default connectModal({ name: MODALS.modifyStaticProperties })(
  ModifyPropertiesModal,
);
