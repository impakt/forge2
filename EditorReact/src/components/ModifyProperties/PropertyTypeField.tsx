import React from 'react';
import { Forge } from '../../engine/Packet_generated';
import {
  PropertyDataType,
  Vector3,
  Vector4,
} from '../../store/engineMiddleware/propertyDataToValue';
import {
  Checkbox,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  TextField,
} from '@material-ui/core';
import propertyTypeToString from '../../store/engineMiddleware/propertyTypeToString';
import {
  AnimatedTimelinePropertyValue,
  StaticEntityProperty,
} from '../../types/engine/properties';

type Props = {
  description?: string;
  label?: string;
  onChange: (
    newValue: NonNullable<PropertyDataType | AnimatedTimelinePropertyValue>,
  ) => any;
  property?: StaticEntityProperty;
  type: Forge.PropertyData | Forge.TimelineData | Forge.TimelineKeyData;
  value: PropertyDataType;
};

const VECTOR_WIDTH = 64;

const PropertyTypeField: React.FC<Props> = ({
  description,
  label,
  onChange,
  property,
  type,
  value,
  ...props
}) => {
  let control: any = null;

  switch (type) {
    case Forge.PropertyData.NONE:
    case Forge.TimelineData.NONE:
    case Forge.TimelineKeyData.NONE:
      return null;

    case Forge.PropertyData.BoolProperty:
    case Forge.TimelineData.BooleanTimeline:
    case Forge.TimelineKeyData.BooleanTimelineKey:
      return (
        <div>
          <FormControlLabel
            control={
              <Checkbox
                checked={value as boolean}
                onChange={e => onChange(e.target.checked)}
              />
            }
            label={label}
            style={{ marginTop: -8, marginBottom: -8 }}
          />
          <FormHelperText style={{ marginTop: 2 }}>
            {description}
          </FormHelperText>
        </div>
      );

    case Forge.PropertyData.IntegerProperty:
    case Forge.TimelineData.IntegerTimeline:
    case Forge.TimelineKeyData.IntegerTimelineKey:
      control = (
        <TextField
          label={label}
          type="number"
          onChange={e => onChange(+e.target.value)}
          // @ts-ignore
          step={1}
          min={0}
          value={value as number}
        />
      );
      break;

    case Forge.PropertyData.UnsignedIntegerProperty:
    case Forge.TimelineData.UnsignedIntegerTimeline:
    case Forge.TimelineKeyData.UnsignedIntegerTimelineKey:
      control = (
        <TextField
          label={label}
          type="number"
          onChange={e => onChange(+e.target.value)}
          // @ts-ignore
          step={1}
          value={value as number}
        />
      );
      break;

    case Forge.PropertyData.FloatProperty:
    case Forge.TimelineData.FloatTimeline:
    case Forge.TimelineKeyData.FloatTimelineKey:
      control = (
        <TextField
          label={label}
          type="number"
          onChange={e => onChange(+e.target.value)}
          value={value as number}
        />
      );
      break;

    case Forge.PropertyData.Vector3Property:
    case Forge.TimelineData.Vector3Timeline:
    case Forge.TimelineKeyData.Vector3TimelineKey:
      control = (
        <FormGroup row>
          <div style={{ alignSelf: 'center', marginRight: 16 }}>{label}:</div>

          <TextField
            label="X"
            type="number"
            onChange={e =>
              onChange({ ...(value as Vector3), x: +e.target.value })
            }
            value={(value as Vector3).x}
            style={{ marginRight: 8, width: VECTOR_WIDTH }}
          />

          <TextField
            label="Y"
            type="number"
            onChange={e =>
              onChange({ ...(value as Vector3), y: +e.target.value })
            }
            value={(value as Vector3).y}
            style={{ marginRight: 8, width: VECTOR_WIDTH }}
          />

          <TextField
            label="Z"
            type="number"
            onChange={e =>
              onChange({ ...(value as Vector3), z: +e.target.value })
            }
            value={(value as Vector3).z}
            style={{ width: VECTOR_WIDTH }}
          />
        </FormGroup>
      );
      break;

    case Forge.PropertyData.Vector4Property:
    case Forge.TimelineData.QuaternionTimeline:
    case Forge.TimelineKeyData.QuaternionTimelineKey:
      control = (
        <FormGroup row>
          <div style={{ alignSelf: 'center', marginRight: 16 }}>{label}:</div>

          <TextField
            label="X"
            type="number"
            onChange={e =>
              onChange({ ...(value as Vector4), x: +e.target.value })
            }
            value={(value as Vector4).x}
            style={{ marginRight: 8, width: VECTOR_WIDTH }}
          />

          <TextField
            label="Y"
            type="number"
            onChange={e =>
              onChange({ ...(value as Vector4), y: +e.target.value })
            }
            value={(value as Vector4).y}
            style={{ marginRight: 8, width: VECTOR_WIDTH }}
          />

          <TextField
            label="Z"
            type="number"
            onChange={e =>
              onChange({ ...(value as Vector4), z: +e.target.value })
            }
            value={(value as Vector4).z}
            style={{ marginRight: 8, width: VECTOR_WIDTH }}
          />

          <TextField
            label="W"
            type="number"
            onChange={e =>
              onChange({ ...(value as Vector4), w: +e.target.value })
            }
            value={(value as Vector4).w}
            style={{ width: VECTOR_WIDTH }}
          />
        </FormGroup>
      );
      break;

    case Forge.PropertyData.StringProperty:
      control = (
        <TextField
          fullWidth
          label={label}
          onChange={e => onChange(e.target.value)}
          value={value as string}
        />
      );
      break;
  }

  if (!control) {
    return null;
  }

  return (
    <div style={{ margin: '8px 0' }}>
      {control}

      <FormHelperText>
        [{propertyTypeToString(type)}] {description}
      </FormHelperText>
    </div>
  );
};

export default PropertyTypeField;
