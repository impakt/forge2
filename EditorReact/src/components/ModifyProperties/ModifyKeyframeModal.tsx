import React, { useCallback, useState } from 'react';
import { connectModal, InjectedProps } from 'redux-modal';
import MODALS from '../../store/modal/modals';
import { ModifyKeyframeModalProps } from '../../store/modal/actions';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormHelperText,
  TextField,
} from '@material-ui/core';
import useReduxState from '../../hooks/useReduxState';
import { getEntityById } from '../../store/engine/selectors';
import PropertyTypeField from './PropertyTypeField';
import { makeStyles } from '@material-ui/styles';
import redHue from '@material-ui/core/colors/red';
import useReduxDispatch from '../../hooks/useReduxDispatch';
import {
  modifyAnimatedKeyframeRequest,
  removeAnimatedKeyframeRequest,
} from '../../store/engine/actions';
import { snackbarActions } from 'material-ui-snackbar-redux';
import { styled } from '@material-ui/styles';
import { AnimatedTimelinePropertyValue } from '../../types/engine/properties';

const DestroyButton = styled(Button)({
  // background: `${redHue['500']} !important`,
  // color: 'white !important',
  // '&:hover': {
  //   backgroundColor: redHue['700'],
  // },

  background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
  border: 0,
  borderRadius: 3,
  boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
  color: 'white',
  height: 48,
  padding: '0 30px',
});

const useStyles = makeStyles(() => ({
  root: {
    background: redHue['500'],
    color: 'white',
    '&:hover': {
      backgroundColor: redHue['700'],
    },
  },
}));

type Props = InjectedProps & ModifyKeyframeModalProps;

const ModifyKeyframeModal: React.FC<Props> = ({
  handleHide,
  show,
  entityId,
  keyIndex,
  property,
  propertyIndex,
  time,
  value,
}) => {
  const entity = useReduxState(getEntityById, entityId)!;
  const dispatch = useReduxDispatch();

  const classes = useStyles();

  const [newTime, setNewTime] = useState(time);
  const [newValue, setNewValue] = useState(value);

  function handleDelete(e: React.MouseEvent<HTMLButtonElement>) {
    handleHide();
    dispatch(
      removeAnimatedKeyframeRequest({
        entityId,
        keyIndex,
        propertyIndex,
      }),
    );
  }

  const handleSubmit = useCallback(
    (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();

      dispatch(
        modifyAnimatedKeyframeRequest({
          entityId,
          keyIndex,
          propertyIndex,
          newTime,
          newValue,
          valueType: property.type,
        }),
      );

      handleHide();
    },
    [newTime, newValue],
  );

  function handleTimeChange(e: React.ChangeEvent<HTMLInputElement>) {
    return setNewTime(+e.target.value);
  }

  function handleChange(v: AnimatedTimelinePropertyValue) {
    setNewValue(v);
  }

  return (
    <Dialog open={show} onClose={handleHide} fullWidth>
      <DialogTitle>
        {entity.name} Keyframe at {time}s
      </DialogTitle>

      <DialogContent>
        <form id="keyframe" onSubmit={handleSubmit}>
          <TextField
            label="Time"
            type="number"
            onChange={handleTimeChange}
            value={newTime}
          />

          <FormHelperText>
            With precision, change the time in seconds of this frame
          </FormHelperText>

          <PropertyTypeField
            onChange={handleChange}
            description={
              property.description ? property.description : undefined
            }
            label={`${property.name}`}
            type={property.type}
            value={newValue}
          />
        </form>
      </DialogContent>

      <DialogActions>
        <Button variant="text" onClick={handleHide}>
          Cancel
        </Button>

        <Button
          className={classes.root}
          variant="contained"
          onClick={handleDelete}
        >
          Delete
        </Button>

        <Button
          color="primary"
          variant="contained"
          type="submit"
          form="keyframe"
        >
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default connectModal({ name: MODALS.modifyKeyframeValue })(
  ModifyKeyframeModal,
);
