import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React, { useEffect, useRef, useState } from 'react';
import { useField, useForm } from 'react-final-form-hooks';
import { connectModal, InjectedProps } from 'redux-modal';
import MODALS from '../../store/modal/modals';
import { createEntityRequest } from '../../store/engine/actions';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles(theme => ({
  formControl: {
    // margin: theme.spacing.unit * 2,
    width: '100%',
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
}));

const validate = (values: Record<string, any>) => {
  const errors: Record<string, any> = {};

  if (!values.entityName) {
    errors.entityName = 'Required';
  }

  return errors;
};

type Props = InjectedProps & {};

const CreateEntityModal: React.FC<Props> = ({ handleHide, show }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [uploadedFile, setUploadedFile] = useState<File | null>(null);
  const fileUrl = useRef<string | null>(null);

  useEffect(() => {
    if (uploadedFile) {
      fileUrl.current = URL.createObjectURL(uploadedFile);
    }

    return () => {
      if (fileUrl.current) {
        URL.revokeObjectURL(fileUrl.current);
        fileUrl.current = null;
      }
    };
  }, [uploadedFile]);

  const { form, handleSubmit } = useForm({
    onSubmit: (values: any, form: any, callback: any) => {
      console.log(values);
      dispatch(createEntityRequest(values));
    },
    validate,
  });

  const typeIndex = useField('typeIndex', form);
  const entityName = useField('entityName', form);
  const outputPath = useField('outputPath', form);

  return (
    <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      open={show}
      onClose={handleHide}
      fullWidth
    >
      <form onSubmit={handleSubmit}>
        <DialogTitle>Create Entity</DialogTitle>
        <DialogContent>
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="entity-type">Entity Type</InputLabel>
            <Select
              {...typeIndex.input}
              inputProps={{
                id: 'entity-type',
              }}
            >
              <MenuItem value={0} disabled>
                <em>Unknown</em>
              </MenuItem>

              {/*
              {Object.entries(Forge.EntityType).map(([name, value]) => {
                if (value === 0) {
                  return null;
                }

                return <MenuItem value={value}>{name}</MenuItem>;
              })}
*/}
            </Select>

            <FormControl className={classes.formControl}>
              <TextField
                label="Entity Name"
                {...entityName.input}
                error={entityName.meta.touched && entityName.meta.error}
              />
              {entityName.meta.touched && entityName.meta.error && (
                <FormHelperText error>{entityName.meta.error}</FormHelperText>
              )}
            </FormControl>

            {typeIndex.input.value === 'test' && (
              <React.Fragment>
                <FormControl className={classes.formControl}>
                  <TextField label="Output Path" {...outputPath.input} />
                </FormControl>

                <FormControl className={classes.formControl}>
                  <label htmlFor="file">
                    <input
                      accept="*/*"
                      id="file"
                      type="file"
                      onChange={e => {
                        const files = e.currentTarget.files;
                        if (files) {
                          const file = files[0];
                          if (file) {
                            setUploadedFile(file);
                          }
                        }
                      }}
                    />
                  </label>
                </FormControl>
              </React.Fragment>
            )}
          </FormControl>
        </DialogContent>

        <DialogActions>
          <Button variant="text" onClick={handleHide}>
            Cancel
          </Button>
          <Button variant="contained" color="primary" type="submit">
            Create
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default connectModal({ name: MODALS.createEntity })(CreateEntityModal);
