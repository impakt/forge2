import React from 'react';

type Props = {
  count: number;
  tickWidth: number;
  width: number;
};

const TimelineTicks: React.FC<Props> = ({ count, tickWidth, width }) => {
  const range = [];
  for (let i = 0; i < count; i++) {
    range.push(i);
  }

  return (
    <div style={{ display: 'flex', width }}>
      <div className="timeline-property--name-row" />

      {range.map(key => (
        <div
          key={key}
          className="timeline-tick"
          style={{ width: tickWidth - 16 }}
        >
          {key}
        </div>
      ))}
    </div>
  );
};

export default TimelineTicks;
