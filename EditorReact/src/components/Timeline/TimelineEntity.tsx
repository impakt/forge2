import { Button, Typography } from '@material-ui/core';
import classNames from 'classnames';
import { makeStyles } from '@material-ui/styles';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import redHue from '@material-ui/core/colors/red';
import {
  makeGetEntityAnimatedProperties,
  makeGetFilteredEntityById,
} from '../../store/engine/selectors';
import { RootState } from '../../types/store';
import EntityPropertyTimeline from './EntityPropertyTimeline';
import { destroyEntityRequest } from '../../store/engine/actions';
import { openModifyStaticPropertiesModal } from '../../store/modal/actions';
import { AnimatedEntityProperty } from '../../types/engine/properties';

const useStyles = makeStyles(theme => ({
  heading: {
    fontSize: theme.typography.pxToRem(16),
    fontWeight: theme.typography.fontWeightMedium,
  },
  destroyButton: {
    backgroundColor: redHue['500'],
    color: 'white',
    '&:hover': {
      backgroundColor: redHue['700'],
    },
  },
}));

type OwnProps = {
  id: number;
  tickCount: number;
  tickWidth: number;
  width: number;
};

type Props = typeof mapDispatchToProps &
  ReturnType<ReturnType<typeof mapStateToProps>> &
  OwnProps;

const TimelineEntity: React.FC<Props> = ({
  animatedProperties,
  entity,
  id,
  onDestroyEntity,
  onModifyProperties,
  tickWidth,
  width,
}) => {
  const classes = useStyles();

  const [hovered, setIsHovering] = useState<null | number>(null);

  if (!entity) {
    return null;
  }

  function handleMouseOver(index: number) {
    setIsHovering(index);
  }

  function handleMouseOut() {
    setIsHovering(null);
  }

  return (
    <div className="timeline-entity">
      <div className="timeline-entity--name-column">
        <Typography variant="h6" style={{ marginBottom: 8 }}>
          {entity.name}
        </Typography>

        {animatedProperties.map(
          (property: AnimatedEntityProperty, i: number) => (
            <div
              className={classNames('timeline-property--name-row', {
                highlighted: hovered === i,
              })}
              onMouseOver={() => handleMouseOver(i)}
              onMouseOut={handleMouseOut}
            >
              {property.name}
            </div>
          ),
        )}

        <div style={{ display: 'flex' }}>
          <Button
            className={classes.destroyButton}
            onClick={() => onDestroyEntity(entity.id)}
            variant="contained"
          >
            Destroy Entity
          </Button>

          <Button
            onClick={() =>
              onModifyProperties({
                entityId: id,
              })
            }
            color="primary"
            variant="contained"
          >
            Modify Properties
          </Button>
        </div>
      </div>

      <div className="timeline-entity--timeline-section">
        {animatedProperties.map(
          (property: AnimatedEntityProperty, i: number) => (
            <EntityPropertyTimeline
              entityId={id}
              highlighted={hovered === i}
              onMouseOver={() => handleMouseOver(i)}
              onMouseOut={handleMouseOut}
              property={property}
              propertyIndex={i}
              tickWidth={tickWidth}
              width={width}
            />
          ),
        )}
      </div>
    </div>
  );
};

const mapDispatchToProps = {
  onDestroyEntity: destroyEntityRequest,
  onModifyProperties: openModifyStaticPropertiesModal,
};

const mapStateToProps = () => {
  const getEntity = makeGetFilteredEntityById();
  const getAnimatedProperties = makeGetEntityAnimatedProperties();

  return (state: RootState, { id }: { id: number }) => ({
    // @ts-ignore
    animatedProperties: getAnimatedProperties(state, id),
    // @ts-ignore
    entity: getEntity(state, id),
  });
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(React.memo(TimelineEntity));
