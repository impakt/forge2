import React from 'react';
import { Slider } from '@material-ui/core';
import useReduxDispatch from '../../hooks/useReduxDispatch';
import useReduxState from '../../hooks/useReduxState';
import { getCurrentTime } from '../../store/engine/selectors';
import { changeCurrentTime } from '../../store/engine/actions';
import { MAX_TIME } from '../../store/engineMiddleware/constants';

const MIN = 0;
const STEP = 0.01;

type Props = {
  width: number;
};

const Timebar: React.FC<Props> = ({ width }) => {
  const dispatch = useReduxDispatch();
  const currentTime = useReduxState(getCurrentTime);

  function changeTime(newTime: number) {
    dispatch(changeCurrentTime(newTime));
  }

  function handleSliderChange(e: any, value: any) {
    changeTime(value);
  }

  function handleFieldChange(e: React.ChangeEvent<HTMLInputElement>) {
    changeTime(e.target.valueAsNumber);
  }

  return (
    <div style={{ display: 'flex', alignItems: 'center' }}>
      <input
        type="number"
        onChange={handleFieldChange}
        value={currentTime}
        min={MIN}
        step={STEP}
        max={MAX_TIME}
        style={{ marginRight: 24, width: 64 }}
      />

      <Slider
        aria-labelledby="label"
        min={MIN}
        step={STEP}
        max={MAX_TIME}
        onChange={handleSliderChange}
        value={currentTime}
        style={{ position: 'absolute', left: 256, width: width - 256 }}
      />
    </div>
  );
};

export default Timebar;
