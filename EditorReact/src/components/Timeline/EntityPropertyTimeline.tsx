import React from 'react';
import { createAnimatedKeyFrameRequest } from '../../store/engine/actions';
import { openModifyKeyframeModal } from '../../store/modal/actions';
import useReduxDispatch from '../../hooks/useReduxDispatch';
import classNames from 'classnames';
import { AnimatedEntityProperty } from '../../types/engine/properties';

type Props = Omit<React.HTMLProps<HTMLDivElement>, 'property'> & {
  entityId: number;
  highlighted?: boolean;
  property: AnimatedEntityProperty;
  propertyIndex: number;
  tickWidth: number;
  width: number;
};

/*
This is roughly 256 + some adjustment to line up the timeline
 */
const ADJUSTMENTS = 262;

const EntityPropertyTimeline: React.FC<Props> = ({
  entityId,
  highlighted,
  property,
  propertyIndex,
  tickWidth,
  width,
  ...props
}) => {
  const dispatch = useReduxDispatch();

  function handleDoubleClick(e: React.MouseEvent<HTMLDivElement>) {
    e.persist();
    const time = Math.max(0, (e.screenX - ADJUSTMENTS) / tickWidth);

    dispatch(
      createAnimatedKeyFrameRequest({
        propertyIndex,
        entityId,
        timeInS: time,
        valueType: property.type,
        value: false,
      }),
    );
  }

  return (
    <div
      className={classNames('timeline-entity-timeline', {
        highlighted,
      })}
      style={{ width }}
      onDoubleClick={handleDoubleClick}
      {...props}
    >
      {property.timeline &&
        property.timeline.map((key, i) => {
          return (
            <div
              key={key.time}
              className="timeline-key"
              title={`Time: ${key.time}\nValue: ${JSON.stringify(
                key.value,
              )}\n\nClick to edit`}
              style={{
                left: key.time * tickWidth + 10, // accounts for border and padding
              }}
              onClick={() =>
                dispatch(
                  openModifyKeyframeModal({
                    entityId,
                    property,
                    keyIndex: i,
                    propertyIndex,
                    time: key.time,
                    value: key.value,
                  }),
                )
              }
            />
          );
        })}
    </div>
  );
};

export default React.memo(EntityPropertyTimeline);
