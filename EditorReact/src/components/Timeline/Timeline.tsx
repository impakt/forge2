import React, { useState } from 'react';
import useReduxState from '../../hooks/useReduxState';
import TimelineEntity from './TimelineEntity';
import TimelineTicks from './TimelineTicks';
import { getTimelineEntities } from '../../store/engine/selectors';
import './Timeline.css';
import Timebar from './Timebar';
import { MAX_TIME } from '../../store/engineMiddleware/constants';

const Timeline: React.FC = () => {
  const entities = useReduxState(getTimelineEntities);

  const [tickWidth, setTickWidth] = useState(64);

  // function changeZoom(e: React.ChangeEvent<HTMLInputElement>) {
  //   setTickWidth(+e.target.value);
  // }

  if (!entities) {
    return null;
  }

  // console.log(entities);

  const width = MAX_TIME * tickWidth + 256 + MAX_TIME + 1;

  return (
    <React.Fragment>
      <div className="timeline-header" style={{ width }}>
        <TimelineTicks count={MAX_TIME} width={width} tickWidth={tickWidth} />
        <Timebar width={width} />
      </div>

      <div className="timeline" style={{ width }}>
        {entities.map(entity => (
          <TimelineEntity
            key={entity.id}
            id={entity.id}
            tickCount={MAX_TIME}
            tickWidth={tickWidth}
            width={width}
          />
        ))}
      </div>
    </React.Fragment>
  );
};

export default Timeline;
