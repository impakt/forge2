import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import React from 'react';

const PropertyElements: React.FC = () => {
  return (
    <Grid
      container
      justify="space-between"
      direction="column"
      wrap="nowrap"
      style={{ height: '100%', overflowY: 'auto', overflowX: 'hidden' }}
    >
      <Grid container direction="column" spacing={8} wrap="nowrap">
        <Grid item>
          <Typography variant="h6">Property Section</Typography>
        </Grid>

        <Grid item>
          <TextField label="ID" value="123456" disabled />
        </Grid>

        <Grid item>
          <TextField label="Name" />
        </Grid>
      </Grid>

      <Grid container justify="space-between">
        <Grid item>
          <Button variant="text">Reset</Button>
        </Grid>

        <Grid item>
          <Button variant="contained" color="primary">
            Save
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default PropertyElements;
