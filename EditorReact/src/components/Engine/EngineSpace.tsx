import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React, { useRef } from 'react';
import { getLoadedDemoFileName } from '../../store/demoFile/selectors';
import EngineCanvas from './EngineCanvas';
import LoadFileCta from './LoadFileCTA';
import useReduxState from '../../hooks/useReduxState';
import { isEngineDead } from '../../store/engine/selectors';

const useStyles = makeStyles(theme => ({
  content: {
    background: 'transparent',
    flexGrow: 1,
    minWidth: 0, // So the Typography noWrap works
    padding: `${theme.spacing.unit * 3} 0`,
    height: 'calc(100% - 64px)',
  },
  drawerPaper: {
    position: 'relative',
    width: 240,
  },
  fileCta: {
    height: '100%',
    margin: -24,
    display: 'flex',
    paddingLeft: 64,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  deadEngine: {
    backgroundColor: theme.palette.grey.A400,
    height: '100%',
    margin: -24,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  engine: {
    background: 'transparent',
  },
  toolbar: theme.mixins.toolbar,
}));

const EngineSpace: React.FC = () => {
  const isDead = useReduxState(isEngineDead);
  const loadedDemoFile = useReduxState(getLoadedDemoFileName);

  const engineSpaceRef = useRef(null);
  const classes = useStyles();

  let showFileCTA = false;
  if (!isDead && !loadedDemoFile) {
    showFileCTA = true;
  }

  let engineSpace;
  if (isDead) {
    engineSpace = (
      <div className={classes.deadEngine} ref={engineSpaceRef}>
        <Typography color="error" variant="h2">
          Engine not connected
        </Typography>
      </div>
    );
  } else if (showFileCTA) {
    engineSpace = (
      <div className={classes.fileCta}>
        <LoadFileCta />
      </div>
    );
  } else {
    engineSpace = (
      <div className={classes.engine} ref={engineSpaceRef}>
        <EngineCanvas />
      </div>
    );
  }

  return (
    <main className={classes.content}>
      {/* Adds spacing to account for the toolbar */}
      <div className={classes.toolbar} />

      {engineSpace}
    </main>
  );
};

export default EngineSpace;
