import { Button, Typography } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from '@material-ui/styles';
import { Field, Form, Formik } from 'formik';
import { TextField } from 'formik-material-ui';
import React from 'react';
import useReduxAction from '../../hooks/useReduxAction';
import useReduxState from '../../hooks/useReduxState';
import { loadDemo } from '../../store/demoFile/actions';
import { getLoadedDemoFileName } from '../../store/demoFile/selectors';

const useStyles = makeStyles(() => ({
  input: {
    display: 'none',
  },
}));

const LoadFileCta: React.FC = () => {
  const classes = useStyles();
  const currentFile = useReduxState(getLoadedDemoFileName);
  const doLoadDemo = useReduxAction(loadDemo);

  return (
    <Formik
      initialValues={{
        fileName: currentFile || 'res/data/atparty2019.demo',
      }}
      onSubmit={values => doLoadDemo(values.fileName)}
      render={({ setFieldValue }) => (
        <React.Fragment>
          <Typography color="primary" variant="h2" style={{ marginBottom: 8 }}>
            No File Loaded
          </Typography>

          <Typography
            color="textSecondary"
            variant="subtitle1"
            style={{ marginBottom: 16 }}
          >
            Type in the resource file. It should end in <code>.demo</code>
          </Typography>

          <Form>
            <Grid
              alignContent="center"
              container
              spacing={2}
              direction="column"
            >
              <Grid item>
                <Field
                  autoFocus
                  component={TextField}
                  name="fileName"
                  label="File Name"
                  helperText="Default: res/data/atparty2019.demo"
                  style={{ width: 500 }}
                />
              </Grid>

              <Grid
                container
                justify="space-between"
                style={{ padding: '0 8px' }}
              >
                <Grid item>
                  <label htmlFor="file">
                    <input
                      accept="*/*"
                      className={classes.input}
                      id="file"
                      type="file"
                      onChange={e => {
                        const files = e.currentTarget.files;
                        if (files) {
                          const fileName = files[0].name;
                          if (fileName) {
                            setFieldValue('fileName', fileName);
                          }
                        }
                      }}
                    />
                    <Tooltip
                      title="WARNING: Not fully implemented, may crash server"
                      color="primary"
                      placement="bottom"
                    >
                      <Button
                        color="primary"
                        variant="outlined"
                        component="span"
                      >
                        Browse
                      </Button>
                    </Tooltip>
                  </label>
                </Grid>

                <Grid item>
                  <Button color="primary" type="submit" variant="contained">
                    Load
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Form>
        </React.Fragment>
      )}
    />
  );
};

export default LoadFileCta;
