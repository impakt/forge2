import React, { useEffect, useRef, useState } from 'react';
import useReduxContext from '../../hooks/useReduxContext';
import { toggleFreelookRequest } from '../../store/engine/actions';
import useCameraControls from '../../hooks/useCameraControls';
import { RESOLUTION } from '../../store/engineMiddleware/constants';

function handleContextMenu(e: React.MouseEvent<HTMLCanvasElement>) {
  e.preventDefault();
  e.stopPropagation();
}

const EngineCanvas: React.FC = () => {
  const canvasRef = useRef<HTMLCanvasElement | null>(null);
  const contextRef = useRef<CanvasRenderingContext2D | null>(null);
  const imageRef = useRef<HTMLImageElement | null>(null);

  const storeContext = useReduxContext();
  const [pointerLocked, setPointerLocked] = useState(false);

  function handleClick(e: React.MouseEvent<HTMLCanvasElement>) {
    const mouseX = e.clientX;
    const mouseY = e.clientY;
    const rect = e.currentTarget.getBoundingClientRect();

    const x = (mouseX - rect.left) / rect.width;
    const y = (mouseY - rect.top) / rect.height;

    console.log(x, y);
  }

  function handleMouseDown(e: React.MouseEvent<HTMLCanvasElement>) {
    // Right click
    if (e.button === 2) {
      // @ts-ignore
      e.target.requestPointerLock();
      setPointerLocked(true);
    }
  }

  function handleMouseUp() {
    document.exitPointerLock();
    setPointerLocked(false);
  }

  useEffect(() => {
    storeContext.store.dispatch(toggleFreelookRequest());
  }, [canvasRef.current]);

  useEffect(() => {
    if (canvasRef.current) {
      contextRef.current = canvasRef.current.getContext('2d');
    }
  }, [canvasRef.current]);

  useEffect(() => {
    if (contextRef.current) {
      storeContext.store.dispatch({
        type: 'UPDATE_IMAGE_FUNCTION',
        payload: {
          imageRef: imageRef.current,
          renderFrame: contextRef.current.drawImage.bind(contextRef.current),
        },
      });
    }
  }, [contextRef.current]);

  useCameraControls(pointerLocked, canvasRef);

  return (
    <React.Fragment>
      <canvas
        ref={canvasRef}
        width={RESOLUTION[0]}
        height={RESOLUTION[1]}
        onClick={handleClick}
        onMouseDown={handleMouseDown}
        onContextMenu={handleContextMenu}
        onMouseUp={handleMouseUp}
        style={{ position: 'absolute', top: 64 }}
      />

      <img
        ref={imageRef}
        // style={{ width: RESOLUTION[0], height: RESOLUTION[1] }}
      />
    </React.Fragment>
  );
};

export default EngineCanvas;
