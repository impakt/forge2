import React from 'react';
import { Button } from '@material-ui/core';
import { ButtonProps } from '@material-ui/core/Button';
import useSaveFile from '../../hooks/useSaveFile';

type Props = Partial<Omit<ButtonProps, 'onClick'>> & {};

const SaveButton: React.FC<Props> = props => {
  const saveFile = useSaveFile();

  if (!saveFile) {
    return null;
  }

  return (
    <Button {...props} onClick={saveFile}>
      Save
    </Button>
  );
};

export default SaveButton;
