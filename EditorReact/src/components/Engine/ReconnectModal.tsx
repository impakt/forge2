import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import React, { useState } from 'react';
import { CircularProgress, FormHelperText } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import useReduxAction from '../../hooks/useReduxAction';
import useReduxState from '../../hooks/useReduxState';
import {
  getConnectionFailureReason,
  isEngineConnecting,
} from '../../store/engine/selectors';
import { beginEngineConnection } from '../../store/engine/actions';

const useStyles = makeStyles({
  paper: {
    overflow: 'hidden',
  },
});

type Props = {
  open: boolean;
};

const ReconnectModal: React.FC<Props> = ({ open }) => {
  const classes = useStyles();

  const connectEngine = useReduxAction(beginEngineConnection);
  const failureReason = useReduxState(getConnectionFailureReason);
  const connecting = useReduxState(isEngineConnecting);
  // const [url, setUrl] = useState('ws://localhost:8005');
  const [url, setUrl] = useState('ws://10.0.0.236:8005');

  function handleSubmit(e: React.FormEvent) {
    e.preventDefault();
    connectEngine(url);
  }

  return (
    <Dialog open={open} classes={classes}>
      <DialogTitle>Connect</DialogTitle>
      <form onSubmit={handleSubmit}>
        <DialogContent>
          <DialogContentText>
            Enter a URI to connect to. It defaults to the localhost version. The
            protocol is required to be <code>ws://</code>.
          </DialogContentText>

          <TextField
            autoFocus
            name="url"
            error={!!failureReason}
            margin="dense"
            label="Engine Connection URL"
            fullWidth
            onChange={e => setUrl(e.target.value)}
            value={url}
          />

          {failureReason && (
            <FormHelperText error>{failureReason}</FormHelperText>
          )}
        </DialogContent>
        <DialogActions>
          {connecting ? (
            <CircularProgress style={{ margin: 16, textAlign: 'right' }} />
          ) : (
            <Button type="submit" color="primary" variant="outlined">
              Connect
            </Button>
          )}
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default ReconnectModal;
