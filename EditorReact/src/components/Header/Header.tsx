import React from 'react';
import Toolbar from './Toolbar';

const Header: React.FC = () => <Toolbar />;

export default Header;
