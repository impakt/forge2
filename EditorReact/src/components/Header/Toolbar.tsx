import {
  AppBar,
  Button,
  Toolbar as MUIToolbar,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import { show } from 'redux-modal';
import useReduxState from '../../hooks/useReduxState';
import MODALS from '../../store/modal/modals';
import CreateEntityModal from '../CreateEntity/CreateEntityModal';
import { getDemoName, isEngineDead } from '../../store/engine/selectors';
import SaveButton from '../Engine/SaveButton';
import useReduxDispatch from '../../hooks/useReduxDispatch';
import { reloadResourcesRequest } from '../../store/engine/actions';

const useStyles = makeStyles(theme => ({
  root: {},
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  toolbar: theme.mixins.toolbar,
}));

const Toolbar: React.FC = () => {
  const classes = useStyles();

  const dispatch = useReduxDispatch();
  const engineDead = useReduxState(isEngineDead);
  const demoName = useReduxState(getDemoName);

  function handleCreateEntity() {
    dispatch(show(MODALS.createEntity));
  }

  function handleReloadResources() {
    dispatch(reloadResourcesRequest());
  }

  return (
    <AppBar className={classes.appBar} position="absolute">
      <MUIToolbar>
        <Typography variant="h6" color="inherit">
          {demoName || 'Editor'}
        </Typography>

        <SaveButton style={{ color: '#fff', margin: '0 16px' }} />

        <div className={classes.flex} />

        {!engineDead && (
          <React.Fragment>
            <Button
              variant="text"
              onClick={handleReloadResources}
              style={{ color: '#fff', marginRight: 8 }}
            >
              Reload Resources
            </Button>

            <Button
              variant="outlined"
              color="secondary"
              onClick={handleCreateEntity}
            >
              Create Entity
            </Button>
          </React.Fragment>
        )}

        <CreateEntityModal />
      </MUIToolbar>
    </AppBar>
  );
};

export default Toolbar;
