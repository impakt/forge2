import { makeStyles } from '@material-ui/styles';
import React, { useEffect } from 'react';
import useReduxActions from '../../hooks/useReduxActions';
import useReduxState from '../../hooks/useReduxState';
import EngineSpace from '../Engine/EngineSpace';
import ReconnectModal from '../Engine/ReconnectModal';
import Header from '../Header/Header';
import {
  toggleFreelookRequest,
  togglePlayPause,
} from '../../store/engine/actions';
import { isEngineDead } from '../../store/engine/selectors';
import { hasLoadedDemoFile } from '../../store/demoFile/selectors';
import Timeline from '../Timeline/Timeline';
import ModifyPropertiesModal from '../ModifyProperties/ModifyPropertiesModal';
import useSaveFile from '../../hooks/useSaveFile';
import ModifyKeyframeModal from '../ModifyProperties/ModifyKeyframeModal';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexGrow: 1,
    height: '100%',
    overflow: 'hidden',
    position: 'relative',
    zIndex: 1,
  },
}));

const App = () => {
  const classes = useStyles();

  const engineDead = useReduxState(isEngineDead);
  const hasFile = useReduxState(hasLoadedDemoFile);

  const [playPause, freeLook] = useReduxActions([
    togglePlayPause,
    toggleFreelookRequest,
  ]);

  const saveFile = useSaveFile();

  useEffect(() => {
    document.addEventListener('keypress', e => {
      if (!document.activeElement || document.activeElement.tagName !== 'INPUT')
        switch (e.code) {
          case 'Space':
            return playPause();

          case 'KeyE':
            return freeLook();

          case 'KeyS':
            if (e.ctrlKey) {
              e.preventDefault();
              e.stopPropagation();

              if (saveFile) {
                saveFile();
              }

              return false;
            }
        }
    });
  }, []);

  return (
    <div className={classes.root}>
      <Header />

      <EngineSpace />

      {/* The height here is adjustable. At some point, make it resizeable and state-driven */}

      {hasFile && (
        <div className="timeline-container">
          <Timeline />
        </div>
      )}

      <ReconnectModal open={engineDead} />
      <ModifyPropertiesModal />
      <ModifyKeyframeModal />
    </div>
  );
};

export default App;
