import { createMuiTheme } from '@material-ui/core';
import ThemeProvider from '@material-ui/styles/ThemeProvider';
import { SnackbarProvider } from 'material-ui-snackbar-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './bootstrap.js';
import App from './components/App/App';
import './index.css';
import store from './store';

const theme = createMuiTheme();

function render() {
  ReactDOM.render(
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <SnackbarProvider
          SnackbarProps={{
            autoHideDuration: 3500,
            anchorOrigin: { vertical: 'bottom', horizontal: 'left' },
          }}
        >
          <App />
        </SnackbarProvider>
      </ThemeProvider>
    </Provider>,
    document.getElementById('root'),
  );
}

render();

// @ts-ignore
if (module.hot) {
  // @ts-ignore
  module.hot.accept('./components/App/App', () => {
    render();
  });
}
