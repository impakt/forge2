import { loadDemo, loadDemoFailure, loadDemoSuccess } from './actions';
import { engineConnectFailed, engineStateResponse } from '../engine/actions';
import produce from 'immer';
import { isType } from 'typescript-fsa';

type Statuses = 'loading' | 'none' | 'done' | 'error';

type State = {
  errors: string | null;
  file: string | null;
  status: Statuses;
};

const demoFileReducer = produce<State, any>(
  (state, action) => {
    if (isType(action, loadDemo)) {
      state.file = action.payload;
      state.status = 'loading';
      return;
    }

    if (isType(action, loadDemoSuccess)) {
      state.status = 'done';
      state.errors = null;
      return;
    }

    if (isType(action, loadDemoFailure)) {
      state.status = 'error';
      state.errors = "Failed to load file. It probably doesn't exist";
      return;
    }

    if (isType(action, engineConnectFailed)) {
      state.status = 'none';
      state.file = null;
      return;
    }

    if (isType(action, engineStateResponse)) {
      const project = action.payload.project;

      if (project) {
        state.status = 'done';
        state.file = project;
      }
      return;
    }
  },
  {
    errors: null,
    file: null,
    status: 'none',
  } as State,
);

export default demoFileReducer;
