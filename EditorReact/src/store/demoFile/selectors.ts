import createSelector from 'selectorator';
import { RootState } from '../../types/store';

type File = RootState['demoFile']['file'];

export const getLoadedDemoFileName = createSelector<RootState, File>([
  'demoFile.file',
]);

export const hasLoadedDemoFile = createSelector<RootState, boolean>(
  ['demoFile.file'],
  (file: File) => !!file,
);
