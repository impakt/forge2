import { put, take, takeLatest } from 'redux-saga/effects';
import { loadDemo, loadDemoSuccess } from './actions';
import { SagaIterator } from 'redux-saga';
import { queryEngineState, renderFrameRequest } from '../engine/actions';

function* handleLoadDemo(): SagaIterator {
  // const fileName = action.payload;
  yield take(loadDemoSuccess);
  yield put(queryEngineState());
  // yield put(renderFrameRequest(0));
  // yield put(loadDemo('res/data/atparty2019.demo'));
  // TODO: for failure handling, do a race
}

export default function* demoFileSaga(): SagaIterator {
  yield takeLatest(loadDemo, handleLoadDemo);
}
