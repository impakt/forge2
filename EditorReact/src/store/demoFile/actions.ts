import createEngineAction from '../../util/createEngineAction';
import actionCreatorFactory from 'typescript-fsa';

const actionCreator = actionCreatorFactory('demoFile');

type FileName = string;

export const loadDemo = createEngineAction<FileName>('loadDemo');
export const loadDemoSuccess = actionCreator('loadDemoSuccess');
export const loadDemoFailure = actionCreator('loadDemoFailed');
