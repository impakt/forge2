import {
  snackbarActions,
  snackbarActions as snackbar,
} from 'material-ui-snackbar-redux';
import { buffers } from 'redux-saga';
import {
  actionChannel,
  put,
  race,
  select,
  take,
  takeEvery,
  takeLatest,
} from 'redux-saga/effects';
import delay from '../../util/sagaDelay';
import { loadDemoSuccess } from '../demoFile/actions';
import {
  beginEngineConnection,
  changeCurrentTime,
  createAnimatedKeyFrameRequest,
  createAnimatedKeyFrameResponse,
  destroyEntityRequest,
  destroyEntityResponseSucceeded,
  engineConnected,
  engineConnectFailed,
  engineStateResponse,
  modifyAnimatedKeyframeRequest,
  modifyAnimatedKeyframeResponse,
  modifyAnimatedKeyframeResponseFailed,
  modifyEntityPropertiesRequest,
  modifyEntityPropertiesResponse,
  modifyViewport,
  queryEngineState,
  queryEntitiesRequest,
  queryEntitiesResponse,
  queryEntityAnimatedProperties,
  queryEntityAnimatedPropertiesResponse,
  queryEntityPropertiesById,
  queryEntityPropertiesRequest,
  queryEntityPropertiesResponse,
  queryInterfaceVersionRequest,
  queryInterfaceVersionResponse,
  removeAnimatedKeyframeRequest,
  removeAnimatedKeyframeResponse,
  renameEntityRequest,
  renameEntityResponse, renderBase64JpegFrameRequest,
  renderJpegFrameRequest,
  toggleFreelookRequest,
  updateEntityProperties,
} from './actions';
import { getCurrentTime, isEngineDead } from './selectors';
import * as Cookies from 'js-cookie';
import { RESOLUTION } from '../engineMiddleware/constants';

const lastSuccessfulConnectionCookie = 'last_successful_connection';

function* reconnectSaga() {
  while (true) {
    const isDead = yield select(isEngineDead);
    if (!isDead) {
      break;
    }

    // yield put(beginEngineConnection());
    yield delay(1000);
  }
}

function* handleBeginEngineConnection(
  action: ReturnType<typeof beginEngineConnection>,
) {
  const uri = action.payload;

  // We only care about successful connections.
  // Failures are handled by the reducer + UI
  const { success } = yield race({
    success: take(engineConnected),
    fail: take(engineConnectFailed),
  });

  if (success) {
    Cookies.set(lastSuccessfulConnectionCookie, encodeURIComponent(uri));
  }
}

function* getAllEntityProperties(
  action: ReturnType<typeof queryEntitiesResponse>,
) {
  const entities = action.payload;

  const chan = yield actionChannel(
    [queryEntityPropertiesResponse, queryEntityAnimatedPropertiesResponse],
    buffers.expanding(1000),
  );

  for (const entity of entities) {
    // Make requests to the engine
    yield put(queryEntityPropertiesRequest(entity.id));
    yield put(queryEntityAnimatedProperties(entity.id));
  }

  for (const entity of entities) {
    const staticProperties: ReturnType<
      typeof queryEntityPropertiesResponse
    > = yield take(chan);

    const animatedProperties: ReturnType<
      typeof queryEntityAnimatedPropertiesResponse
    > = yield take(chan);

    yield put(
      updateEntityProperties({
        id: entity.id,
        staticProperties: staticProperties.payload,
        animatedProperties: animatedProperties.payload,
      }),
    );
  }
}

function* handleQueryEntityById(
  action: ReturnType<typeof queryEntityPropertiesById>,
) {
  const id = action.payload;

  yield put(queryEntityPropertiesRequest(id));
  yield put(queryEntityAnimatedProperties(id));

  const staticProperties: ReturnType<
    typeof queryEntityPropertiesResponse
  > = yield take(queryEntityPropertiesResponse);

  const animatedProperties: ReturnType<
    typeof queryEntityAnimatedPropertiesResponse
  > = yield take(queryEntityAnimatedPropertiesResponse);

  yield put(
    updateEntityProperties({
      id,
      staticProperties: staticProperties.payload,
      animatedProperties: animatedProperties.payload,
    }),
  );
}

function* handleDemoLoaded() {
  yield put(queryEntitiesRequest());
}

function* handleConnection() {
  yield put(queryInterfaceVersionRequest());
  yield take(queryInterfaceVersionResponse);

  yield put(queryEngineState());
  yield put(
    modifyViewport({
      width: RESOLUTION[0],
      height: RESOLUTION[1],
    }),
  );

  const { payload } = yield take(engineStateResponse);
  // Cookies.set(lastSuccessfulConnectionCookie);

  if (payload.project) {
    // If there's a project loaded, call the loadedProject handler,
    // and also render a frame, because why not
    // yield put(renderFrameRequest(0));
    yield handleDemoLoaded();

    yield put(
      snackbar.show({
        message: 'Connected',
      }),
    );
  }
}

function* handleDestroyEntity(action: ReturnType<typeof destroyEntityRequest>) {
  const id = action.payload;

  yield take(destroyEntityResponseSucceeded);

  yield put(
    snackbar.show({
      message: 'Entity destroyed',
    }),
  );

  yield put(queryEntitiesRequest());
}

function* handleCreateKeyFrame(
  action: ReturnType<typeof createAnimatedKeyFrameRequest>,
) {
  const { entityId } = action.payload;

  // wait for the response...
  yield take(createAnimatedKeyFrameResponse);
  yield put(queryEntityPropertiesById(entityId));
}

function* handleRenameEntity(action: ReturnType<typeof renameEntityRequest>) {
  const { entityId } = action.payload;

  yield take(renameEntityResponse);

  yield put(
    snackbar.show({
      message: 'Entity renamed',
    }),
  );

  yield put(queryEntitiesRequest());
}

function* handleModifyProperty(
  action: ReturnType<typeof modifyEntityPropertiesRequest>,
) {
  const { entityId } = action.payload;
  yield take(modifyEntityPropertiesResponse);
  yield put(queryEntityPropertiesById(entityId));
}

function* handleRemoveKeyframe(
  action: ReturnType<typeof removeAnimatedKeyframeRequest>,
) {
  const { entityId } = action.payload;
  yield take(removeAnimatedKeyframeResponse);
  yield put(queryEntityPropertiesById(entityId));
}

function* handleModifyKeyframe(
  action: ReturnType<typeof modifyAnimatedKeyframeRequest>,
) {
  const { entityId } = action.payload;
  const { success, error } = yield race({
    success: take(modifyAnimatedKeyframeResponse),
    error: take(modifyAnimatedKeyframeResponseFailed),
  });

  if (success) {
    yield put(queryEntityPropertiesById(entityId));
    yield handleFrameChange();
  }

  if (error) {
    yield put(
      snackbarActions.show({
        message: 'Failed to update entity',
      }),
    );
  }
}

function* handleFrameChange() {
  const time: number = yield select(getCurrentTime);
  yield put(renderBase64JpegFrameRequest(time));
}

function* handleFrameChangeDelay() {
  yield delay(16);
  yield handleFrameChange();
}

export default function* engineSaga() {
  yield takeLatest(engineConnected, handleConnection);
  yield takeLatest(loadDemoSuccess, handleDemoLoaded);
  // yield takeLatest(engineConnectFailed, reconnectSaga);
  yield takeLatest(queryEntitiesResponse, getAllEntityProperties);
  yield takeLatest(destroyEntityRequest, handleDestroyEntity);
  yield takeLatest(renameEntityRequest, handleRenameEntity);
  yield takeLatest(modifyEntityPropertiesRequest, handleModifyProperty);
  yield takeLatest(removeAnimatedKeyframeRequest, handleRemoveKeyframe);
  yield takeLatest(modifyAnimatedKeyframeRequest, handleModifyKeyframe);
  yield takeLatest(queryEntityPropertiesById, handleQueryEntityById);
  yield takeLatest(createAnimatedKeyFrameRequest, handleCreateKeyFrame);
  yield takeEvery([engineStateResponse, changeCurrentTime], handleFrameChange);

  yield takeEvery(toggleFreelookRequest, handleFrameChangeDelay);

  // Try to connect to the most recently successful connection
  const lastConnection = Cookies.get(lastSuccessfulConnectionCookie);
  if (lastConnection) {
    yield put(beginEngineConnection(decodeURIComponent(lastConnection)));
  }

  yield takeLatest(beginEngineConnection, handleBeginEngineConnection);
}
