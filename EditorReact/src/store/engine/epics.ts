/*
 *
 * THIS FILE IS CURRENTLY UNUSED AND INACTIVE
 *
 * */

import { combineEpics, Epic } from 'redux-observable';
import { create } from 'rxjs-spy';
import { ActionCreator, AnyAction } from 'typescript-fsa';
import { map, mergeMap, switchMap, take } from 'rxjs/operators';
import { RootState } from '../../types/store';
import {
  createAnimatedKeyFrameRequest,
  createAnimatedKeyFrameResponse,
  destroyEntityResponseSucceeded,
  queryEntitiesRequest,
  queryEntityPropertiesById,
} from './actions';
import { ofAction } from '../ofAction';
import invariant from 'invariant';
import { snackbarActions as snackbar } from 'material-ui-snackbar-redux';
import { loadDemoSuccess } from '../demoFile/actions';

type ActionEpic = Epic<AnyAction, AnyAction, RootState>;

const spy = create();

function modifyAndUpdateEntity<
  B extends ActionCreator<any>,
  R extends ActionCreator<any>
>(base: B, response: R) {
  const epic: Epic<B, any, RootState> = action$ =>
    action$.pipe(
      ofAction(base),
      switchMap(action =>
        action$.pipe(
          ofAction(response),
          take(1),
          map(() => {
            if (typeof action.payload === 'number') {
              return queryEntityPropertiesById(action.payload);
            }

            invariant(
              !!action.payload.entityId,
              'Action passed to `modifyAndUpdateEntity` must have a payload with an entityId',
            );

            return queryEntityPropertiesById(action.payload.entityId);
          }),
        ),
      ),
    );

  return epic;
}

const handleDestroyedEntity: ActionEpic = action$ =>
  action$.pipe(
    ofAction(destroyEntityResponseSucceeded),
    mergeMap(() => [
      snackbar.show({
        message: 'Entity destroyed',
      }),
      queryEntitiesRequest(),
    ]),
  );

const handleCreateKeyframeRequest = modifyAndUpdateEntity(
  createAnimatedKeyFrameRequest,
  createAnimatedKeyFrameResponse,
);

const handleConnected: ActionEpic = action$ =>
  action$.pipe(
    ofAction(loadDemoSuccess),
    map(() => queryEntitiesRequest()),
  );

const engineEpics = combineEpics(
  handleDestroyedEntity,
  handleCreateKeyframeRequest,
  handleConnected,
);

export default engineEpics;
