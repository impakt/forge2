import createEngineAction from '../../util/createEngineAction';
import actionCreatorFactory from 'typescript-fsa';
import { Forge } from '../../engine/Packet_generated';
import {
  AnimatedEntityProperty,
  AnimatedTimelinePropertyValue,
  Entity,
  EntityType,
  StaticEntityProperty,
} from '../../types/engine/properties';

const actionCreator = actionCreatorFactory('engine');

export const changeCurrentTime = actionCreator<number>('changeCurrentTime');

export const beginEngineConnection = actionCreator<string>(
  'beginEngineConnection',
);
export const engineConnected = actionCreator('engineConnected');
export const engineConnectFailed = actionCreator<string | undefined>(
  'engineConnectFailed',
);
export const closeEngineConnection = actionCreator('closeEngineConnection');

export const queryInterfaceVersionRequest = createEngineAction(
  'queryInterfaceVersionRequest',
);
export const queryInterfaceVersionResponse = actionCreator<number>(
  'queryInterfaceVersionResponse',
);

export const modifyViewport = createEngineAction<{
  height: number;
  width: number;
}>('modifyViewport');

export const togglePlayPause = createEngineAction('togglePlayPause');

export const queryEntitiesRequest = createEngineAction('queryEntitiesRequest');
export const queryEntitiesResponse = actionCreator<Entity[]>(
  'queryEntitiesResponse',
);

export const queryEntityPropertiesById = actionCreator<number>(
  'queryEntityPropertiesById',
);

// export const updateEntity = actionCreator<{ id: number; newEntity: Entity }>(
//   'updateEntity',
// );

export const queryEntityPropertiesRequest = createEngineAction<number>(
  'queryEntitiesPropertiesRequest',
);
export const queryEntityPropertiesResponse = actionCreator<
  StaticEntityProperty[]
>('queryEntitiesPropertiesResponse');

export const queryEntityAnimatedProperties = createEngineAction<number>(
  'queryEntityAnimatedProperties',
);
export const queryEntityAnimatedPropertiesResponse = actionCreator<
  AnimatedEntityProperty[]
>('queryEntityAnimatedPropertiesResponse');

export const updateEntityProperties = actionCreator<{
  id: number;
  staticProperties?: StaticEntityProperty[];
  animatedProperties?: AnimatedEntityProperty[];
}>('updateEntityProperties');

/**
 * @deprecated
 */
export const renderFrameRequest = createEngineAction<number>(
  'renderFrameRequest',
);
export const renderJpegFrameRequest = createEngineAction<number>(
  'renderJpegFrameRequest',
);
export const renderBase64JpegFrameRequest = createEngineAction<number>(
  'renderBase64JpegFrameRequest',
);

export const queryEngineState = createEngineAction('queryEngineState');
export const engineStateResponse = actionCreator<{
  group: string | null;
  name: string | null;
  project: string | null;
}>('engineStateResponse');

export const createEntityRequest = createEngineAction<{
  entityName: string;
  typeIndex: EntityType;
  fileUrl?: string;
}>('createEntityRequest');

export const destroyEntityRequest = createEngineAction<number>(
  'destroyEntityRequest',
);
export const destroyEntityResponseSucceeded = actionCreator(
  'destroyEntityResponseSucceeded',
);
export const destroyEntityResponseFailed = actionCreator(
  'destroyEntityResponseFailed',
);

export const createAnimatedKeyFrameRequest = createEngineAction<{
  entityId: number;
  propertyIndex: number;
  timeInS: number;
  value: AnimatedTimelinePropertyValue;
  valueType: Forge.TimelineKeyData | Forge.TimelineData;
}>('createAnimatedKeyFrameRequest');
export const createAnimatedKeyFrameResponse = actionCreator(
  'createAnimatedKeyFrameResponse',
);

export const removeAnimatedKeyframe = actionCreator.async<
  {
    entityId: number;
    propertyIndex: number;
    keyIndex: number;
  },
  void,
  void
>('removeAnimatedKeyframe');

export const removeAnimatedKeyframeRequest = createEngineAction<{
  entityId: number;
  propertyIndex: number;
  keyIndex: number;
}>('removeAnimatedKeyframeRequest');
export const removeAnimatedKeyframeResponse = actionCreator(
  'removeAnimatedKeyframeResponse',
);

export const modifyAnimatedKeyframeRequest = createEngineAction<{
  entityId: number;
  propertyIndex: number;
  keyIndex: number;
  newTime: number;
  valueType: Forge.TimelineKeyData | Forge.TimelineData;
  newValue: AnimatedTimelinePropertyValue;
}>('modifyAnimatedKeyframeRequest');
export const modifyAnimatedKeyframeResponse = actionCreator(
  'modifyAnimatedKeyframeResponse',
);
export const modifyAnimatedKeyframeResponseFailed = actionCreator(
  'modifyAnimatedKeyframeResponseFailed',
  undefined,
  true,
);

export const renameEntityRequest = createEngineAction<{
  entityId: number;
  newName: string;
}>('renameEntityRequest');
export const renameEntityResponse = actionCreator('renameEntityResponse');

export const modifyEntityPropertiesRequest = createEngineAction<{
  entityId: number;
  newProperty: StaticEntityProperty;
  propertyIndex: number;
}>('modifyEntityPropertiesRequest');
export const modifyEntityPropertiesResponse = actionCreator(
  'modifyEntityPropertiesResponse',
);

export const updateCameraLocation = createEngineAction<{
  deltaPitch: number;
  deltaYaw: number;
  deltaX: number;
  deltaY: number;
  deltaZ: number;
}>('updateCameraLocation');

export const toggleFreelookRequest = createEngineAction(
  'toggleFreelookRequest',
);

export const reloadResourcesRequest = createEngineAction('reloadResources');

export const saveDemoRequest = createEngineAction<string>('saveDemoRequest');
