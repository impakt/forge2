import { produce } from 'immer';
import { isType } from 'typescript-fsa';
import {
  beginEngineConnection,
  changeCurrentTime,
  engineConnected,
  engineConnectFailed,
  engineStateResponse,
  queryEntitiesRequest,
  queryEntitiesResponse,
  updateEntityProperties,
} from './actions';
import { State } from '../../types/engine/properties';

const engineReducer = produce<State, any>(
  (state, action) => {
    if (isType(action, beginEngineConnection)) {
      state.reason = null;
      state.status = 'CONNECTING';
      return;
    }

    if (isType(action, engineConnected)) {
      state.reason = null;
      state.status = 'ALIVE';
      return;
    }

    if (isType(action, engineConnectFailed)) {
      state.reason = action.payload || state.reason;
      state.status = 'DEAD';
      state.name = null;
      state.group = null;
      state.project = null;
      return;
    }

    if (isType(action, queryEntitiesRequest)) {
      state.entities.normalized = {};
      state.entities.byId = [];
      return;
    }

    if (isType(action, queryEntitiesResponse)) {
      const items = action.payload;
      for (let i = 0; i < items.length; i++) {
        const item = items[i];
        state.entities.normalized[item.id] = item;
        if (!state.entities.byId.includes(item.id)) {
          state.entities.byId.push(item.id);
        }
      }
      return;
    }

    if (isType(action, updateEntityProperties)) {
      const { id, staticProperties, animatedProperties } = action.payload;
      if (!state.entities.normalized[id]) {
        throw new Error(
          "Tried to add properties to an entity that doesn't exist in Redux.",
        );
      }

      if (staticProperties) {
        state.entities.normalized[id].staticProperties = staticProperties;
      }

      if (animatedProperties) {
        state.entities.normalized[id].animatedProperties = animatedProperties;
      }
      return;
    }

    if (isType(action, engineStateResponse)) {
      const { group, name, project } = action.payload;
      state.group = group;
      state.name = name;
      state.project = project;
      return;
    }

    if (isType(action, changeCurrentTime)) {
      state.time = action.payload;
      return;
    }
  },
  {
    group: null,
    name: null,
    project: null,
    entities: {
      byId: [],
      normalized: {},
    },
    reason: null,
    status: 'DEAD',
    time: 0,
  } as State,
);

export default engineReducer;
