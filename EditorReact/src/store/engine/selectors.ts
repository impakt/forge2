import createSelector from 'selectorator';
import { RootState } from '../../types/store';
import { AnimatedEntityProperty, Entity, StaticEntityProperty } from '../../types/engine/properties';

export const isEngineConnecting = createSelector<RootState, boolean>(
  ['engine.status'],
  status => status === 'CONNECTING',
);

export const isEngineDead = createSelector<RootState, boolean>(
  ['engine.status'],
  status => status !== 'ALIVE',
);

export const getConnectionFailureReason = createSelector<
  RootState,
  RootState['engine']['reason']
>(['engine.reason']);

export const getTimelineEntities = createSelector<RootState, Entity[]>(
  ['engine.entities.byId', 'engine.entities.normalized'],
  (byId, normalized) => byId.map((id: number) => normalized[id]),
);

export const getEntityById = (
  state: RootState,
  id: number,
): Entity | undefined => state.engine.entities.normalized[id];

export const getEntityStaticProperties = (
  state: RootState,
  id: number,
): StaticEntityProperty[] => {
  const entity = getEntityById(state, id);
  if (!entity) {
    return [];
  }

  return entity.staticProperties;
};

export const makeGetFilteredEntityById = () =>
  createSelector<
    RootState,
    null | { id: Entity['id']; name: Entity['name']; typeIndex: Entity['typeIndex'] }
  >(
    [getEntityById],
    (entity?: Entity) => {
      if (!entity) {
        return null;
      }

      return {
        id: entity.id,
        name: entity.name,
        typeIndex: entity.typeIndex,
      };
    },
  );

export const makeGetEntityProperties = (entityId: number) =>
  createSelector<RootState, null | StaticEntityProperty[]>(
    ['engine.entities.normalized'],
    normalized => {
      const entity: Entity | undefined = normalized[entityId];

      if (entity) {
        return entity.staticProperties;
      }
      return null;
    },
  );

export const makeGetEntityAnimatedProperties = () =>
  createSelector<RootState, null | AnimatedEntityProperty[]>(
    [getEntityById],
    (entity: Entity) => {
      if (!entity) {
        return null;
      }

      return entity.animatedProperties;
    },
  );

export const getCurrentTime = (state: RootState) => state.engine.time;

export const getDemoName = createSelector(['engine.name']);
