import { combineEpics } from 'redux-observable';
import engineEpics from './engine/epics';

const rootEpic = combineEpics(engineEpics);

export default rootEpic;
