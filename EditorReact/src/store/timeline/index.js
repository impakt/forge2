import { createReducer } from 'redux-starter-kit';
import { queryEntityAnimatedPropertiesResponse } from '../engine';

const timelineReducer = createReducer(
  {
    totalTime: null,
    data: [],
  },
  {
    [queryEntityAnimatedPropertiesResponse]: (state, action) => {
      const { id: entityId, properties } = action.payload;

      for (const property of properties) {
        const { id, timeline } = property;
      }
    },
  },
);

export default timelineReducer;
