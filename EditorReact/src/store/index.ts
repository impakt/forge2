import createSagaMiddleware, { Monitor } from 'redux-saga';
import { configureStore, Middleware } from 'redux-starter-kit';
import engineMiddleware from './engineMiddleware/engineMiddleware';
import rootSaga from './rootSaga';
import rootReducer from './rootReducer';
import { RootState } from '../types/store';

function createStore() {
  /*const epicMiddleware = createEpicMiddleware<
    AnyAction,
    AnyAction,
    RootState
  >();*/

  // @ts-ignore
  const monitor: Monitor = window.__SAGA_MONITOR_EXTENSION__;

  const sagaMiddleware = createSagaMiddleware({ sagaMonitor: monitor });

  const store = configureStore({
    reducer: rootReducer,
    devTools: process.env.NODE_ENV !== 'production',
    middleware: [
      engineMiddleware() as Middleware<{}, RootState>,
      // epicMiddleware,
      sagaMiddleware,
    ],
    // preloadedState: initialState,
  });

  // @ts-ignore
  // epicMiddleware.run(rootEpic);
  sagaMiddleware.run(rootSaga);

  return store;
}

const store = createStore();

export default store;
