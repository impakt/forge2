import { snackbarReducer } from 'material-ui-snackbar-redux';
import { combineReducers } from 'redux';
import demoFileReducer from './demoFile/reducer';
import { reducer as modal } from 'redux-modal';
import engineReducer from './engine/reducer';

const rootReducer = combineReducers({
  demoFile: demoFileReducer,
  engine: engineReducer,
  modal,
  snackbar: snackbarReducer,
});

export default rootReducer;
