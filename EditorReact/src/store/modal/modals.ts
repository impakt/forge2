const MODALS = {
  createEntity: 'createEntity',
  modifyStaticProperties: 'modifyStaticProperties',
  modifyKeyframeValue: 'modifyKeyframeValue',
};

export default MODALS;
