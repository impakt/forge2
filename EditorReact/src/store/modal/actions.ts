import { show } from 'redux-modal';
import MODALS from './modals';
import { AnimatedEntityProperty, AnimatedTimelinePropertyValue } from '../../types/engine/properties';

export interface ModifyStaticPropertiesModalProps {
  entityId: number;
}

export interface ModifyKeyframeModalProps {
  entityId: number;
  keyIndex: number;
  property: AnimatedEntityProperty;
  propertyIndex: number;
  time: number;
  value: AnimatedTimelinePropertyValue;
}

export const openModifyStaticPropertiesModal = (
  props: ModifyStaticPropertiesModalProps,
) => show(MODALS.modifyStaticProperties, props);

export const openModifyKeyframeModal = (props: ModifyKeyframeModalProps) =>
  show(MODALS.modifyKeyframeValue, props);
