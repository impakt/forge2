import { fork } from 'redux-saga/effects';
import demoFileSaga from './demoFile/sagas';
import engineSaga from './engine/sagas';

export default function* rootSaga() {
  yield fork(engineSaga);
  yield fork(demoFileSaga);
}
