import { Forge } from '../../engine/Packet_generated';
import { Vector3, Vector4 } from './propertyDataToValue';
import { AnimatedTimelinePropertyValue } from '../../types/engine/properties';

export default function addDataToBuilder(
  builder: flatbuffers.Builder,
  valueType: Forge.TimelineKeyData,
  value: AnimatedTimelinePropertyValue,
) {
  let valueOffset: flatbuffers.Offset = 0;

  switch (valueType) {
    case Forge.TimelineKeyData.NONE:
      break;

    case Forge.TimelineKeyData.BooleanTimelineKey:
      Forge.BoolProperty.startBoolProperty(builder);
      Forge.BoolProperty.addValue(builder, value as boolean);
      valueOffset = Forge.BoolProperty.endBoolProperty(builder);
      break;

    case Forge.TimelineKeyData.IntegerTimelineKey:
      Forge.IntegerProperty.startIntegerProperty(builder);
      Forge.IntegerProperty.addValue(builder, value as number);
      valueOffset = Forge.IntegerProperty.endIntegerProperty(builder);
      break;

    case Forge.TimelineKeyData.UnsignedIntegerTimelineKey:
      Forge.UnsignedIntegerProperty.startUnsignedIntegerProperty(builder);
      Forge.UnsignedIntegerProperty.addValue(builder, value as number);
      valueOffset = Forge.UnsignedIntegerProperty.endUnsignedIntegerProperty(
        builder,
      );
      break;

    case Forge.TimelineKeyData.FloatTimelineKey:
      Forge.FloatProperty.startFloatProperty(builder);
      Forge.FloatProperty.addValue(builder, value as number);
      valueOffset = Forge.FloatProperty.endFloatProperty(builder);
      break;

    case Forge.TimelineKeyData.Vector3TimelineKey: {
      const { x, y, z } = value as Vector3;
      const vectorOffset = Forge.Vector3.createVector3(
        builder,
        x,
        y,
        z,
      ) as flatbuffers.Offset;

      Forge.Vector3Property.startVector3Property(builder);
      Forge.Vector3Property.addValue(builder, vectorOffset);
      valueOffset = Forge.Vector3Property.endVector3Property(builder);
      break;
    }

    case Forge.TimelineKeyData.QuaternionTimelineKey:
      const { x, y, z, w } = value as Vector4;
      const quatOffset = Forge.Vector4.createVector4(
        builder,
        x,
        y,
        z,
        w,
      ) as flatbuffers.Offset;

      Forge.Vector4Property.startVector4Property(builder);
      Forge.Vector4Property.addValue(builder, quatOffset);
      valueOffset = Forge.Vector4Property.endVector4Property(builder);
      break;
  }

  return valueOffset;
}
