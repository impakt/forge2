import { flatbuffers } from 'flatbuffers';
import { Forge } from '../../engine/Packet_generated';
import isActionForEngine from '../../util/isActionForEngine';
import { Dispatch, Store } from 'redux';
import {
  beginEngineConnection,
  closeEngineConnection,
  engineConnected,
  engineConnectFailed,
} from '../engine/actions';
import { isEngineDead } from '../engine/selectors';
import actionToPacket from './actionToPacket';
import socketMessageHandler from './socketMessageHandler';
import { RootState } from '../../types/store';
import { Action } from 'typescript-fsa';

function arrayBufferToUint8Array(arrayBuffer: ArrayBuffer): Uint8Array {
  return new Uint8Array(arrayBuffer);
}

// At build-time cache will be populated with all required modules.

const DEV = process.env.NODE_ENV !== 'production';

const engineMiddleware = () => {
  let engineSocket: WebSocket;
  let imageRef: HTMLImageElement | null = null;
  let renderFrame: CanvasRenderingContext2D['drawImage'] | null = null;

  // window.addEventListener('beforeunload', e => {
  //   engineSocket.close();
  // });

  return (store: Store<RootState>) => {
    // Setup websocket

    function connectSocket(url: string) {
      try {
        engineSocket = new WebSocket(url, 'forge');
      } catch (e) {
        store.dispatch(engineConnectFailed('Invalid URL'));
        return;
      }

      engineSocket.binaryType = 'arraybuffer';

      engineSocket.addEventListener('error', err => {
        store.dispatch(engineConnectFailed('Failed to connect'));
      });

      engineSocket.addEventListener('open', () => {
        store.dispatch(engineConnected());
      });

      engineSocket.addEventListener('message', event => {
        const data = arrayBufferToUint8Array(event.data);
        const buffer = new flatbuffers.ByteBuffer(data);
        const packet = Forge.Packet.getRootAsPacket(buffer);

        socketMessageHandler(packet, store, imageRef, renderFrame);
      });

      engineSocket.addEventListener('close', () => {
        // store.dispatch(engineConnectFailed('Disconnected'));
      });
    }

    return (next: Dispatch) => (action: Action<any>) => {
      if (action.type === beginEngineConnection.toString()) {
        connectSocket(action.payload);
        next(action);
        return;
      }

      if (action.type === 'UPDATE_IMAGE_FUNCTION') {
        renderFrame = action.payload.renderFrame;
        imageRef = action.payload.imageRef;
        return;
      }

      if (action.type === engineConnectFailed.toString()) {
        next(action);
        return;
      }

      if (action.type === closeEngineConnection.toString()) {
        engineSocket.close();
        next(action);
        return;
      }

      if (!engineSocket) {
        if (!isEngineDead(store.getState())) {
          store.dispatch(engineConnectFailed('Engine died'));
        }

        return;
      }

      if (!isActionForEngine(action)) {
        next(action);
        return;
      }

      const builder = new flatbuffers.Builder();

      const { offset, payloadType } = actionToPacket(builder, action);

      if (payloadType === Forge.PacketPayload.NONE) {
        next(action);
        return;
      }

      // Build a packet to send to the engine
      Forge.Packet.startPacket(builder);
      Forge.Packet.addPayloadType(builder, payloadType);

      // If we have a valid payload offset, add the payload to the packet
      if (offset != null) {
        Forge.Packet.addPayload(builder, offset);
      }

      // Finalize the packet and turn it into a byte array.
      builder.finish(Forge.Packet.endPacket(builder));
      const bytes = builder.asUint8Array();

      next(action);

      engineSocket.send(bytes);
    };
  };
};

export default engineMiddleware;
