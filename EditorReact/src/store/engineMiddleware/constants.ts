export const SUPPORTED_VERSION = 18;

type Width = number;
type Height = number;

// This is temporary until it's stored in Redux, or driven by the UI
export const RESOLUTION: Readonly<[Width, Height]> = [800, 600] as const;

export const MAX_TIME = 240;
