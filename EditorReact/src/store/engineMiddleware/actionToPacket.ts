import { Action, isType } from 'typescript-fsa';
import { Forge } from '../../engine/Packet_generated';
import { flatbuffers } from 'flatbuffers';
import { loadDemo } from '../demoFile/actions';
import {
  createAnimatedKeyFrameRequest,
  createEntityRequest,
  destroyEntityRequest,
  modifyAnimatedKeyframeRequest,
  modifyEntityPropertiesRequest,
  modifyViewport,
  queryEngineState,
  queryEntitiesRequest,
  queryEntityAnimatedProperties,
  queryEntityPropertiesRequest,
  queryInterfaceVersionRequest,
  reloadResourcesRequest,
  removeAnimatedKeyframeRequest,
  renameEntityRequest,
  renderBase64JpegFrameRequest,
  renderFrameRequest,
  renderJpegFrameRequest,
  saveDemoRequest,
  toggleFreelookRequest,
  togglePlayPause,
  updateCameraLocation,
} from '../engine/actions';
import addDataToBuilder from './addDataToBuilder';
import valueToPropertyData from './valueToPropertyData';

interface ActionToPacketReturn {
  offset?: flatbuffers.Offset | null;
  payloadType: Forge.PacketPayload;
}

/*
  How this is working:

  It's a function that takes a Redux action, and returns
  the appropriate ArrayBuffer for the engine.
*/
export default function actionToPacket(
  builder: flatbuffers.Builder,
  action: Action<any>,
): ActionToPacketReturn {
  if (isType(action, updateCameraLocation)) {
    const { deltaX, deltaY, deltaZ, deltaPitch, deltaYaw } = action.payload;

    Forge.UpdateFreeLookCameraNotification.startUpdateFreeLookCameraNotification(
      builder,
    );
    Forge.UpdateFreeLookCameraNotification.addDeltaX(builder, deltaX);
    Forge.UpdateFreeLookCameraNotification.addDeltaY(builder, deltaY);
    Forge.UpdateFreeLookCameraNotification.addDeltaZ(builder, deltaZ);
    Forge.UpdateFreeLookCameraNotification.addDeltaPitch(builder, deltaPitch);
    Forge.UpdateFreeLookCameraNotification.addDeltaYaw(builder, deltaYaw);

    return {
      payloadType: Forge.PacketPayload.UpdateFreeLookCameraNotification,
      offset: Forge.UpdateFreeLookCameraNotification.endUpdateFreeLookCameraNotification(
        builder,
      ),
    };
  }

  if (isType(action, queryInterfaceVersionRequest)) {
    return {
      payloadType: Forge.PacketPayload.QueryInterfaceRequest,
    };
  }

  if (isType(action, loadDemo)) {
    const stringOffset = builder.createString(action.payload);
    Forge.LoadDemoRequest.startLoadDemoRequest(builder);
    Forge.LoadDemoRequest.addPath(builder, stringOffset);

    return {
      payloadType: Forge.PacketPayload.LoadDemoRequest,
      offset: Forge.LoadDemoRequest.endLoadDemoRequest(builder),
    };
  }

  if (isType(action, modifyViewport)) {
    const { height, width } = action.payload;

    Forge.ModifyViewportNotification.startModifyViewportNotification(builder);
    Forge.ModifyViewportNotification.addHeight(builder, height);
    Forge.ModifyViewportNotification.addWidth(builder, width);

    return {
      payloadType: Forge.PacketPayload.ModifyViewportNotification,
      offset: Forge.ModifyViewportNotification.endModifyViewportNotification(
        builder,
      ),
    };
  }

  if (isType(action, togglePlayPause)) {
    return {
      payloadType: Forge.PacketPayload.TogglePlaybackNotification,
    };
  }

  if (isType(action, queryEntitiesRequest)) {
    return { payloadType: Forge.PacketPayload.QueryEntitiesRequest };
  }

  if (isType(action, queryEntityPropertiesRequest)) {
    Forge.QueryPropertiesRequest.startQueryPropertiesRequest(builder);
    Forge.QueryPropertiesRequest.addEntityId(builder, action.payload);
    return {
      payloadType: Forge.PacketPayload.QueryPropertiesRequest,
      offset: Forge.QueryPropertiesRequest.endQueryPropertiesRequest(builder),
    };
  }

  if (isType(action, queryEntityAnimatedProperties)) {
    Forge.QueryAnimatedPropertiesRequest.startQueryAnimatedPropertiesRequest(
      builder,
    );
    Forge.QueryAnimatedPropertiesRequest.addEntityId(builder, action.payload);

    return {
      payloadType: Forge.PacketPayload.QueryAnimatedPropertiesRequest,
      offset: Forge.QueryAnimatedPropertiesRequest.endQueryAnimatedPropertiesRequest(
        builder,
      ),
    };
  }

  if (isType(action, renderBase64JpegFrameRequest)) {
    Forge.RenderJpegBase64FrameRequest.startRenderJpegBase64FrameRequest(
      builder,
    );
    Forge.RenderJpegBase64FrameRequest.addTimeInS(builder, action.payload);
    Forge.RenderJpegBase64FrameRequest.addQuality(builder, 70);

    return {
      payloadType: Forge.PacketPayload.RenderJpegBase64FrameRequest,
      offset: Forge.RenderJpegBase64FrameRequest.endRenderJpegBase64FrameRequest(
        builder,
      ),
    };
  }

  if (isType(action, renderFrameRequest)) {
    throw new Error(
      'renderFrameRequest is not supported in this editor. Use a different renderer',
    );
  }

  if (isType(action, renderJpegFrameRequest)) {
    throw new Error('Use Base64 encoded render');
  }

  if (isType(action, queryEngineState)) {
    return { payloadType: Forge.PacketPayload.QueryDemoStateRequest };
  }

  if (isType(action, createEntityRequest)) {
    const { typeIndex, entityName, fileUrl } = action.payload;

    const nameString = builder.createString(entityName);

    Forge.CreateEntityRequest.startCreateEntityRequest(builder);
    Forge.CreateEntityRequest.addTypeIndex(builder, typeIndex);
    Forge.CreateEntityRequest.addName(builder, nameString);

    return {
      payloadType: Forge.PacketPayload.CreateEntityRequest,
      offset: Forge.CreateEntityRequest.endCreateEntityRequest(builder),
    };
  }

  if (isType(action, destroyEntityRequest)) {
    const id = action.payload;

    Forge.DestroyEntityRequest.startDestroyEntityRequest(builder);
    Forge.DestroyEntityRequest.addEntityId(builder, id);

    return {
      payloadType: Forge.PacketPayload.DestroyEntityRequest,
      offset: Forge.DestroyEntityRequest.endDestroyEntityRequest(builder),
    };
  }

  if (isType(action, createAnimatedKeyFrameRequest)) {
    const {
      entityId,
      propertyIndex,
      timeInS,
      value,
      valueType,
    } = action.payload;

    const valueOffset = addDataToBuilder(
      builder,
      valueType as Forge.TimelineKeyData,
      value,
    );

    Forge.AddAnimatedPropertyKeyRequest.startAddAnimatedPropertyKeyRequest(
      builder,
    );
    Forge.AddAnimatedPropertyKeyRequest.addEntityId(builder, entityId);
    Forge.AddAnimatedPropertyKeyRequest.addPropertyIndex(
      builder,
      propertyIndex,
    );
    Forge.AddAnimatedPropertyKeyRequest.addTimeInS(builder, timeInS);
    Forge.AddAnimatedPropertyKeyRequest.addValueType(
      builder,
      valueType as Forge.TimelineKeyData,
    );
    Forge.AddAnimatedPropertyKeyRequest.addValue(builder, valueOffset);

    return {
      payloadType: Forge.PacketPayload.AddAnimatedPropertyKeyRequest,
      offset: Forge.AddAnimatedPropertyKeyRequest.endAddAnimatedPropertyKeyRequest(
        builder,
      ),
    };
  }

  if (isType(action, removeAnimatedKeyframeRequest)) {
    const { entityId, keyIndex, propertyIndex } = action.payload;

    Forge.RemoveAnimatedPropertyKeyRequest.startRemoveAnimatedPropertyKeyRequest(
      builder,
    );
    Forge.RemoveAnimatedPropertyKeyRequest.addEntityId(builder, entityId);
    Forge.RemoveAnimatedPropertyKeyRequest.addKeyIndex(builder, keyIndex);
    Forge.RemoveAnimatedPropertyKeyRequest.addPropertyIndex(
      builder,
      propertyIndex,
    );

    return {
      payloadType: Forge.PacketPayload.RemoveAnimatedPropertyKeyRequest,
      offset: Forge.RemoveAnimatedPropertyKeyRequest.endRemoveAnimatedPropertyKeyRequest(
        builder,
      ),
    };
  }

  if (isType(action, modifyAnimatedKeyframeRequest)) {
    const {
      entityId,
      keyIndex,
      propertyIndex,
      newTime,
      newValue,
      valueType,
    } = action.payload;

    const valueOffset = addDataToBuilder(
      builder,
      valueType as Forge.TimelineKeyData,
      newValue,
    );

    Forge.ModifyAnimatedPropertyKeyRequest.startModifyAnimatedPropertyKeyRequest(
      builder,
    );
    Forge.ModifyAnimatedPropertyKeyRequest.addEntityId(builder, entityId);
    Forge.ModifyAnimatedPropertyKeyRequest.addKeyIndex(builder, keyIndex);
    Forge.ModifyAnimatedPropertyKeyRequest.addPropertyIndex(
      builder,
      propertyIndex,
    );
    Forge.ModifyAnimatedPropertyKeyRequest.addValueType(
      builder,
      valueType as Forge.TimelineKeyData,
    );
    Forge.ModifyAnimatedPropertyKeyRequest.addValue(builder, valueOffset);
    Forge.ModifyAnimatedPropertyKeyRequest.addTime(builder, newTime);

    return {
      payloadType: Forge.PacketPayload.ModifyAnimatedPropertyKeyRequest,
      offset: Forge.ModifyAnimatedPropertyKeyRequest.endModifyAnimatedPropertyKeyRequest(
        builder,
      ),
    };
  }

  if (isType(action, renameEntityRequest)) {
    const { entityId, newName } = action.payload;

    const nameOffset = builder.createString(newName);

    Forge.RenameEntityRequest.startRenameEntityRequest(builder);
    Forge.RenameEntityRequest.addEntityId(builder, entityId);
    Forge.RenameEntityRequest.addName(builder, nameOffset);

    return {
      payloadType: Forge.PacketPayload.RenameEntityRequest,
      offset: Forge.RenameEntityRequest.endRenameEntityRequest(builder),
    };
  }

  if (isType(action, modifyEntityPropertiesRequest)) {
    const { entityId, newProperty, propertyIndex } = action.payload;

    const valueOffset = valueToPropertyData(
      builder,
      newProperty.type,
      newProperty.value,
    );

    if (!valueOffset) {
      return { payloadType: Forge.PacketPayload.NONE };
    }

    Forge.ModifyPropertyRequest.startModifyPropertyRequest(builder);
    Forge.ModifyPropertyRequest.addValue(builder, valueOffset);
    Forge.ModifyPropertyRequest.addEntityId(builder, entityId);
    Forge.ModifyPropertyRequest.addPropertyIndex(builder, propertyIndex);
    Forge.ModifyPropertyRequest.addValueType(builder, newProperty.type);

    return {
      payloadType: Forge.PacketPayload.ModifyPropertyRequest,
      offset: Forge.ModifyPropertyRequest.endModifyPropertyRequest(builder),
    };
  }

  if (isType(action, toggleFreelookRequest)) {
    return {
      payloadType: Forge.PacketPayload.ToggleFreeLookNotification,
    };
  }

  if (isType(action, saveDemoRequest)) {
    const stringOffset = builder.createString(action.payload);

    Forge.SaveDemoRequest.startSaveDemoRequest(builder);
    Forge.SaveDemoRequest.addPath(builder, stringOffset);

    return {
      payloadType: Forge.PacketPayload.SaveDemoRequest,
      offset: Forge.SaveDemoRequest.endSaveDemoRequest(builder),
    };
  }

  if (isType(action, reloadResourcesRequest)) {
    return {
      payloadType: Forge.PacketPayload.ReloadResourcesRequest,
    };
  }

  return {
    offset: null,
    payloadType: Forge.PacketPayload.NONE,
  };
}
