import { Forge } from '../../engine/Packet_generated';
import { Vector3, Vector4 } from './propertyDataToValue';

export default function propertyTypeToString(
  propertyType: Forge.PropertyData | Forge.TimelineData | Forge.TimelineKeyData,
): string {
  switch (propertyType) {
    case Forge.PropertyData.NONE:
    case Forge.TimelineData.NONE:
    case Forge.TimelineKeyData.NONE:
      return 'None';

    case Forge.PropertyData.BoolProperty:
    case Forge.TimelineData.BooleanTimeline:
    case Forge.TimelineKeyData.BooleanTimelineKey:
      return 'Boolean';

    case Forge.PropertyData.IntegerProperty:
    case Forge.TimelineData.IntegerTimeline:
    case Forge.TimelineKeyData.IntegerTimelineKey:
      return 'Integer';

    case Forge.PropertyData.UnsignedIntegerProperty:
    case Forge.TimelineData.UnsignedIntegerTimeline:
    case Forge.TimelineKeyData.UnsignedIntegerTimelineKey:
      return 'Unsigned Integer';

    case Forge.PropertyData.FloatProperty:
    case Forge.TimelineData.FloatTimeline:
    case Forge.TimelineKeyData.FloatTimelineKey:
      return 'Float';

    case Forge.PropertyData.Vector3Property:
    case Forge.TimelineData.Vector3Timeline:
    case Forge.TimelineKeyData.Vector3TimelineKey:
      return 'Vector3';

    case Forge.PropertyData.Vector4Property:
      return 'Vector4';

    case Forge.TimelineData.QuaternionTimeline:
    case Forge.TimelineKeyData.QuaternionTimelineKey:
      return 'Quaternion';

    case Forge.PropertyData.StringProperty:
      return 'String';
  }
}
