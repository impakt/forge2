import { Forge } from '../../engine/Packet_generated';
import { Store } from 'redux';
import { RootState } from '../../types/store';
import {
  closeEngineConnection,
  createAnimatedKeyFrameResponse,
  destroyEntityResponseSucceeded,
  engineConnectFailed,
  engineStateResponse,
  modifyAnimatedKeyframeResponse,
  modifyAnimatedKeyframeResponseFailed,
  modifyEntityPropertiesResponse,
  queryEntitiesRequest,
  queryEntitiesResponse,
  queryEntityAnimatedPropertiesResponse,
  queryEntityPropertiesResponse,
  queryInterfaceVersionResponse,
  removeAnimatedKeyframeResponse,
  renameEntityResponse,
} from '../engine/actions';
import propertyDataToValue from './propertyDataToValue';
import animatedPropertyToTimeline from './animatedPropertyToTimeline';
import { loadDemoFailure, loadDemoSuccess } from '../demoFile/actions';
import { hide } from 'redux-modal';
import MODALS from '../modal/modals';
import { snackbarActions } from 'material-ui-snackbar-redux';
import { RESOLUTION, SUPPORTED_VERSION } from './constants';
import {
  AnimatedEntityProperty,
  Entity,
  StaticEntityProperty,
} from '../../types/engine/properties';

export default function socketMessageHandler(
  packet: Forge.Packet,
  store: Store<RootState>,
  imageRef: HTMLImageElement | null,
  renderFrame: CanvasRenderingContext2D['drawImage'] | null,
): void {
  switch (packet.payloadType()) {
    case Forge.PacketPayload.RenderJpegBase64FrameResponse: {
      const payload = packet.payload(
        new Forge.RenderJpegBase64FrameResponse(),
      )!;
      const data = payload.jpegData();

      if (!data) {
        return;
      }

      const rawString = `data:image/jpeg;base64,${data}`;

      const image = new Image(RESOLUTION[0], RESOLUTION[1]);
      image.src = rawString;

      image.onload = () => renderFrame!(image, 0, 0);
      break;
    }

    case Forge.PacketPayload.NONE:
      console.warn('NONE TYPE FOUND');
      break;

    case Forge.PacketPayload.QueryInterfaceResponse: {
      const payload = packet.payload(new Forge.QueryInterfaceResponse());
      if (!payload) break;

      const version = payload.version();
      if (version > SUPPORTED_VERSION) {
        const message = `Engine version ${version} is not compatible. Expected version ${SUPPORTED_VERSION}`;
        store.dispatch(engineConnectFailed(message));
        store.dispatch(closeEngineConnection());

        store.dispatch(
          snackbarActions.show({
            message,
            action: 'Reload',
            handler: () => window.location.reload(),
          }),
        );
      } else {
        store.dispatch(queryInterfaceVersionResponse(version));
      }
      break;
    }

    case Forge.PacketPayload.QueryEntitiesResponse: {
      const payload = packet.payload(new Forge.QueryEntitiesResponse());
      if (!payload) break;

      const length = payload.entitiesLength();

      const entities: Entity[] = [];
      for (let i = 0; i < length; i++) {
        const entity = payload.entities(i);
        if (!entity) continue;

        entities.push({
          id: entity.id(),
          typeIndex: entity.typeIndex(),
          name: entity.name()!,
          staticProperties: [],
          animatedProperties: [],
        } as Entity);
      }

      store.dispatch(queryEntitiesResponse(entities));
      break;
    }

    case Forge.PacketPayload.QueryPropertiesResponse: {
      const payload = packet.payload(new Forge.QueryPropertiesResponse());

      if (!payload) break;

      if (payload.result() === Forge.Result.Error) {
        console.error('Failed to get properties');
        break;
      }

      // TODO: Handle result failure

      const properties: StaticEntityProperty<any>[] = [];

      for (let i = 0; i < payload.propertiesLength(); i++) {
        const property = payload.properties(i);
        if (!property) continue;

        const type = property.valueType();
        const object: StaticEntityProperty<any> = {
          type,
          description: property.description(),
          name: property.name()!,
          value: propertyDataToValue(property),
        };

        properties.push(object);
      }
      store.dispatch(queryEntityPropertiesResponse(properties));
      break;
    }

    case Forge.PacketPayload.QueryAnimatedPropertiesResponse: {
      const payload = packet.payload(
        new Forge.QueryAnimatedPropertiesResponse(),
      );

      if (!payload) break;

      // TODO: handle failure

      const properties: AnimatedEntityProperty[] = [];
      for (let i = 0; i < payload.propertiesLength(); i++) {
        const property = payload.properties(i);
        if (!property) continue;

        const timelineType = property.timelineType();

        const object: AnimatedEntityProperty = {
          type: timelineType,
          name: property.name()!,
          description: property.description(),
          animated: true,
          timeline: animatedPropertyToTimeline(property),
        };

        properties.push(object);
      }

      store.dispatch(queryEntityAnimatedPropertiesResponse(properties));
      break;
    }

    case Forge.PacketPayload.LoadDemoResponse: {
      const payload = packet.payload(new Forge.LoadDemoResponse());
      if (!payload) break;

      const result = payload.result();

      switch (result) {
        case Forge.Result.Success:
          store.dispatch(loadDemoSuccess());
          break;

        case Forge.Result.Error:
          store.dispatch(loadDemoFailure());
          break;
      }
      break;
    }

    case Forge.PacketPayload.QueryDemoStateResponse: {
      const payload = packet.payload(new Forge.QueryDemoStateResponse());
      if (!payload) break;

      const name = payload.name();
      const group = payload.group();
      const project = payload.project();

      store.dispatch(
        engineStateResponse({
          name,
          group,
          project,
        }),
      );
      break;
    }

    case Forge.PacketPayload.CreateEntityResponse: {
      const payload = packet.payload(new Forge.CreateEntityResponse());
      if (!payload) break;

      if (payload.result() === Forge.Result.Success) {
        // TODO: put this in a saga
        store.dispatch(hide(MODALS.createEntity));
        store.dispatch(queryEntitiesRequest());
      } else {
        console.error('Failed to create entity');
      }

      break;
    }

    case Forge.PacketPayload.DestroyEntityResponse: {
      const payload = packet.payload(new Forge.DestroyEntityResponse());
      if (!payload) break;

      // TODO: A saga needs to coordinate removing these from state
      if (payload.result() === Forge.Result.Success) {
        store.dispatch(destroyEntityResponseSucceeded());
      } else {
        store.dispatch(
          snackbarActions.show({
            message: 'Failed to delete entity',
          }),
        );
      }
      break;
    }

    case Forge.PacketPayload.AddAnimatedPropertyKeyResponse: {
      const payload = packet.payload(
        new Forge.AddAnimatedPropertyKeyResponse(),
      );
      if (!payload) break;

      if (payload.result() === Forge.Result.Success) {
        store.dispatch(
          snackbarActions.show({
            message: 'Created keyframe',
          }),
        );
        store.dispatch(createAnimatedKeyFrameResponse());
      } else {
        store.dispatch(
          snackbarActions.show({
            message: 'Failed to create keyframe',
          }),
        );
      }
      break;
    }

    case Forge.PacketPayload.SaveDemoResponse: {
      const payload = packet.payload(new Forge.SaveDemoResponse());
      if (!payload) break;

      if (payload.result() === Forge.Result.Success) {
        store.dispatch(
          snackbarActions.show({
            message: 'Saved',
          }),
        );
      } else {
        store.dispatch(
          snackbarActions.show({
            message: 'Error saving!',
          }),
        );
      }
      break;
    }

    case Forge.PacketPayload.ModifyPropertyResponse: {
      const payload = packet.payload(new Forge.ModifyPropertyResponse());
      if (!payload) break;

      if (payload.result() === Forge.Result.Success) {
        store.dispatch(modifyEntityPropertiesResponse());
      } else {
        store.dispatch(
          snackbarActions.show({
            message: 'Error!',
          }),
        );
      }
      break;
    }

    case Forge.PacketPayload.RenameEntityResponse: {
      const payload = packet.payload(new Forge.RenameEntityResponse());
      if (!payload) break;

      if (payload.result() === Forge.Result.Success) {
        store.dispatch(renameEntityResponse());
      } else {
        store.dispatch(
          snackbarActions.show({
            message: 'Error!',
          }),
        );
      }
      break;
    }

    case Forge.PacketPayload.RemoveAnimatedPropertyKeyResponse: {
      const payload = packet.payload(
        new Forge.RemoveAnimatedPropertyKeyResponse(),
      );
      if (!payload) break;

      if (payload.result() === Forge.Result.Success) {
        store.dispatch(removeAnimatedKeyframeResponse());
      } else {
        store.dispatch(
          snackbarActions.show({
            message: 'Could not remove key frame',
          }),
        );
      }
      break;
    }

    case Forge.PacketPayload.ModifyAnimatedPropertyKeyResponse: {
      const payload = packet.payload(
        new Forge.ModifyAnimatedPropertyKeyResponse(),
      );
      if (!payload) break;

      if (payload.result() === Forge.Result.Success) {
        store.dispatch(modifyAnimatedKeyframeResponse());
      } else {
        store.dispatch(modifyAnimatedKeyframeResponseFailed());
      }
      break;
    }

    case Forge.PacketPayload.ReloadResourcesResponse: {
      const payload = packet.payload(new Forge.ReloadResourcesResponse());
      if (!payload) break;

      if (payload.result() === Forge.Result.Error) {
        store.dispatch(
          snackbarActions.show({
            message: 'Error trying to reload resources',
          }),
        );
      }
      break;
    }

    default:
      console.log(
        'A different type found',
        Forge.PacketPayload[packet.payloadType()],
      );
      break;
  }
}
