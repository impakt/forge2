import { Forge } from '../../engine/Packet_generated';
import { Vector3, Vector4 } from './propertyDataToValue';
import { KeyframeType } from '../../types/engine/properties';

export default function animatedPropertyToTimeline(
  property: Forge.AnimatedPropertyDescription,
): KeyframeType[] {
  switch (property.timelineType()) {
    case Forge.TimelineData.NONE:
      return [];

    case Forge.TimelineData.BooleanTimeline: {
      const timelineData = property.timeline(new Forge.BooleanTimeline());
      if (!timelineData) {
        return [];
      }

      const timeline: KeyframeType[] = [];
      for (let j = 0; j < timelineData.keysLength(); j++) {
        const key = timelineData.keys(j);
        if (!key) continue;

        timeline.push({
          time: key.time(),
          value: key.value(),
        });
      }
      return timeline;
    }

    case Forge.TimelineData.IntegerTimeline: {
      const timelineData = property.timeline(new Forge.IntegerTimeline());
      if (!timelineData) {
        return [];
      }

      const timeline: KeyframeType[] = [];
      for (let j = 0; j < timelineData.keysLength(); j++) {
        const key = timelineData.keys(j);
        if (!key) continue;

        timeline.push({
          time: key.time(),
          value: key.value(),
        });
      }
      return timeline;
    }

    case Forge.TimelineData.UnsignedIntegerTimeline: {
      const timelineData = property.timeline(
        new Forge.UnsignedIntegerTimeline(),
      );
      if (!timelineData) {
        return [];
      }

      const timeline: KeyframeType[] = [];
      for (let j = 0; j < timelineData.keysLength(); j++) {
        const key = timelineData.keys(j);
        if (!key) continue;

        timeline.push({
          time: key.time(),
          value: key.value(),
        });
      }
      return timeline;
    }

    case Forge.TimelineData.FloatTimeline: {
      const timelineData = property.timeline(new Forge.FloatTimeline());
      if (!timelineData) {
        return [];
      }

      const timeline: KeyframeType[] = [];
      for (let j = 0; j < timelineData.keysLength(); j++) {
        const key = timelineData.keys(j);
        if (!key) continue;

        timeline.push({
          time: key.time(),
          value: key.value(),
        });
      }
      return timeline;
    }

    case Forge.TimelineData.Vector3Timeline: {
      const timelineData = property.timeline(new Forge.Vector3Timeline());
      if (!timelineData) {
        return [];
      }

      const timeline: KeyframeType[] = [];
      for (let j = 0; j < timelineData.keysLength(); j++) {
        const key = timelineData.keys(j);
        if (!key) continue;

        const vec3 = key.value();
        if (!vec3) continue;

        timeline.push({
          time: key.time(),
          value: {
            x: vec3.x(),
            y: vec3.y(),
            z: vec3.z(),
          } as Vector3,
        });
      }
      return timeline;
    }

    case Forge.TimelineData.QuaternionTimeline: {
      const timelineData = property.timeline(new Forge.QuaternionTimeline());
      if (!timelineData) {
        return [];
      }

      const timeline: KeyframeType[] = [];
      for (let j = 0; j < timelineData.keysLength(); j++) {
        const key = timelineData.keys(j);
        if (!key) continue;

        const quat = key.value();
        if (!quat) continue;

        timeline.push({
          time: key.time(),
          value: {
            x: quat.x(),
            y: quat.y(),
            z: quat.z(),
            w: quat.w(),
          } as Vector4,
        });
      }
      return timeline;
    }
  }
}
