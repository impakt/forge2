interface Frame {
  url: string;
  createdAt: Date;
}

export default class FrameImageManager {
  private _frames: Frame[] = [];
  private _interval: number | NodeJS.Timeout = 0;

  private cleanup() {
    if (this._frames && this._frames.length > 1) {
      requestIdleCallback(() => {
        // Skip 0, it's always the current rendered frame
        for (let i = 1; i < this._frames.length; i++) {
          URL.revokeObjectURL(this._frames[i].url);
        }
      });
    }
  }

  constructor() {
    this._interval = setInterval(this.cleanup, 5000);
  }

  registerFrame(
    blob: Blob,
    width: number,
    height: number,
  ): Promise<HTMLImageElement> {
    return new Promise<HTMLImageElement>(resolve => {
      const image = new Image(width, height);
      const frameUrl = URL.createObjectURL(blob);

      image.src = frameUrl;
      image.onload = () => resolve(image);

      this._frames.unshift({
        url: frameUrl,
        createdAt: new Date(),
      });
    });
  }
}
