import { Forge } from '../../engine/Packet_generated';
import { Vector3, Vector4 } from './propertyDataToValue';
import { flatbuffers } from 'flatbuffers';
import { PropertyDataToValue } from '../../types/engine/properties';

export default function valueToPropertyData<T extends Forge.PropertyData>(
  builder: flatbuffers.Builder,
  propertyType: T,
  value: PropertyDataToValue<T>,
): flatbuffers.Offset | null {
  switch (propertyType) {
    case Forge.PropertyData.NONE:
      return null;

    case Forge.PropertyData.BoolProperty:
      Forge.BoolProperty.startBoolProperty(builder);
      Forge.BoolProperty.addValue(builder, value as boolean);
      return Forge.BoolProperty.endBoolProperty(builder);

    case Forge.PropertyData.IntegerProperty:
      Forge.IntegerProperty.startIntegerProperty(builder);
      Forge.IntegerProperty.addValue(builder, value as number);
      return Forge.IntegerProperty.endIntegerProperty(builder);

    case Forge.PropertyData.UnsignedIntegerProperty:
      Forge.UnsignedIntegerProperty.startUnsignedIntegerProperty(builder);
      Forge.UnsignedIntegerProperty.addValue(builder, value as number);
      return Forge.UnsignedIntegerProperty.endUnsignedIntegerProperty(builder);

    case Forge.PropertyData.FloatProperty:
      Forge.FloatProperty.startFloatProperty(builder);
      Forge.FloatProperty.addValue(builder, value as number);
      return Forge.FloatProperty.endFloatProperty(builder);

    case Forge.PropertyData.Vector3Property: {
      const { x, y, z } = value as Vector3;
      const vectorOffset = Forge.Vector3.createVector3(builder, x, y, z);

      Forge.Vector3Property.startVector3Property(builder);
      Forge.Vector3Property.addValue(builder, vectorOffset);
      return Forge.Vector3Property.endVector3Property(builder);
    }

    case Forge.PropertyData.Vector4Property: {
      const { x, y, z, w } = value as Vector4;
      const vectorOffset = Forge.Vector4.createVector4(builder, x, y, z, w);

      Forge.Vector4Property.startVector4Property(builder);
      Forge.Vector4Property.addValue(builder, vectorOffset);
      return Forge.Vector4Property.endVector4Property(builder);
    }

    case Forge.PropertyData.StringProperty: {
      const stringOffset = builder.createString(value as string);
      Forge.StringProperty.startStringProperty(builder);
      Forge.StringProperty.addValue(builder, stringOffset);
      return Forge.StringProperty.endStringProperty(builder);
    }
  }

  return null;
}
