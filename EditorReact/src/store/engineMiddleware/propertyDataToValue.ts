import { Forge } from '../../engine/Packet_generated';

export type Vector3 = { x: number; y: number; z: number };
export type Vector4 = { w: number; x: number; y: number; z: number };

export type PropertyDataType =
  | string
  | boolean
  | number
  | Vector3
  | Vector4
  | null;

export default function propertyDataToValue(
  property: Forge.PropertyDescription,
): PropertyDataType {
  switch (property.valueType()) {
    case Forge.PropertyData.NONE:
      return null;

    case Forge.PropertyData.BoolProperty: {
      const propertyValue = property.value(new Forge.BoolProperty());
      if (!propertyValue) {
        return null;
      }
      return propertyValue.value();
    }

    case Forge.PropertyData.IntegerProperty: {
      const propertyValue = property.value(new Forge.IntegerProperty());
      if (!propertyValue) {
        return null;
      }
      return propertyValue.value();
    }

    case Forge.PropertyData.UnsignedIntegerProperty: {
      const propertyValue = property.value(new Forge.UnsignedIntegerProperty());
      if (!propertyValue) {
        return null;
      }
      return propertyValue.value();
    }

    case Forge.PropertyData.FloatProperty: {
      const propertyValue = property.value(new Forge.FloatProperty());
      if (!propertyValue) {
        return null;
      }
      return propertyValue.value();
    }

    case Forge.PropertyData.Vector3Property: {
      const propertyValue = property.value(new Forge.Vector3Property());
      if (!propertyValue) {
        return null;
      }

      const v = propertyValue.value();

      if (!v) {
        return null;
      }

      return {
        x: v.x(),
        y: v.y(),
        z: v.z(),
      } as Vector3;
    }

    case Forge.PropertyData.Vector4Property: {
      const propertyValue = property.value(new Forge.Vector4Property());
      if (!propertyValue) {
        return null;
      }

      const v = propertyValue.value();

      if (!v) {
        return null;
      }

      return {
        w: v.w(),
        x: v.x(),
        y: v.y(),
        z: v.z(),
      } as Vector4;
    }

    case Forge.PropertyData.StringProperty: {
      const propertyValue = property.value(new Forge.StringProperty());
      if (!propertyValue) {
        return null;
      }
      return propertyValue.value();
    }
  }
}
